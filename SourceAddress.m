//
//  SourceAddress.m
//  UBER
//
//  Created by Rahul Sharma on 22/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SourceAddress.h"


@implementation SourceAddress

@dynamic srcAddress;
@dynamic srcAddress2;
@dynamic srcLatitude;
@dynamic srcLongitude;

@end
