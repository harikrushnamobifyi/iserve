//
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
#import "VerifyiMobileViewController.h"
#import "CountryNameTableViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "FBLoginHandler.h"
#import "CountryPicker.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()<FBLoginHandlerDelegate>
{
    BOOL isEmailValid;
    BOOL isPhoneNumberValid;
    BOOL isReferralValid;
}
@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (assign ,nonatomic) BOOL isKeyboardIsShown;
typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;

//-(PasswordStrengthType)checkPasswordStrength:(NSString *)password;
-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize mainView, mainScrollView;
@synthesize firstNameTextField,referralCodeTextField;
@synthesize lastNameTextField;
@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize phoneNoTextField;
@synthesize helperCountry;
@synthesize helperCity;
@synthesize navNextButton;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncButton;
@synthesize tncCheckButton;
@synthesize creatingLabel;
@synthesize isKeyboardIsShown;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    self.navigationItem.title = @"REGISTER";
    isEmailValid = NO;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self createNavLeftButton];
    [self createNavRightButton];
    
    [Helper setToLabel:creatingLabel Text:@"By creating an account you agree to our " WithFont:OpenSans_Light FSize:13 Color:UIColorFromRGB(0x000000)];
    
    [Helper setButton:_signUpButton Text:@"Sign Up" WithFont:HELVETICANEUE_BOLD FSize:16 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    
    [firstNameTextField setValue:UIColorFromRGB(0x999999)
                      forKeyPath:@"_placeholderLabel.textColor"];
    
    [lastNameTextField setValue:UIColorFromRGB(0x999999)
                     forKeyPath:@"_placeholderLabel.textColor"];
    
    [emailTextField setValue:UIColorFromRGB(0x999999)
                  forKeyPath:@"_placeholderLabel.textColor"];
    
    [passwordTextField setValue:UIColorFromRGB(0x999999)
                     forKeyPath:@"_placeholderLabel.textColor"];
    
    [phoneNoTextField setValue:UIColorFromRGB(0x999999)
                    forKeyPath:@"_placeholderLabel.textColor"];
    [referralCodeTextField setValue:UIColorFromRGB(0x999999)
                         forKeyPath:@"_placeholderLabel.textColor"];
    
    profileImageView.image = [UIImage imageNamed:@"pic_bg"];
    
    mainScrollView.scrollEnabled = YES;
    isTnCButtonSelected = NO;
    
    mainScrollView.contentSize = CGSizeMake(320,CGRectGetHeight(creatingLabel.frame)+CGRectGetMinY(creatingLabel.frame)+30);
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    [_countryCode setTitle:[NSString stringWithFormat:@"+%@",stringCountryCode] forState:UIControlStateNormal];
    _countryFlag.image = [UIImage imageNamed:imagePath];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    isKeyboardIsShown = NO;
    
    if (!firstNameTextField.text.length > 0) {
        
        [firstNameTextField becomeFirstResponder];
    }
    
    if(_isComingFromLogin)
    {
        
        [self setSignupDetailsFromFacebook];
    }
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    // unregister for keyboard notifications while not visible.
    [self keyboardWillHide:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationItem.titleView  = nil;
    [self keyboardWillHide:nil];
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)createNavRightButton
{
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImageOn = [UIImage imageNamed:@"next_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"next_btn_off"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navNextButton addTarget:self action:@selector(NextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navNextButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navNextButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [phoneNoTextField resignFirstResponder];
    
}

-(void)signUp
{
    NSString *signupFirstName       = firstNameTextField.text;
    NSString *signupEmail           = emailTextField.text;
    NSString *signupPassword        = passwordTextField.text;
    NSString *signupPhoneno         = phoneNoTextField.text;
    
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter First Name"];
        [firstNameTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
        [emailTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupPhoneno.length == 0 || !isPhoneNumberValid)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter valid phone number."];
        [phoneNoTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password"];
        [passwordTextField becomeFirstResponder];
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email Id"];
        emailTextField.text             = @"";
        [emailTextField becomeFirstResponder];
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please select our Terms and Condition"];
    }
    else if (isEmailValid == NO) {
        
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter valid zipcode to continue."];
    }
    else
    {
        
        [self dismissKeyboard];
        NSString *signupPhoneno = [NSString stringWithFormat:@"%@%@",self.countryCode.titleLabel.text,phoneNoTextField.text];
        
        if (referralCodeTextField.text.length > 0 && isReferralValid) {
            
            saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text,flStrForStr(lastNameTextField.text),emailTextField.text,signupPhoneno,passwordTextField.text,flStrForStr(referralCodeTextField.text), nil];
            
            [self performSegueWithIdentifier:@"verifyiController" sender:saveSignUpDetails];
        }
        else if (referralCodeTextField.text.length == 0) {
            
            saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text,flStrForStr(lastNameTextField.text),emailTextField.text,signupPhoneno,passwordTextField.text,@"", nil];
            [self performSegueWithIdentifier:@"verifyiController" sender:saveSignUpDetails];
        }
        else {
            
            [self validateRefferalCode];
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -Referral Validation-

- (void) validateRefferalCode
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Validating referral code...", @"Validating referral code...")];
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@""])
        lat = @"13.028869";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"77.589695";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    
    NSDictionary *params = @{
                             @"ent_coupon":referralCodeTextField.text,
                             @"ent_lat":lat,
                             @"ent_long":lon,
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       NSLog(@"response %@",response);
                                       [self validateRefferalCodeResponse:response];
                                   }
                               }];
}

-(void)validateRefferalCodeResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey: @"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        referralCodeTextField.text = @"";
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [referralCodeTextField resignFirstResponder];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
            isReferralValid = YES;
        }
        else
        {
            referralCodeTextField.text = @"";
            isReferralValid = NO;
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
            [referralCodeTextField becomeFirstResponder];
        }
    }
}

- (void) validateEmailAndPostalCode
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Validating Email.."];
    
    
    NSDictionary *params = @{
                             @"ent_email":emailTextField.text,
                             @"zip_code":@"560024",
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"validateEmailZip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response

                                       [self validateEmailResponse:response];
                                   }
                               }];
    
    
}

-(void)validateEmailResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            isEmailValid = YES;
            saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text,lastNameTextField.text,emailTextField.text,phoneNoTextField.text,passwordTextField.text, nil];
            
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            emailTextField.text = @"";
            [emailTextField becomeFirstResponder];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:phoneNoTextField]) {
        
        if (phoneNoTextField.text.length == 10)
        {
            [self sendServiceToGetVerificationCode];
        }
        else
        {
            
            [Helper showAlertWithTitle:@"Message" Message:@"Please enter Valid Mobile No!"];
            
        }
        
        
        
    }
    else if ([textField isEqual:emailTextField])
    {
        if ([Helper emailValidationCheck:emailTextField.text])
        {
            [self validateEmailAndPostalCode];
        }
        
        
    }
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == firstNameTextField)
    {
        [lastNameTextField becomeFirstResponder];
    }
    else if(textField == lastNameTextField)
    {
        [emailTextField becomeFirstResponder];
    }
    else if (textField == emailTextField)
    {
        [phoneNoTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField)
    {
        int strength = [self checkPasswordStrength:passwordTextField.text];
        if(strength == 0)
        {
            [passwordTextField becomeFirstResponder];
        }
        else
        {
            [referralCodeTextField becomeFirstResponder];
            
        }
    }
    else {
        
        [referralCodeTextField resignFirstResponder];
        
        [self signUp];
        
    }
    return YES;
}

- (IBAction)NextButtonClicked:(id)sender {
    
    [self signUp];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"verifyiController"])
    {
        VerifyiMobileViewController *CLVC = (VerifyiMobileViewController*)[segue destinationViewController];
        CLVC.saveSignUpDetails = [saveSignUpDetails mutableCopy];
        CLVC.pickedImage = _pickedImage;
        
        
    }
    else if ([segue.identifier isEqualToString:@"countryPicker"]) {
        NSArray *temp = [[segue destinationViewController] childViewControllers];
        CountryNameTableViewController *popoverViewController = (CountryNameTableViewController *)[temp objectAtIndex:0];
        popoverViewController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
        {
            self.countryFlag.image = flagimg;
            NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
            [self.countryCode setTitle:countryCode forState:UIControlStateNormal];
            self.countryName.titleLabel.text = countryName;
            
        };
    }
    
    else if ([[segue identifier] isEqualToString:@"gotoTerms"])
    {
        
    }
}

- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)TermsNconButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)checkButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        [tncCheckButton setImage:[UIImage imageNamed:@"checkbox_off"] forState:UIControlStateNormal];
        
    }
    else
    {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        [tncCheckButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateSelected];
        
    }
}



- (IBAction)profileButtonClicked:(id)sender
{
    [_activeTextField resignFirstResponder];
    
    if(profileImageView.image.scale == 1){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library",@"Remove Profile Photo",nil];
        actionSheet.tag = 1;
        [actionSheet showInView:self.view];
    }
    else{
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library",nil];
        actionSheet.tag = 1;
        [actionSheet showInView:self.view];
        
        
    }
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            case 2:
            {
                [self deSelectProfileImage];
                break;
            }
            default:
                break;
        }
    }
}

-(void)deSelectProfileImage {
    
    profileImageView.image =  [UIImage imageNamed:@"pic_bg"];;
    _pickedImage = nil;
    
}

-(void)cameraButtonClicked
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    profileImageView.image = _pickedImage;
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter password first"];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Password should contain atleast one uppercase & one lowercase alphabet & one number!"];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Password should contain atleast one uppercase & one lowercase alphabet & one number!"];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:@"Message" Message:@"Password should contain atleast one uppercase & one lowercase alphabet & one number!"];
        // [passwordTextField becomeFirstResponder];
        return 0;
    }
    return 1;
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.mainScrollView.scrollIndicatorInsets = ei;
        self.mainScrollView.contentInset = ei;
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.mainScrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.mainScrollView.scrollIndicatorInsets = ei;
    self.mainScrollView.contentInset = ei;
}


#pragma mark - FacebookButton Actions

- (IBAction)registerWithFacebookButtonAction:(id)sender {
    
    FBLoginHandler *fb= [[FBLoginHandler alloc]init];
    fb.delegate = self;
    
    // Check for Network Availability
    if ([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        [fb loginWithFacebook];
    }
    else
    {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}
-(void)didFacebookUserLogin:(BOOL)login withDetail:(NSDictionary*)userInfo{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    if(login == NO){
        [Helper showAlertWithTitle:@"Message" Message:@"Facebook Signup is cancelled by user"];
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
        return;
    }
    
    [self getFbLogInService:userInfo];
}

- (void)getFbLogInService:(NSDictionary *)dictionary
{
    _facebookData = dictionary;
    NSString *emailID;
    if ([_facebookData objectForKey:@"email"])
    {
        emailID = [_facebookData objectForKey:@"email"];
    }
    else
    {
        emailID = [NSString stringWithFormat:@"%@@gmail.com",[_facebookData objectForKey:@"id"]];
    }
    [self setSignupDetailsFromFacebook];
}

-(void)validateEmailResponseforSignup :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([[response objectForKey:@"errNum"] intValue] == 11){
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
        
    }
    
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1)
        {
            [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
            
        }
        else if([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            isEmailValid = YES;
        }
    }
}

-(void)sendServiceToGetVerificationCode {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    
    NSDictionary *params = @{
                             @"ent_phone":[NSString stringWithFormat:@"%@%@",self.countryCode.titleLabel.text,phoneNoTextField.text]
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkMobile"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       
                                       [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                       
                                       
                                       if (!response)
                                       {
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:NSLocalizedString(@"Message", @"Message")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                                           [alertView show];
                                           
                                       }
                                       else if ([response objectForKey:@"Error"])
                                       {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
                                       }
                                       else
                                       {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
                                           {
                                               isPhoneNumberValid = YES;
                                           }
                                           else
                                           {
                                               phoneNoTextField.text = @"";
                                               isPhoneNumberValid = NO;
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                           }
                                       }
                                   }
                               }];
    
}


- (void)setSignupDetailsFromFacebook
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (_facebookData)
    {
        firstNameTextField.text = _facebookData[@"first_name"];
        lastNameTextField.text = _facebookData [@"last_name"];
        emailTextField.text = _facebookData [@"email"];
        if (emailTextField.text.length == 0)
        {
            [Helper showAlertWithTitle:@"Oops" Message:@"Please enter your email address. We are having trouble getting that from your Facebook profile."];
            [emailTextField becomeFirstResponder];
        }
        else
        {
            [phoneNoTextField becomeFirstResponder];
            isEmailValid = YES;
        }
        
        NSString *imageURL =  [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",_facebookData[@"id"]];
        
        [profileImageView sd_setImageWithURL:[NSURL URLWithString:imageURL]
                            placeholderImage:[UIImage imageNamed:@"pic_bg"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                       
                                       if(!error)
                                       {
                                           _pickedImage = image;
                                           _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
                                           
                                       }
                                       else
                                       {
                                           NSLog(@"Error");
                                       }
                                       
                                   }];
        
        _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
        
    }
}
- (IBAction)countryChooseBtn:(id)sender
{
    
    
    
    
    
    
    
}








@end
