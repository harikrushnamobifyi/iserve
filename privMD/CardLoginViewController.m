//
//  CardLoginViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CardLoginViewController.h"
#import "CardIO.h"
#import "UploadFiles.h"
#import "ViewController.h"
#import "Database.h"
#import "Entity.h"
#import "PTKTextField.h"
#import "PKView+Private.h"

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface CardLoginViewController ()<CardIOPaymentViewControllerDelegate>
@property(nonatomic,strong)PTKCard *card;
@property(nonatomic,strong)STPCard *stripeCard;
@property (strong, nonatomic) IBOutlet UIView *topView;
@end

@implementation CardLoginViewController

@synthesize navNextButton;
@synthesize getSignupDetails;
@synthesize scanButton;
@synthesize infoLabel;
@synthesize cvvLabel;
@synthesize postalLabel;
@synthesize expLabel;
@synthesize doneButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//pkviewDelegate
- (void) paymentView:(PTKView*)paymentView withCard:(PTKCard *)card isValid:(BOOL)valid
{
    _card = card;
    TELogInfo(@"Card number: %@", card.number);
    TELogInfo(@"Card expiry: %lu/%lu", (unsigned long)card.expMonth, (unsigned long)card.expYear);
    TELogInfo(@"Card cvc: %@", card.cvc);
    TELogInfo(@"Address zip: %@", card.addressZip);
    
}

- (void)setupCardPostalField
{
    postalText = [[UITextField alloc] initWithFrame:CGRectMake(290,0,55,20)];
    postalText.delegate = self;
    postalText.placeholder = @"Postal";
    postalText.keyboardType = UIKeyboardTypeNumberPad;
    postalText.textColor = [UIColor lightGrayColor];
    [postalText.layer setMasksToBounds:YES];
    
    [self.view addSubview:postalText];
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.hidesBackButton = YES;
    self.infoLabel.text = @"";
    self.paymentView = [[PTKView alloc] initWithFrame:CGRectMake(11,50,300,50)];
    self.paymentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"card_textfield"]];
    [self.view addSubview:self.paymentView];
    [self setUpFrameForScanButton];
    
}
-(void)setUpFrameForScanButton {
    
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0X333333) forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    doneButton.titleLabel.font = [UIFont fontWithName:OpenSans_SemiBold size:13];
    
    [scanButton setTitle:@"Scan the card" forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    scanButton.titleLabel.font = [UIFont fontWithName:OpenSans_SemiBold size:13];
    [scanButton addTarget:self action:@selector(scanCardClicked:) forControlEvents:UIControlEventTouchUpInside];

}

-(void)viewDidDisappear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"ADD CARD";
    self.paymentView.delegate = self;
    self.navigationController.navigationBarHidden = NO;
    
    if((_isComingFromPayment ==1)||(_isComingFromPayment == 2))
    {
        if (_isComingFromPayment == 2)
        {
            //self.navigationController.navigationBarHidden = YES;
          //  [self createTopView];
            self.paymentView.frame = CGRectMake(11,40,300,50);
            CGRect frameOfDoneButton = doneButton.frame;
            frameOfDoneButton.origin.y = 160;
            doneButton.frame = frameOfDoneButton;
            CGRect fr = scanButton.frame;
            fr.origin.y = 100;
            scanButton.frame = fr;
            CGRect fr1 = _topImageView.frame;
            fr1.origin.y = 23;
            _topImageView.frame = fr1;
            _topImageView.hidden = YES;
            
        }
        _topImageView.hidden = YES;

        self.navigationItem.title = @"ADD CARD";
        [self createNavLeftButton];
        
    }
    else
    {
        [self createNavView];
        //[self createNavLeftButton];
        [self createNavRightButton];

    }
}


-(void)createTopView{
    
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0,0, 320, 64)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,27, 320, 30)];
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.text = @"ADD CARD";
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.font = [UIFont fontWithName:OpenSans_Regular size:17];
    [navView addSubview:navTitle];
    navView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_navigationbar"]];

    
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,18.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
        [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    [navView addSubview:navCancelButton];
    [self.view addSubview:navView];

}

- (IBAction)backButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,10, 147, 30)];
    navTitle.text = @"ADD CARD";
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:OpenSans_Regular size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}

-(void)createNavRightButton
{
    
//    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *buttonImageOn = [UIImage imageNamed:@"next_btn_on"];
//    UIImage *buttonImageOff = [UIImage imageNamed:@"next_btn_off"];
//    [navNextButton setFrame:CGRectMake(10,0,15,25)];
//    navNextButton.titleLabel.text = @"SKIP";
//   navNextButton.titleLabel.textColor = UIColorFromRGB(0xffffff);
//    
//    navNextButton.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:17];
//
//    [navNextButton addTarget:self action:@selector(skipButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    
//      [navNextButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
//    [navNextButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
//    // Create a container bar button
//    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
//    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
//                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
//                                       target:nil action:nil];
//    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
//    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
//
    
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navNextButton setFrame:CGRectMake(0,0,50,50)];
    
    [navNextButton addTarget:self action:@selector(skipButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [Helper setButton:navNextButton Text:NSLocalizedString(@"SKIP", @"SKIP") WithFont:OpenSans_Regular FSize:15 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];

    
    
    
    
    
    
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    //[Helper setButton:navCancelButton Text:@"" WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    //    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    //    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    //    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
    
}

-(void)checkMandatoryFields
{
    NSString *cardno = _card.number;
    NSUInteger cardexpMnt = _card.expMonth;
    NSUInteger cardexpYr = _card.expYear;
    NSString *cardcvc = _card.cvc;
    
    TELogInfo(@"%lu ",(unsigned long)infoLabel.text.length);
    
    if (((unsigned long)cardno.length == 0) && ((unsigned long)infoLabel.text.length== 0))
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Card No"];
        
    }
    else if (((unsigned long)cardcvc.length == 0) && ((unsigned long)infoLabel.text.length== 0))
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter CVC"];
    }
    else if (((unsigned long)infoLabel.text.length== 0) && (((unsigned long)cardexpMnt == 0) || ((unsigned long)cardexpYr == 0)))
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter expiry details"];
    }
    else
    {
     
        checkMandatoryCard = YES;
        checkMandatoryCamera = YES;
    }
}

-(void)skipButtonClicked
{
    [self.view endEditing:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];

}

- (IBAction)doneButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if((_isComingFromPayment == 1) || (_isComingFromPayment == 2))
    {
        [self checkMandatoryFields];
        if(checkMandatoryCard)
        {
        [self callStripeForCreatingToken];
        
        }
    }
    else
    {
        [self checkMandatoryFields];
        if(checkMandatoryCard)
        {
            [self callStripeForCreatingToken];
        }
    }

}

-(void)callStripeForCreatingToken
{

    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Adding Card..."];
    self.stripeCard = [[STPCard alloc] init];
    self.stripeCard.number = _card.number;
    self.stripeCard.cvc = _card.cvc;
    self.stripeCard.expMonth = _card.expMonth;
    self.stripeCard.expYear = _card.expYear;
    
    [self performStripeOperation];
    
}
-(void)getcardWithToken:(NSString*)token
{
    [Stripe requestTokenWithID:token publishableKey:kPMDStripeTestKey completion:^(STPToken *token , NSError *error){

    }];
}
- (void)performStripeOperation {
    
    //1
    self.doneButton.enabled = NO;
    
    //2
    
    [Stripe createTokenWithCard:self.stripeCard publishableKey:kPMDStripeTestKey completion:^(STPToken *token , NSError *error){
        NSString *accesstoken = @"";
        if (error) {
            //handle strip error
            [Helper showAlertWithTitle:@"Message" Message:[error localizedDescription]];
            if(_isComingFromPayment == 2)
            {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
               
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
                
            }
            else {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }        }
        else if(token != nil) {
            accesstoken = token.tokenId;
            [self sendServiceAddCardsDetails:accesstoken];
            
        }
    }];
}

-(void)cancelButtonClicked
{
    if(_isComingFromPayment == 2)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - User Actions

- (void)scanCardClicked:(id)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [[[scanViewController navigationController]navigationBar] setBackgroundImage:[UIImage imageNamed:@"login_navigationbar"] forBarMetrics:UIBarMetricsDefault];
    scanViewController.navigationController.navigationBarHidden = NO;
    [self presentViewController:scanViewController animated:YES completion:nil];

}
+ (BOOL)canReadCardWithCamera
{
    return YES;
}


#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    TELogInfo(@"Scan succeeded with info: %@", info);
    // Do whatever needs to be done to deliver the purchased items.
    [self dismissViewControllerAnimated:YES completion:nil];
    TELogInfo(@"info.cardNumber : %@",info.cardNumber);
    self.infoLabel.text = [NSString stringWithFormat:@"%@", info.cardNumber/*info.redactedCardNumber*/];
    NSString *str = [NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear];
    str = [str substringFromIndex:2];
    self.expLabel.text = [NSString stringWithFormat:@"%02lu/%@",(unsigned long)info.expiryMonth,str];
    self.cvvLabel.text = [NSString stringWithFormat:@"%@",info.cvv];
    self.postalLabel.text = [NSString stringWithFormat:@"%02lu",(unsigned long)info.postalCode];
    _card.cvc = info.cvv;
    [self startWithCardNumber:info.cardNumber];
    [self startWithExpNo:self.expLabel.text];
    [self startWithCVVNumber:info.cvv];
    
    UIImageView *cardTypeImageView = nil;
    [cardTypeImageView setImage:[CardIOCreditCardInfo logoForCardType:info.cardType]];

}
-(void)startWithCardNumber:(NSString*)cardNumberString
{
    PTKCardNumber *cardNumber = [PTKCardNumber cardNumberWithString:cardNumberString];
    
    if ( ![cardNumber isPartiallyValid] )
        return;
    
    self.paymentView.cardNumberField.text = [cardNumber formattedStringWithTrail];
    
    [self.paymentView setPlaceholderToCardType];
    
    if ([cardNumber isValid]) {
        [self.paymentView textFieldIsValid:self.paymentView.cardNumberField];
        [self.paymentView stateMeta];
        
    } else if ([cardNumber isValidLength] && ![cardNumber isValidLuhn]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:YES];
        
    } else if (![cardNumber isValidLength]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:NO];
    }
    
}

-(void)startWithCVVNumber:(NSString*)CVVNumberString
{
    PTKCardCVC *CVVNumber = [PTKCardCVC cardCVCWithString:CVVNumberString];
    
    if ( ![CVVNumber isPartiallyValid] )
        return;
    
    // Only support shorthand year
    if ([CVVNumber formattedString].length > 3)
        return ;

    
    self.paymentView.cardCVCField.text = [CVVNumber formattedStringWithTrail];
    
//    [self.paymentView setPlaceholderToCardType];
    
    if ([CVVNumber isValid]) {
        [self.paymentView textFieldIsValid:self.paymentView.cardCVCField];
        [self.paymentView stateMeta];
        
//    } else if ([CVVNumber isValidWithType] && ![CVVNumber isValidLuhn]) {
//        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:YES];
//        
//    } else if (![CVVNumber isValidLength]) {
//        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:NO];
//    }
    }
}




-(void)startWithExpNo:(NSString*)expDetailsString
{
    PTKCardExpiry *cardExpiry = [PTKCardExpiry cardExpiryWithString:expDetailsString];
    
    if (![cardExpiry isPartiallyValid])
        return ;
    
    // Only support shorthand year
    if ([cardExpiry formattedString].length > 5)
        return ;
    
    self.paymentView.cardExpiryField.text = [cardExpiry formattedStringWithTrail];
    
    if ([cardExpiry isValid]) {
        [self.paymentView textFieldIsValid:self.paymentView.cardExpiryField];
        [self.paymentView stateCardCVC];
        
    } else if ([cardExpiry isValidLength] && ![cardExpiry isValidDate]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardExpiryField withErrors:YES];
    } else if (![cardExpiry isValidLength]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardExpiryField withErrors:NO];
    }
    
}
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    TELogInfo(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WebService call

-(void)sendServiceAddCardsDetails:(NSString *)token
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],
                             @"ent_dev_id":deviceId,
                             @"ent_token":token,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"addCard"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       TELogInfo(@"response %@",response);
                                       [self addCardDetails:response];
                                   }
                               }];
    

    
}

-(void)addCardDetails:(NSDictionary *)response
{
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        NSDictionary *dictResponse= [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
             _arrayContainingCardInfo = dictResponse[@"cards"];
            if (!_arrayContainingCardInfo || !_arrayContainingCardInfo.count){
                
            }
            else
            {
                [self addInDataBase];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
            }
            
            if(_isComingFromPayment == 2)
            {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
                
            }
            else {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
    
            }
        }
        else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1) {
            
            if(_isComingFromPayment == 2)
            {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [Helper showAlertWithTitle:@"Message" Message:dictResponse[@"errMsg"]];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
                                [Helper showAlertWithTitle:@"Message" Message:dictResponse[@"errMsg"]];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];

            }
            else {
                
                [Helper showAlertWithTitle:@"Message" Message:dictResponse[@"errMsg"]];
                [self.navigationController popViewControllerAnimated:YES];

            }
        }
    }
}

-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
   
   // if(isPresentInDBalready != 1)
    {
        for (int i =0; i<_arrayContainingCardInfo.count; i++)
        {
            // Entity *fav = _arrayContainingCardInfo[i];
            NSString *str = _arrayContainingCardInfo[i][@"id"];
                                                          
            [self checkCampaignIdAddedOrNot:str:i];
            if(isPresentInDBalready == 1)
            {
                
            }
            else
            {
                [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
            }
        }
    }
    
}

- (void)checkCampaignIdAddedOrNot:(NSString *)cardId :(int)arrIndex
{
    isPresentInDBalready = 0;
    NSArray *array = [Database getCardDetails];
    if ([array count]== 0)
    {
        // if(flag)
        //[self AddToFavButtonClicked:nil];
    }
    else
    {
        for(int i=0 ; i<[array count];i++)
        {
            Entity *fav = [array objectAtIndex:i];
            if ([fav.idCard isEqualToString:_arrayContainingCardInfo[arrIndex][@"id"]])
            {
                isPresentInDBalready = 1;
                
            }
        }
}
    
}




@end
