//
//  StatusView.h
//  iserve
//
//  Created by Rahul Sharma on 04/09/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusView : UIView
+ (id)sharedInstance;
-(void)updateValues:(BookingNotificationType )status;
@end
