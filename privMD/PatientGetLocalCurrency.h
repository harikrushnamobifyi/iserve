//
//  PatientGetLocalCurrency.h
//  UBER
//
//  Created by Rahul Sharma on 14/08/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientGetLocalCurrency : NSObject
+(NSString *)getCurrencyLocal :(float)amount;

@end
