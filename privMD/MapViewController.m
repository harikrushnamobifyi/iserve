
//  MapViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "MapViewController.h"
#import "PickUpViewController.h"
#import "XDKAirMenuController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import "PatientPubNubWrapper.h"
#import "AppointedDoctor.h"
#import "InvoiceViewController.h"
#import "PaymentViewController.h"
#import "CustomNavigationBar.h"
#import "NetworkHandler.h"
#import "UploadProgress.h"
#import <AFNetworking.h>
#import "LocationServicesMessageVC.h"
#import "Entity.h"
#import "Database.h"
#import "DirectionService.h"
#import <AXRatingView.h>
#import "UIImageView+WebCache.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PatientAppDelegate.h"
#import "RoundedImageView.h"
#import "MyAppTimerClass.h"
#import "ProArrivedView.h"
#import "DoctorDetailsViewController.h"
#import "DoctorSummaryViewController.h"
#import "DoctorDetailView.h"
#import "ChatVC.h"
#import "PatientViewController.h"
#import "AddressManageViewController.h"
#import "MKAnnotationView+WebCache.h"
#import "StatusView.h"
#import "SocketIOWrapper.h"

#define imgLinkForSharing @"http://104.131.96.96/guyidee/icon/Icon-Guide.png"

//Define Constants tag for my view
#define myProgressTag  5001
#define mapZoomLevel 16
#define pickupAddressTag 2500
#define addProgressViewTag 6000
#define msgShowingViewTag 7000
#define carType2TAG 7001
#define carType3TAG 7002
#define carType4TAG 7003
#define msgLabelTag 7004

#define trackingViewTag 8000

#define myCustomMarkerTag 5000
#define curLocImgTag 5001
#define driverArrivedViewTag 3000
#define driverMessageViewTag 3001
#define _height  110
#define _heightCenter  160
#define _width   274
#define _widthLabel   216

#define dividerViewTag 77
#define bottomViewWithCarTag 106
#define topViewTag 90
#define horizontalViewTag 4002
#define SubhorizontalViewTag 4003
#define lockLocationViewTag 4005
#define bookNowButtonTag 5
#define bookLaterButtonTag 6
#define detailsViewTag 300
#define statusTopViewTag 70
#define sliderBtnListTag1 79
static MapViewController *sharedInstance = nil;

enum doctorType {
    Now = 1,
    Later = 2
};

typedef enum
{
    isChanging = 0,
    isFixed = 1
}locationchange;

@interface MapViewController ()<SocketWrapperDelegate,CLLocationManagerDelegate,CustomNavigationBarDelegate,AddressManageDelegate,UITableViewDataSource,UITableViewDelegate> {
    
    GMSMapView *mapView_;
    BOOL firstLocationUpdate_;
    GMSGeocoder *geocoder_;
    PatientPubNubWrapper *pubNub;
    float desLat;
    float desLong;
    NSString *desAddr;
    NSString *desAddrline2;
    float srcLat;
    float srcLong;
    NSString *srcAddr;
    NSString *srcAddrline2;
    NSInteger carTypesForLiveBooking;
    NSInteger carTypesForLiveBookingServer;
    NSInteger paymentTypesForLiveBooking;
    //Path Plotting Variables
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    float newLat;
    float newLong;
    int count;
    float actual;
    float actualGMview;
    int docCount;
    float distanceinKm;
    double distanceOfClosetCar;
    BOOL isFromAddress;
    BOOL isFromTable;
    BOOL isFromPubNub;
    NSInteger carTypesArrayCountValue;
    NSString *shareUrlViaSMS;
    NSString *bookLaterDate;
    ProArrivedView *docArrivedView;
    DoctorDetailView *docOnTheView;
    StatusView *statusView;
    
    UIScrollView *scroller;
    UIButton *subScroller;
    
    NSInteger currentHorizontalDataIndex;
    NSMutableArray *allHorizontalData;
    NSMutableArray *alltypesImgstore;
    NSMutableArray *allSubHorizontalData;
    NSMutableArray *arrayOfButtons;
    NSMutableArray *arrayOfKilometer;
    NSMutableDictionary *markerViewDict;
    NSInteger typeButtonTag;
    UIButton *sliderBtn;
    
    SocketIOWrapper *socketWrapper;
    CGRect screenSize;

}

@property(nonatomic,strong) NSString *laterSelectedDate;
@property(nonatomic,strong) NSString *laterSelectedDateServer;
@property(nonatomic,strong) CustomNavigationBar *customNavigationBarView;
@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;
@property(nonatomic,strong) NSMutableDictionary *allMarkers;
@property(nonatomic,strong) WildcardGestureRecognizer * tapInterceptor;
@property(nonatomic,strong) UIActionSheet *actionSheet;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign) float currentLatitudeFare;
@property(nonatomic,assign) float currentLongitudeFare;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;
@property(nonatomic,strong) UIView *DropOffViewForConfirmScreen;
//Progress Indicator
@property(nonatomic,strong) UIProgressView * threadProgressView;
@property(nonatomic,strong) UIView *myProgressView;
@property(nonatomic,strong) UIView *addProgressBarView;
@property(nonatomic,strong) NSString *cardId;
@property(nonatomic,strong) NSString *subscribedChannel;
@property(nonatomic,assign)BOOL isDriverArrived;
@property(nonatomic,assign)BOOL isDriverOnTheWay;
//Path Plotting Variables
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,weak)NSTimer *pubnubStreamTimer;
@property(nonatomic,weak) NSTimer *pollingTimer;
@property(nonatomic,assign) double driverCurLat;
@property(nonatomic,assign) double driverCurLong;
@property(nonatomic,strong) NSDictionary *disnEta;
@property(nonatomic,strong) NSMutableArray *arrayOFDriverChannels;
@property(nonatomic,strong) NSMutableArray *arrayOfDriverEmail;
@property(nonatomic,strong) NSMutableArray *arrayOfCarTypes;
@property(nonatomic,strong) NSMutableArray *arrayOfSubCarTypes;
@property(nonatomic,strong) NSMutableArray *arrayOfCarColor;
@property(nonatomic,strong) NSMutableArray *arrayOfMastersAround;
@property(nonatomic,strong) NSArray *arrayOfCategoryImages;
@property(nonatomic,strong) NSDictionary *addressDetails;

@property(nonatomic,strong) UIView *spinnerView;

//suri
@property(nonatomic,assign)BOOL isBookingStatusCheckedOnce;
@property(nonatomic,assign)BOOL isSelectinLocation;
@property(nonatomic,assign)BOOL isAddressManuallyPicked;
@end

static int isLocationChanged;

@implementation MapViewController

@synthesize doctors;
@synthesize dictSelectedDoctor;
@synthesize threadProgressView;
@synthesize PikUpViewForConfirmScreen;
@synthesize DropOffViewForConfirmScreen;
@synthesize addProgressBarView;
@synthesize customNavigationBarView;
@synthesize cardId;
@synthesize laterSelectedDate;
@synthesize disnEta,pollingTimer,pubnubStreamTimer,subscribedChannel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

+ (instancetype) getSharedInstance
{
    return sharedInstance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isFromAddress = NO;
    // Do any additional setup after loading the view.
    
    screenSize = [[UIScreen mainScreen] bounds];
    sharedInstance = self;
    _patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    _patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    _serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDPublishStreamChannel];
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    [[TELogger getInstance] initiateFileLogging];
    newLat = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat]floatValue];
    newLong = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong]floatValue];
    carTypesForLiveBooking = 1;
    carTypesForLiveBookingServer = 0;
    isNowSelected = YES;
    isLaterSelected = NO;
    threadProgressView.progress = 0.0;
    
    
    _arrayOfMastersAround = [[NSMutableArray alloc]init];
    _arrayOfCategoryImages = @[@"categories_plumber",@"categories_electrician",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_plumber",@"categories_electrician",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_plumber",@"categories_electrician",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_plumber",@"categories_electrician",@"categories_handyman",@"categories_handyman",@"categories_handyman",@"categories_handyman"];
    
    CGRect rect = self.view.frame;
    rect.origin.y = -44;
    [self.view setFrame:rect];
    
    NSUserDefaults *ud =   [NSUserDefaults standardUserDefaults];
    double curLat = [[ud objectForKey:KNUCurrentLat] doubleValue];
    double curLong = [[ud objectForKey:KNUCurrentLong] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:curLat longitude:curLong zoom:mapZoomLevel];
    
    UIView *customMapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    CGRect rectMap = customMapView.frame;
    
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.settings.compassButton = YES;
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    [mapView_ clear];
    customMapView = mapView_;
    geocoder_ = [[GMSGeocoder alloc] init];
    [self.view addSubview:customMapView];
    
    [self addCustomNavigationBar];
    [self loadViewForHomeController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
    
    [socketWrapper connectSocket];
    socketWrapper.socketDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    // Ask for My Location data after the map has already been added to the UI.
    self.navigationController.navigationBarHidden = YES;
    
    [self subscribeToPassengerChannel];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizer:shouldReceiveTouch:)];
    
    [mapView_ addGestureRecognizer:tapRecognizer];
}
- (void) viewDidAppear:(BOOL)animated{
    
    [self addgestureToMap];
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:14];
        
    }
    
}
- (void)viewWillDisappear:(BOOL)animated{
    
    if (!_isSelectinLocation) {
        
        _isSelectinLocation = NO;
        
        //First unsubscribe to the channel you are listening to,then clear all the array contents so that the map will launch freshly
        [self unSubsCribeToPubNubChannels:_arrayOFDriverChannels];
        [_allMarkers removeAllObjects];
        [mapView_ clear];
        
        if (_arrayOFDriverChannels) {
            [_arrayOFDriverChannels removeAllObjects] ;
            _arrayOFDriverChannels = nil;
        }
        
        [self stopPubNubStream];
        _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
        [_myAppTimerClassObj stopPublishTimer];
        [_myAppTimerClassObj stopEtaNDisTimer];
        
        
    }
}
/**
 *  Stop the receiving data from pubnub server
 */
-(void)stopPubNubStream {
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    pubNub.delegate = nil;
    
}

-(void)loadViewForHomeController {
    
    
    [self getCurrentLocationFromGPS];
    
    UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
    [driverView removeFromSuperview];
    
    UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
    [driverMsgLabel removeFromSuperview];
    
    UIImageView *img = (UIImageView*)[self.view viewWithTag:curLocImgTag];
    img.hidden = NO;
    
    UIView *markerView =[self.view viewWithTag:myCustomMarkerTag];
    markerView.hidden = YES;
    markerView.userInteractionEnabled = YES;
    
    UIView *topContainerView =[self.view viewWithTag:topViewTag];
    topContainerView.hidden = NO;
    
    //Add custom navigation to the viewcontroller
    [self addCustomNavigationBar];
    
    NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:KUBERCarArrayKey]];
    
    _arrayOfCarTypes = [carTypes mutableCopy];
    if (_arrayOfCarTypes.count > 0)
    {
        [self createScrollerView];
        if (carTypesForLiveBookingServer == 0) {
            carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
        }
    }
    
    [self addLockLocationButton];
    [self publishPubNubStream];
    [self addCustomLocationView];
    
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj startPublishTimer];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PubNub Methods
#pragma mark - PubNubWrapperDelegate

-(void)subsCribeToPubNubChannel:(NSString*)channel{
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    [pubNub subscribeOnChannels:@[channel]];
    [pubNub setDelegate:self];
}

-(void)subsCribeToPubNubChannels:(NSArray *)channels{
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    [pubNub subscribeOnChannels:channels];
    [pubNub setDelegate:self];
}
-(void)unSubsCribeToPubNubChannels:(NSArray *)channels {
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    [pubNub unSubscribeOnChannels:channels];
    
    [pubNub setDelegate:self];
}

-(void)subscribeToPassengerChannel{
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    
    _patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    
    [pubNub subscribeOnChannels:@[_patientPubNubChannel]];
    
    [pubNub setDelegate:self];
}

-(void)checkDifferenceBetweenTwoLocationAndUpdateCache {
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLat longitude:newLong];
    
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:srcLat longitude:srcLong];
    
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    if (distance > 50000) {
        
        carTypesForLiveBookingServer = 0;
        carTypesForLiveBooking = 1;
        isComingTocheckCarTypes = NO;
        [mapView_ clear];
        newLat = srcLat;
        newLong = srcLong;
    } //what if he changes his city
    
    
}

/**
 *  publishSocketStream to receiving pro near around patient current location
 */
-(void)publishPubNubStream {
    
    NSDictionary *message = @{
                              @"btype":[NSNumber numberWithInt:3],
                              @"email": _patientEmail,
                              @"lat": [NSNumber numberWithDouble:srcLat],
                              @"long": [NSNumber numberWithDouble:srcLong],
                              
                              };
    
    socketWrapper = [SocketIOWrapper sharedInstance];
    [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:message];
}

-(void)messageA2ComeshereWithFlag1:(NSDictionary *)messageDict {
    
    [mapView_ clear];
    [self unSubsCribeToPubNubChannels:_arrayOFDriverChannels];
    [_arrayOFDriverChannels removeAllObjects] ;
    _arrayOFDriverChannels = nil;
    [_arrayOfDriverEmail removeAllObjects] ;
    _arrayOfDriverEmail = nil;
    [_allMarkers removeAllObjects] ;
    _allMarkers = nil;
    //[_arrayOfMastersAround removeAllObjects] ;
    _arrayOfMastersAround = nil;
    [self hideActivityIndicatorWithMessage];
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj stopSpinTimer];
    isFromPubNub = YES;
    [self changeMapMarker:typeButtonTag];
    
}

-(void)messageWithDataOfNearestPros:(NSDictionary *)messageDict {
    
    if (!_arrayOFDriverChannels) {
        _arrayOFDriverChannels = [[NSMutableArray alloc] init];
        _arrayOfMastersAround = messageDict[@"msg"][@"masArr"];
        for (int masArrIndex = 0;masArrIndex < _arrayOfMastersAround.count; masArrIndex++) {
            
            NSArray *channelsData = _arrayOfMastersAround[masArrIndex][@"mas"];
            for(NSDictionary *dict in channelsData){
                [_arrayOFDriverChannels addObject:dict[@"e"]];
                
            }
        }
        
        if (_arrayOfMastersAround.count > 0){

            doctors = [[NSMutableArray alloc] init];
            [doctors addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
        }
        if (doctors.count > 0) {
            
            id dis = _arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"d"];
            [self updateTheNearestDriverinSpinitCircle:dis];
            isFromPubNub = YES;
            [self changeMapMarker:typeButtonTag];
        }
        else {
            [self hideActivityIndicatorWithMessage];
        }
    }
    else {
        
        NSArray *arrayOfNewDriversAround = messageDict[@"msg"][@"masArr"];
        NSMutableArray *arrayNewChannels = [[NSMutableArray alloc] init];
        for (int masArrIndex = 0;masArrIndex < arrayOfNewDriversAround.count; masArrIndex++) {
            
            NSArray *channelsData = arrayOfNewDriversAround[masArrIndex][@"mas"];
            for(NSDictionary *dict in channelsData){
                [arrayNewChannels addObject:dict[@"e"]];
                
            }
        }
        _arrayOfMastersAround = [arrayOfNewDriversAround mutableCopy]; //update the array
        
        if (_arrayOfMastersAround.count > 0){
        NSArray *arr = [[NSArray alloc]initWithArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
        if (arr.count > 0) {
            id dis = _arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"d"];
            [self updateTheNearestDriverinSpinitCircle:dis];
        }
        else {
            [self hideActivityIndicatorWithMessage];
            [self addLockLocationButton];
        }
        }
    
        if (_arrayOFDriverChannels.count > arrayNewChannels.count) {
            
            NSMutableArray *channelToRemove = [[NSMutableArray alloc] init];
            //need to remove one channel
            for(NSString *channel in _arrayOFDriverChannels){
                if ([arrayNewChannels containsObject:channel]) {
                    //do nothing
                }
                else {
                    
                    //remove channel
                    [channelToRemove addObject:channel];
                    GMSMarker *marker =  _allMarkers[channel];
                    [self removeMarker:marker];
                    [_allMarkers removeObjectForKey:channel];
                }
            }
            
            //unsubscribe removed channels
            [_arrayOFDriverChannels removeObjectsInArray:channelToRemove];
            [self unSubsCribeToPubNubChannels:channelToRemove];
        }
        else if (_arrayOFDriverChannels.count < arrayNewChannels.count) {
            //need to add new channel
            
            NSMutableArray *channelsToAdd = [[NSMutableArray alloc] init];
            NSMutableArray *markerToAdd = [[NSMutableArray alloc] init];
            
            for(NSString *channel in arrayNewChannels){
                
                if ([_arrayOFDriverChannels containsObject:channel]) {
                    //do nothing
                }
                else {
                    //add channel and subscribe here
                    [channelsToAdd addObject:channel];
                    [markerToAdd addObjectsFromArray:arrayNewChannels];
                }
            }
            if (!doctors) {
                doctors = [[NSMutableArray alloc]init];
            }
            [doctors addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
            isFromPubNub = YES;
            [self changeMapMarker:typeButtonTag];
            [_arrayOFDriverChannels addObjectsFromArray:channelsToAdd];
            [self subsCribeToPubNubChannels:channelsToAdd];
        }
        else if (_arrayOFDriverChannels.count == arrayNewChannels.count) {
            
            if (!doctors) {
                doctors = [[NSMutableArray alloc]init];
            }
            [doctors addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
            [self addCustomMarkerFor];
        }
        
    }
}

-(void)messageA2ComeshereWithDriverEmails:(NSDictionary *)messageDict {
    
    //This will come inside only when the requesting is not happening when the user will request this will stop coming inside so that the array of driver email list will be static
    
    NSArray *emailData = messageDict[@"es"][carTypesForLiveBooking-1][@"em"];
    
    if(!_arrayOfDriverEmail) { //For the first time when the array in not initialised copy all the data from the array
        _arrayOfDriverEmail = [emailData mutableCopy];
    }
    if (emailData.count != _arrayOfDriverEmail.count) { //when array count from server changes
        
        if(_arrayOfDriverEmail) {
            [_arrayOfDriverEmail removeAllObjects];
            _arrayOfDriverEmail = nil;
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
        else {
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
        
    }
    else if (emailData.count == _arrayOfDriverEmail.count){ //when array count from server is same
        
        if ([emailData isEqualToArray:_arrayOFDriverChannels]) {
            
        }
        else { //if the cotent is not same just copy the new array from the server
            
            [_arrayOfDriverEmail removeAllObjects];
            _arrayOfDriverEmail = nil;
            _arrayOfDriverEmail = [emailData mutableCopy];
            
        }
    }
}

-(void)messageA2ComeshereWithCarTypes:(NSDictionary *)messageDict {
    
    NSArray *carTypesArray = messageDict[@"msg"][@"types"];
    
    if (carTypesArray.count > 0) {
        
        if(!_arrayOfCarTypes) { //For the first time when the array in not initialised copy all the data from the array
            _arrayOfCarTypes = [carTypesArray mutableCopy];
            carTypesForLiveBookingServer = [messageDict[@"types"][0][@"type_id"] integerValue];
            [self createScrollerView];
            
        }
        if (carTypesArray.count != _arrayOfCarTypes.count) { //when array count from server changes
            
            [_arrayOfCarTypes removeAllObjects];
            _arrayOfCarTypes = nil;
            _arrayOfCarTypes = [carTypesArray mutableCopy];
            [self clearPreviousViewAndVariables];
            [self createScrollerView];
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfCarTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        else if (carTypesArray.count == _arrayOfCarTypes.count){ //when array count from server is same
            
            if ([carTypesArray isEqualToArray:_arrayOfCarTypes]) {
                
            }
            else { //if the cotent is not same just copy the new array from the server
                
                [_arrayOfCarTypes removeAllObjects];
                _arrayOfCarTypes = nil;
                _arrayOfCarTypes = [carTypesArray mutableCopy];
                [self clearPreviousViewAndVariables];
                [self createScrollerView];
                [[NSUserDefaults standardUserDefaults] setObject:_arrayOfCarTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
    }
    else {
        
        if (!isComingTocheckCarTypes) {
            isComingTocheckCarTypes = YES;
            
            [_arrayOfCarTypes removeAllObjects];
            _arrayOfCarTypes = nil;
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray array] forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self createScrollerView];
            for (UIView *va in [self.view subviews]) {
                
                if (va.tag == bottomViewWithCarTag) {
                    [va removeFromSuperview];
                }
            }
        }
    }
}
bool isComingTocheckCarTypes;

-(void)clearPreviousViewAndVariables {
    
    for (UIView *va in [self.view subviews]) {
        
        if (va.tag == horizontalViewTag) {
            [va removeFromSuperview];
        }
    }
    
    [scroller removeFromSuperview];
    scroller = nil;
    
    [allHorizontalData removeAllObjects];
    allHorizontalData = nil;
}

-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)messageDict
{
//    NSLog(@"messageDict %@",messageDict);
    
    if ([[messageDict objectForKey:@"flag"] intValue] == 1) //No channels
    {
       // [self messageA2ComeshereWithFlag1:messageDict];
        
        //CarTypes Will come always irrespective of the flag Value
      //  [self messageA2ComeshereWithCarTypes:messageDict];
        
    }
    else if ([[messageDict objectForKey:@"flag"] intValue] == 0) //success
    {
        //CarTypes Will come always irresective of the flag Value
 //       [self messageA2ComeshereWithCarTypes:messageDict];
//        [self messageA2ComeshereWithDriverEmails:messageDict];
//        [self messageWithDataOfNearestPros:messageDict];

    }
    else{
        
    }
}

#pragma Mark- Path Plotting

/**
 *  Update Destination of an incoming Doctor
 *  Returns void and accept two arguments
 *  @param latitude  Doctor Latitude
 *  @param longitude incoming Doctor longitude
 */

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude {
    
    if (!_isPathPlotted) {
        
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        NSString *strImageUrl = [[NSUserDefaults standardUserDefaults] objectForKey:kOnGoingBookingDoctorProfileImageKey];
        UIImage *icon = [self customMarkerForAddingImages:_destinationMarker :strImageUrl];
        _destinationMarker.icon = icon;
        _destinationMarker.tappable = NO;
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count] > 1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
}

-(void)subscribeOnlyBookedDriver{
    
    /*    here array count is checking app goes background or not
     because at that point of time it content will be zero so get the value from user default
     
     */
    
    if (_arrayOFDriverChannels) {
        
        if (_arrayOFDriverChannels.count > 0) {
            
            NSMutableArray *channelToRemove = [[NSMutableArray alloc] init];
            
            for(NSString *channel in _arrayOFDriverChannels){
                if  ([subscribedChannel isEqualToString:channel]) {
                    //do nothing
                    
                }
                else {
                    [channelToRemove addObject:channel];
                    GMSMarker *marker =  _allMarkers[channel];
                    [self removeMarker:marker];
                    [_allMarkers removeObjectForKey:channel];
                }
            }
            [_arrayOFDriverChannels removeObjectsInArray:channelToRemove];
            [self unSubsCribeToPubNubChannels:channelToRemove];
            
        }
    }
    else {
        subscribedChannel = [[NSUserDefaults standardUserDefaults] objectForKey:@"subscribedChannel"];
        if (subscribedChannel.length != 0) {
            [self subsCribeToPubNubChannel:subscribedChannel];
        }
        else {
            // [self clearUserDefaultsAfterBookingCompletion];
        }
        
    }
}

-(void)didFailedToConnectPubNub:(NSError *)error {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:[error localizedDescription] On:self.view];
    [self hideAcitvityIndicator];
}

-(void)addgestureToMap
{
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
    {
        if (touchtype == 1)
        {
            [weakSelf startAnimation];
            
        }
        else {
            
            [weakSelf endAnimation];
        }
    };
    
    [mapView_  addGestureRecognizer:_tapInterceptor];
    
}


#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
}
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    _isSelectinLocation = YES;
    [self performSegueWithIdentifier:@"doctorDetails" sender:marker];
    return YES;
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    
}


- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    
    if ((bookingStatus == kNotificationTypeBookingOnMyWay) || (bookingStatus == kNotificationTypeBookingReachedLocation)) {
        return;
    }
    else{
        
        if(!isFromAddress){
            CGPoint point1 = mapView_.center;
            CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
            _currentLatitude = coor.latitude;
            _currentLongitude = coor.longitude;
            
            if (_isAddressManuallyPicked) {
                // _isAddressManuallyPicked = NO;
                return;
            }
            else {
                [self getAddress:coor];
                [self startSpinitypeButtonTagin];
            }
            
            
        }
    }
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar {
    
    if(!customNavigationBarView)
    {
        customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
        customNavigationBarView.delegate = self;
        [customNavigationBarView addTitleButton];
        [self.view addSubview:customNavigationBarView];
        
    }
}

-(void)addLockLocationButton {
    
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:vb]) {
        UIButton *lockLoctionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        lockLoctionBtn.tag = lockLocationViewTag;
        lockLoctionBtn.frame = CGRectMake(160-20.5,mapView_.frame.size.height/2-58 , 42, 59);
        [lockLoctionBtn setImage:[UIImage imageNamed:@"unlock_location"] forState:UIControlStateNormal];
        [lockLoctionBtn setImage:[UIImage imageNamed:@"lock_location"] forState:UIControlStateSelected];
        lockLoctionBtn.hidden = NO;
        lockLoctionBtn.selected = NO;
        [lockLoctionBtn addTarget:self action:@selector(lockButtonClickedonMap:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:lockLoctionBtn];
        
        
        UIButton *mappinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        mappinBtn.frame = CGRectMake(160-42,mapView_.frame.size.height/2+35 , 63, 77);
        
        [mappinBtn setImage:[UIImage imageNamed:@"mappin"] forState:UIControlStateNormal];
        mappinBtn.tag = 40;
        mappinBtn.hidden = YES;
        [self.view addSubview:mappinBtn];
        
    }
    
}

-(void)lockButtonClickedonMap:(UIButton *)sender {
    
    if (![sender isSelected]) {
        
        _isAddressManuallyPicked = YES;
        [sender setSelected:YES];
        
    }
    else {
        
        _isAddressManuallyPicked = NO;
        [sender setSelected:NO];
        
    }
}

-(void)addCustomLocationView
{
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:vb]) {
        
        CGFloat he = 64;
        
        if (subScroller) {
            he = 156;
        }
        else if (scroller) {
            
            he = 110;
        }
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0,64, 320,70)];
        customView.backgroundColor = [UIColor whiteColor];
        customView.tag = topViewTag;
        [customView setHidden:NO];
        customView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addressbar_bg"]];
        
        UIImageView *imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(6,24,30, 30)];
        imgStr.image =  [UIImage imageNamed:@"addressbar_mappin"];
        [customView addSubview:imgStr];
        
        
        UIImageView *line = [[UIImageView alloc]initWithFrame:CGRectMake(45,27,.4, 26)];
        line.image =  [UIImage imageNamed:@"addressbar_line"];
        [customView addSubview:line];
        
        
        UILabel *labelPickUp = [[UILabel alloc]initWithFrame:CGRectMake(53,14,120,20)];
        [Helper setToLabel:labelPickUp Text:@"JOB LOCATION" WithFont:OpenSans_Regular FSize:12 Color:UIColorFromRGB(0x999999)];
        // labelPickUp.textAlignment = NSTextAlignmentCenter;
        [customView addSubview:labelPickUp];
        
        
        UIButton *buttonAddressTextField = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonAddressTextField.frame = CGRectMake(53,38,210,22);
        
        
        _textFeildAddress = [[UITextField alloc]initWithFrame:CGRectMake(0,0,210,22)];
        [_textFeildAddress setTextColor:UIColorFromRGB(0x000000)];
        _textFeildAddress.placeholder = @"Location";
        _textFeildAddress.tag = 101;
        _textFeildAddress.enabled = NO;
        _textFeildAddress.delegate = self;
        
        [buttonAddressTextField addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        _textFeildAddress.font = [UIFont fontWithName:OpenSans_Regular size:15];
        [buttonAddressTextField addSubview:_textFeildAddress];
        [customView addSubview:buttonAddressTextField];
        
        
        
        UIImageView *line1 = [[UIImageView alloc]initWithFrame:CGRectMake(270,27,.5, 26)];
        line1.image =  [UIImage imageNamed:@"addressbar_line"];
        [customView addSubview:line1];
        
        UIButton *buttonCurrentLocation = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonCurrentLocation.frame = CGRectMake(275,22,35,35);
        buttonCurrentLocation.tag = 91;
        [buttonCurrentLocation setBackgroundImage:[UIImage imageNamed:@"addressbar_current_location"] forState:UIControlStateNormal];
        [buttonCurrentLocation setBackgroundImage:[UIImage imageNamed:@"addressbar_current_location"] forState:UIControlStateSelected];
        [buttonCurrentLocation addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
        
        [customView addSubview:buttonCurrentLocation];
        
        [self.view addSubview:customView];
        
        
        UIButton *bookLaterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        bookLaterButton.frame = CGRectMake(160,self.view.frame.size.height-50,160,50);
        [bookLaterButton setBackgroundImage:[UIImage imageNamed:@"booklater_btn_off"] forState:UIControlStateNormal];
        [bookLaterButton setBackgroundImage:[UIImage imageNamed:@"booknow_btn_on"] forState:UIControlStateSelected];
        bookLaterButton.tag = bookLaterButtonTag;
        [Helper setButton:bookLaterButton  Text:@"BOOK LATER" WithFont: OpenSans_Regular  FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:[UIColor whiteColor]];
        [bookLaterButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateSelected];
        
        [bookLaterButton addTarget:self action:@selector(bookLaterButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:bookLaterButton];
        
        
        
        UIButton *bookNowButton = [UIButton buttonWithType:UIButtonTypeCustom];
        bookNowButton.frame = CGRectMake(0,self.view.frame.size.height-50,160,50);
        [bookNowButton setBackgroundImage:[UIImage imageNamed:@"booknow_btn_off"] forState:UIControlStateNormal];
        
        bookNowButton.tag = bookNowButtonTag;
        
        [bookNowButton setBackgroundImage:[UIImage imageNamed:@"booknow_btn_on"] forState:UIControlStateSelected];
        [bookNowButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateSelected];
        [Helper setButton:bookNowButton  Text:@"BOOK NOW" WithFont: OpenSans_Regular  FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:[UIColor whiteColor]];
        [bookNowButton addTarget:self action:@selector(category1ButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.view addSubview:bookNowButton];
        
        
        UIView *divider = [[UIView alloc]initWithFrame:CGRectMake(159.5,self.view.frame.size.height, 1,50)];
        divider.backgroundColor = [UIColor grayColor];
        divider.tag = dividerViewTag;
        [self.view addSubview:divider];
    }
    
}


-(IBAction)addressButtonClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"toAddressVC" sender:sender];
}



-(void)category1ButtonAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"docSummaryVC" sender:sender];
    
    
}
-(void)bookLaterButtonClickAction:(UIButton *)sender {
    
    CGRect frame = self.datePickerView.frame;
    frame.origin.y = self.view.frame.size.height - self.datePickerView.frame.size.height;
    [_datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    [UIView animateWithDuration:0.5f animations:^{
        [self.view bringSubviewToFront:self.datePickerView];
        self.datePickerView.frame = frame;
        
    }];
    
    
}


- (void)changeDate:(UIDatePicker *)sender {
    
    NSLog(@"New Date: %@", sender.date);
}

- (IBAction)datePickerDoneButtonAction:(id)sender {
    
    UIButton *mBtn = (UIButton *)sender;
    
    if(mBtn.tag == 1){
        
        NSDate *myDate = _datePicker.date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMMM d YYYY, cccc, hh:mm aa"];
        bookLaterDate = [dateFormat stringFromDate:myDate];
        
        
    }
    CGRect frame = self.datePickerView.frame;
    frame.origin.y = 600;
    [UIView animateWithDuration:0.5f animations:^{
        
        self.datePickerView.frame = frame;
        
    }];
    
    [self performSegueWithIdentifier:@"proListViewController" sender:nil];
}



- (IBAction)cancelButtonClicked:(id)sender
{
    
    CGRect frame = self.datePickerView.frame;
    frame.origin.y = screenSize.size.height;
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.view bringSubviewToFront:_datePickerView];
        self.datePickerView.frame = frame;
        
    }];

    
    
    
    
}


/**
 *  after the completion of request submission the view in again bring into picture as it was hidden during the time of creating pikupviewcontroller
 */
-(void)requestSubmittedAddthelocationView
{
    UIView *LV = [self.view viewWithTag:topViewTag];
    [LV setHidden:NO];
    CGRect RTEC = LV.frame;
    RTEC.origin.y = 64;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         LV.frame = RTEC;
                     }
                     completion:^(BOOL finished){
                     }];
    [self startAnimation];
    
}

-(void)createScrollerView {
    
    carTypesArrayCountValue = _arrayOfCarTypes.count;
    if (carTypesArrayCountValue == 0) {
        
        [Helper showAlertWithTitle:@"Message" Message:@"There is no types available in your area."];
    }
    else
    {
        UIView *vb = [self.view viewWithTag:horizontalViewTag];
        NSArray *arr = self.view.subviews;
        
        if (![arr containsObject:vb]) {
            
            NSDictionary *type_name = [[NSDictionary alloc]init];
            if(!allHorizontalData)
            {
                allHorizontalData = [[NSMutableArray alloc]init];
                arrayOfKilometer = [[NSMutableArray alloc]init];
            }
            
            for(type_name in _arrayOfCarTypes)
            {
                [allHorizontalData addObject:flStrForObj([type_name objectForKey:@"tname"])];
                [arrayOfKilometer addObject:flStrForObj([type_name objectForKey:@"type_id"])];
                
            }
            
            if(!scroller){
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-166, 320,116)];
                scroller.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"categories_bg"]];
                scroller.tag = horizontalViewTag;
                scroller.delegate = self;
                [scroller setScrollEnabled:YES];
                
                scroller.showsHorizontalScrollIndicator = NO;
                [scroller setContentSize:CGSizeMake(allHorizontalData.count*107,116)];
                [self.view addSubview:scroller];
                
                //int xOffset=1;
                arrayOfButtons = [[NSMutableArray alloc]init];
                
                for (int i=0; i < _arrayOfCarTypes.count; i++)
                {
                    
                    UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                    buttonCategory.frame = CGRectMake(107*i,0,107,116);
                    
                    buttonCategory.tag=201+i;
                    [arrayOfButtons addObject:buttonCategory];
                    
                    [buttonCategory addTarget:self action:@selector(scrbuttonaction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if (i == 0)
                    {
                        
                        [buttonCategory setSelected:YES];
                        typeButtonTag = 0;
                        // [self changeMapMarker:i];
                        carTypesForLiveBookingServer = [type_name[@"type_id"]integerValue];
                        
                        
                    }
                    UIImageView *category = [[UIImageView alloc]initWithFrame:CGRectMake(23,28,60,60)];
                    category.image =  [UIImage imageNamed:[_arrayOfCategoryImages objectAtIndex:i]];
                    [buttonCategory setBackgroundImage:[UIImage imageNamed:@"categories_bg_selector"] forState:UIControlStateSelected];
                    
//                    [buttonCategory addTarget:self action:@selector(category1ButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    UILabel *kiloMeterLabel = [[UILabel alloc]initWithFrame:CGRectMake(23,4,60,20)];
                    [Helper setToLabel:kiloMeterLabel Text:[NSString stringWithFormat:@"%@ Kms",[arrayOfKilometer objectAtIndex:i]] WithFont:OpenSans_Regular FSize:14 Color:UIColorFromRGB(0x2a2a2a)];
                    kiloMeterLabel.textAlignment = NSTextAlignmentCenter;
                    
                    
                    UILabel *categoryName = [[UILabel alloc]initWithFrame:CGRectMake(0,92,107,20)];
                    [Helper setToLabel:categoryName Text:[NSString stringWithFormat:@"%@",[allHorizontalData objectAtIndex:i]] WithFont:OpenSans_Regular FSize:14 Color:UIColorFromRGB(0x2a2a2a)];
                    categoryName.textAlignment = NSTextAlignmentCenter;
                    
                    [buttonCategory addSubview:categoryName];
                    [buttonCategory addSubview:kiloMeterLabel];
                    [buttonCategory addSubview:category];
                    [scroller addSubview:buttonCategory];
                    [scroller bringSubviewToFront:buttonCategory];
                    
                    
                    
                    
                }
                
            }
            
            
        }
    }
    
}

-(void) scrbuttonaction:(UIButton *)sender {
    
    UIButton *srcBtn = (UIButton *) sender;
    [srcBtn setSelected:YES];
    
    for (int i=0; i<arrayOfButtons.count; i++)
    {
        UIButton *mBtn = [arrayOfButtons objectAtIndex:i];
        
        if ([srcBtn isEqual:mBtn]) {
            
            [mBtn setSelected:YES];
            typeButtonTag = mBtn.tag - 201;
            carTypesForLiveBooking = typeButtonTag+1;
            carTypesForLiveBookingServer = [_arrayOfCarTypes[typeButtonTag][@"type_id"]integerValue];
            isFromPubNub = NO;
            [self changeMapMarker:typeButtonTag];
            
            
        }
        else
        {
            [mBtn setSelected:NO];
            
        }
        
    }
}

-(void)createSubTypeScrollerView {
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    
    if(!subScroller)
    {
        subScroller = [UIButton buttonWithType:UIButtonTypeCustom];
        subScroller.frame = CGRectMake(0,scroller.frame.size.height+scroller.frame.origin.y,size.width ,46);
        subScroller.tag = SubhorizontalViewTag;
        subScroller.backgroundColor = [UIColor lightGrayColor];
        [subScroller setTitle:[allSubHorizontalData objectAtIndex:0] forState:UIControlStateNormal];
        [subScroller setTitleColor:[UIColor colorWithRed:(104.0/255.0) green:(110.0/255.0) blue:(118.0/255.0) alpha:1] forState:UIControlStateNormal];
        [subScroller setTitleColor:[UIColor colorWithRed:(104.0/255.0) green:(110.0/255.0) blue:(118.0/255.0) alpha:1] forState:UIControlStateSelected];
        [subScroller addTarget:self action:@selector(scrSubbuttonaction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:subScroller];
        
    }
    [_filterTableView reloadData];
    
}

-(void)scrSubbuttonaction {
    
    
    [self addSubTypesFilterTableView];
    
}

-(void)addSubTypesFilterTableView {
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    if (!_filterTableView) {
        
        _filterTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,156,size.width,size.height-156)];
        _filterTableView.delegate = self;
        _filterTableView.dataSource = self;
        [self.view addSubview:_filterTableView];
    }
    
}

-(void)startSpinitypeButtonTagin {
    
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj startSpinTimer];
    [self shoActivityIndicator];
}
-(void)updateTheNearestDriverinSpinitCircle:(id)disTance {
    
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj stopSpinTimer];
    [self hideAcitvityIndicator];
    id dis = disTance;
    distanceOfClosetCar = [dis floatValue];
    UILabel *msgLabel = (UILabel *)[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgShowingViewTag];
    [msgLabel setText:[NSString stringWithFormat:@"Nearest iserve is %.2f Miles Away.",[dis floatValue]/1000]];
    
}

-(void)hideAcitvityIndicator {
    
    UILabel *msgLabel = (UILabel *)[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgShowingViewTag];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = YES;
    indicatorView.hidden = YES;
}

-(void)shoActivityIndicator {
    
    UILabel *msgLabel = (UILabel *)[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgShowingViewTag];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = YES;
    indicatorView.hidden = YES;
    
}

-(void)hideActivityIndicatorWithMessage {
    
    NSArray *arr;
    if (_arrayOfMastersAround.count > 0) {
        
        arr = _arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"];
    }
    
    if (arr.count != 0) {
        MyAppTimerClass *ti = [MyAppTimerClass sharedInstance];
        [ti startSpinTimer];
    }
    else {
        
        UILabel *msgLabel = (UILabel *)[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgShowingViewTag];
        msgLabel.hidden = YES;
        [msgLabel setText:[NSString stringWithFormat:@"No iServe Available"]];
        //[Helper showAlertWithTitle:@"Message" Message:@"No Service Providers Available"];
        [self hideAcitvityIndicator];
        
    }
    
}

-(void)changeMapMarker:(NSInteger)type {
    
    [_allMarkers removeAllObjects];
    [mapView_ clear];
    doctors = [[NSMutableArray alloc] init];
    
    _temp = _arrayOfMastersAround[type][@"mas"];
    
    if(_temp.count == 0){
        
        
    }
    else{
        
        [doctors addObjectsFromArray:_arrayOfMastersAround[type][@"mas"]];
        
        if (doctors.count > 0) {
            
            if (doctors.count >3) {
                GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel+2];
                [mapView_ animateWithCameraUpdate:zoomCamera];
                //mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel+1];
                
            }
            else {
                GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel];
                [mapView_ animateWithCameraUpdate:zoomCamera];
                // mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
                
            }
            
            
            id dis = _arrayOfMastersAround[type][@"mas"][0][@"dis"];
            [self updateTheNearestDriverinSpinitCircle:dis];
            [self addCustomMarkerFor];
            
        }
        else {
            [self hideActivityIndicatorWithMessage];
        }
    }
}

/**
 *  This methods get called after calling Handletap method,it brings the main view to its original position
 */

- (void)closeAnimation
{
    [UIView animateWithDuration:0.5f animations:^{
        self.view.transform = CGAffineTransformIdentity;
    }];
}

- (void)closeTransparentView{
    
    UIView *viewToClose = [self.view viewWithTag:50];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    [viewToClose removeFromSuperview];
    
    [UIView commitAnimations];
    
}

- (void) changeCurrentLocation:(NSDictionary *)dictAddress{
    
    NSString *latitude = [dictAddress objectForKey:@"lat"];
    NSString *longitude = [dictAddress objectForKey:@"lng"];
    TELogInfo(@"selected lat and longitude :%@ %@",longitude,latitude);
    
    CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
    
}

- (void)custimizeMyLocationButton {
    
    UIButton *myLocationButton = (UIButton*)[[mapView_ subviews] lastObject];
    myLocationButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    CGRect frame = myLocationButton.frame;
    frame.origin.y = -310;
    frame.origin.x = -10;
    myLocationButton.frame = frame;
}

#pragma mark - Animations
- (void)startAnimation {
    
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = customNaviagtionBar.frame;
    rect2.origin.y = -44;
    customNaviagtionBar.frame = rect2;
    
    CGRect rect4;
    rect4.origin.y = 20;
    
    if (subScroller) {
        UIView *subScroll = [self.view viewWithTag:SubhorizontalViewTag];
        rect4 = subScroll.frame;
        rect4.origin.y = 46;
        subScroll.frame = rect4;
        rect4.origin.y = 92;
    }
    
    UIView *customLocationView = [self.view viewWithTag:topViewTag];
    CGRect rect = customLocationView.frame;
    rect.origin.y = rect4.origin.y;
    customLocationView.frame = rect;
    
    
    CGRect rect1 = statusView.frame;
    rect1.origin.y = rect4.origin.y;
    statusView.frame = rect;
    
    
    [UIView commitAnimations];
    
    
}

- (void)endAnimation{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = customNaviagtionBar.frame;
    rect2.origin.y = 0;
    customNaviagtionBar.frame = rect2;
    
    CGRect rect4;
    rect4.origin.y = 64;
    
    if (subScroller) {
        UIView *subScroll = [self.view viewWithTag:SubhorizontalViewTag];
        rect4 = subScroll.frame;
        rect4.origin.y = 110;
        subScroll.frame = rect4;
        rect4.origin.y = 156;
    }
    
    UIView *customLocationView = [self.view viewWithTag:topViewTag];
    CGRect rect = customLocationView.frame;
    rect.origin.y = rect4.origin.y;
    customLocationView.frame = rect;
    
    
    CGRect rect1 = statusView.frame;
    rect1.origin.y = rect4.origin.y;
    statusView.frame = rect;
    
    
    [UIView commitAnimations];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    
    [self menuButtonPressed:sender];
}

/**
 *  when the search button on the home screen is clicked
 *
 *  @param sender sender of the button whose tag is 91,This method is to make sure the location is not changing after picking up the fixing the pickup location
 */
-(void)searchButtonClicked:(id)sender
{
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    
    UIButton *mBtn = (UIButton *)sender;
    [self pickupLocationAction:mBtn];
}

- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)gotolistViewController
{
    _isSelectinLocation = NO;
    [self performSegueWithIdentifier:@"ListController" sender:self];
    
}

- (void)getCurrentLocation:(UIButton *)sender{
    
    CLLocation *location = mapView_.myLocation;
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    
    srcLat = _currentLatitude;
    srcLong = _currentLongitude;
    
    if (location) {
        
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
    
    
}

- (void)pickupLocationAction:(UIButton *)sender
{
    
    [self performSegueWithIdentifier:@"pickupFromHomeVC" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"pickupFromHomeVC"])
    {
        PickUpViewController *pickController = [segue destinationViewController];
        pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
        pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
        pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType){
            
            UIButton *lockLoctionBtn = (UIButton *)[self.view viewWithTag:lockLocationViewTag];
            lockLoctionBtn.selected = NO;
            [self lockButtonClickedonMap:lockLoctionBtn];
            
            _isAddressManuallyPicked = YES;
            
            NSString *addressText = flStrForStr(dict[@"address1"]);
            if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound) {
                addressText = [addressText stringByAppendingString:@","];
                addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
            } else {
                
                addressText = flStrForStr(dict[@"address2"]);
            }
            _textFeildAddress.text = addressText;
            srcAddr = flStrForStr(dict[@"address1"]);
            srcAddrline2 = flStrForStr(dict[@"address2"]);
            srcLat = [dict[@"lat"] floatValue];
            srcLong = [dict[@"lng"] floatValue];
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
            
        };
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
        apLocaion.currentLatitude = [NSNumber numberWithDouble:_currentLatitude];
        apLocaion.currentLongitude =  [NSNumber numberWithDouble:_currentLongitude];
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat];
        apLocaion.pickupLongitude = [NSNumber numberWithDouble:srcLong];
        apLocaion.dropOffLatitude = [NSNumber numberWithDouble:desLat];
        apLocaion.dropOffLongitude = [NSNumber numberWithDouble:desLong];
        apLocaion.srcAddressLine1 = flStrForObj(srcAddr);
        apLocaion.srcAddressLine2 = flStrForObj(srcAddrline2);
        apLocaion.desAddressLine1 = flStrForObj(desAddr);
        apLocaion.desAddressLine2 = flStrForObj(desAddrline2);
        
        _isSelectinLocation = YES;
        
        
    }else if([[segue identifier] isEqualToString:@"doctorDetails"]){
        
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat] ;
        apLocaion.pickupLongitude =  [NSNumber numberWithDouble:srcLong];
        apLocaion.srcAddressLine1 =  _textFeildAddress.text;
        apLocaion.srcAddressLine2 =  @"";//_textFeildAddress.text;
        
        DoctorDetailsViewController *dDetailController = [segue destinationViewController];
        dDetailController.appointmenttype = kAppointmentTypeLater;
        GMSMarker *marker = (GMSMarker*)sender;
        [apLocaion setDropOffLatitude:marker.userData[@"lt"]];
        [apLocaion setDropOffLongitude:marker.userData[@"lg"]];
        
        
    }
    
    else if([[segue identifier] isEqualToString:@"proListViewController"])
    {
        
        
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat] ;
        apLocaion.pickupLongitude =  [NSNumber numberWithDouble:srcLong];
        apLocaion.srcAddressLine1 =  _textFeildAddress.text;
        apLocaion.srcAddressLine2 =  srcAddrline2;//_textFeildAddress.text;
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:_currentLatitude];
        apLocaion.pickupLongitude = [NSNumber numberWithDouble:_currentLongitude];
        apLocaion.bookingType = kAppointmentTypeLater;
        
        
    }
    
    else if ([[segue identifier] isEqualToString:@"toAddressVC"])
    {
        AddressManageViewController *addVC = (AddressManageViewController *)[segue destinationViewController];
        self.navigationController.navigationBarHidden = YES;
        addVC.isComingFromVC = 1;
        addVC.addressManageDelegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"docSummaryVC"])
    {
        
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat] ;
        apLocaion.pickupLongitude =  [NSNumber numberWithDouble:srcLong];
        apLocaion.srcAddressLine1 =  _textFeildAddress.text;
        apLocaion.srcAddressLine2 =  srcAddrline2;//_textFeildAddress.text;
        apLocaion.pickupLatitude = [NSNumber numberWithDouble:_currentLatitude];
        apLocaion.pickupLongitude = [NSNumber numberWithDouble:_currentLongitude];
        apLocaion.bookingType = kAppointmentTypeNow;
        DoctorSummaryViewController *dDetailController = [segue destinationViewController];
        dDetailController.appointmentType = kAppointmentTypeNow;
    }
    
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate{
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *addresstext = [address.lines componentsJoinedByString:@","];
            
            UIView *customView = [self.view viewWithTag:topViewTag];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:101];
            textAddress.text =  addresstext; //[response.firstResult.lines objectAtIndex:0];
            //  UIView *customView1 = [self.view viewWithTag:150];
            UITextField *pickUpAddress= (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed))
            {
                pickUpAddress.text = addresstext; //[response.firstResult.lines objectAtIndex:0];
            }
            pickUpAddress.text = addresstext;
            srcLat = response.firstResult.coordinate.latitude;
            srcLong = response.firstResult.coordinate.longitude;
            srcAddr = pickUpAddress.text;
            srcAddrline2 = flStrForObj(response.firstResult.subLocality);
            
        }
        
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate completionHandler:handler];
    
}

#pragma mark UITextField Delegate -
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)getCurrentLocationFromGPS {
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
            {
                [self.locationManager requestAlwaysAuthorization];
            }
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    
    if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        if (!_isUpdatedLocation) {
            
            [_locationManager stopUpdatingLocation];
            
            //change flag that we have updated location once and we don't need it again
            _isUpdatedLocation = YES;
            
            [_allMarkers removeAllObjects];
            [mapView_ clear];
            
        }
    }
    else {
        if(!isFromAddress){
            
            if (!_isUpdatedLocation) {
                _isUpdatedLocation = YES;
                [_locationManager stopUpdatingLocation];
                
                isFromAddress = NO;
                //change map camera postion to current location
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                        longitude:newLocation.coordinate.longitude
                                                                             zoom:mapZoomLevel];
                [mapView_ setCamera:camera];
                
                
                //add marker at current location
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
                //save current location to plot direciton on map
                _currentLatitude = newLocation.coordinate.latitude;
                _currentLongitude =  newLocation.coordinate.longitude;
                
                //get address for current location
                [self getAddress:position];
                
                MyAppTimerClass *obj = [MyAppTimerClass sharedInstance];
                [obj startPublishTimer];
                
            }
        }
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        [self gotoLocationServicesMessageViewController];
    }
    TELogInfo(@"locationManager failed to update location : %@",[error localizedDescription]);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:[error localizedDescription] On:self.view];
    
}

-(void)locationServicesChanged:(NSNotification*)notification{
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized){
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController{
    
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

/**
 *  swithc between appointment type
 *
 *  @param sender button
 */
- (void)checkDoctorsAvailability:(UIButton *)sender{
    
    if (sender.tag == 107) // Now
    {  //now
        
        isNowSelected = YES;
        isLaterSelected = NO;
        
        [sender setSelected:isNowSelected];
        
        UIButton *laterButton =  (UIButton*)[(UIView*)[sender superview] viewWithTag:108];
        [laterButton setSelected:isLaterSelected];
        
        
    }
    else if(sender.tag == 108) //Later
    {
        isNowSelected = NO;
        isLaterSelected = YES;
        
        [sender setSelected:isLaterSelected];
        
        UIButton *nowButton =  (UIButton*)[(UIView*)[sender superview] viewWithTag:107];
        [nowButton setSelected:isNowSelected];
    }
    
}


/**
 *  will return the Appointment Type
 *
 *  @return Appointment type Nor or Later
 */
-(AppointmentType)getSelectedAppointmentType {
    
    if (isNowSelected) {
        return  kAppointmentTypeNow;
    }
    
    return kAppointmentTypeLater;
    
}

/**
 *  Plots Marker on Map
 *
 *  @param medicalSpecialist doctor or nurse
 */
- (void)addCustomMarkerFor
{
    if (!_allMarkers) {
        
        _allMarkers = [[NSMutableDictionary alloc] init];
    }
    
    
    for (int i = 0; i < doctors.count; i++)
    {
        
        NSDictionary *dict = doctors[i];
        float latitude = [dict[@"lt"] floatValue];
        float longitude = [dict[@"lg"] floatValue];
        TELogInfo(@"In addCustomMarkerFor Outside: %@",doctors);
        if (!_allMarkers[dict[@"e"]]) {
            TELogInfo(@"In addCustomMarkerFor Inside coming First Time: %@ \n ",_allMarkers[dict[@"e"]]);
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.userData = dict;//dict[@"e"];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            marker.tappable = YES;
            marker.flat = YES;
            marker.groundAnchor = CGPointMake(0.5f, 0.5f);
            NSString *se = dict[@"i"];
            UIImage *image = [self customMarkerForAddingImages:marker :se];
            marker.icon = image;
            marker.map = mapView_;            
            [_allMarkers setObject:marker forKey:dict[@"e"]];
        }
        else {
            
            GMSMarker *marker = _allMarkers[dict[@"e"]];
        
            [CATransaction begin];
            [CATransaction setAnimationDuration:2.0];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            [CATransaction commit];
            marker.flat = YES;

        }
        
        
    }
    
    [doctors removeAllObjects];
    
}

-(UIImage *)customMarkerForAddingImages:(GMSMarker *)marker :(NSString *)serverImgLink {
    
    
    
    UIImage *markerImage = [UIImage imageNamed:@"circular_map_icon"];
    
    UIView *markerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 52, 65)];
    markerView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0];
    
    UIImageView *markerImageViewOuter = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 52, 65)];
    markerImageViewOuter.image = markerImage;
    [markerView addSubview:markerImageViewOuter];
    
    UIImageView *markerImageViewInner = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,45,45)];
    markerImageViewInner.image = [UIImage imageNamed:@"signup_image_user"];
    markerImageViewInner.layer.cornerRadius = 45/2;
    markerImageViewInner.layer.masksToBounds = YES;
    markerImageViewInner.clipsToBounds = YES;
    CGPoint cen = markerView.center;
    cen.y = cen.y - 5.7;
    [markerImageViewInner setCenter:cen];
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,serverImgLink];
    [markerView addSubview:markerImageViewInner];
    
    if (!markerViewDict) {
        markerViewDict = [[NSMutableDictionary alloc]init];
    }
    [markerViewDict setObject:markerView forKey:strImageUrl];
    
    __weak typeof(self) weakSelf = self;
    
    
    [markerImageViewInner sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                            placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       if (!image) {
                                           image = [UIImage imageNamed:@"menu_profile_default_image"];
                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf updateMarkerImagesAfterDownloadCompletion:marker getImageFromServer:image :imageURL];
                                           
                                       });
                                       
                                   }];
    
    UIImage *icon = [self imageWithView:markerView];
    
    return icon;
    
}
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque = NO, 0.0);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

-(void)updateMarkerImagesAfterDownloadCompletion:(GMSMarker *)marker getImageFromServer:(UIImage *)image :(NSURL *)urlOfImage {
    
    NSString *imgUrlStr = [urlOfImage absoluteString];
    
    UIView *markerView = [markerViewDict objectForKey:imgUrlStr];
    
    UIImage *image2=  [self imageWithView:markerView];
    markerView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0];
    marker.icon = image2;
    
}



-(void)removeMarker:(GMSMarker*)marker{
    
    marker.map = mapView_;
    marker.map = nil;
}


-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERONTHEWAY" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERREACHED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBSTARTED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBCOMPLETED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBookingConfirmationNameKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLocationServicesChangedNameKey object:nil];
    
}


-(void)getAddressFromDatabase:(NSDictionary *)addressDetails {
    
    _addressDetails = [addressDetails mutableCopy];
    
    UIView *customView = [self.view viewWithTag:topViewTag];
    UITextField *textAddress = (UITextField *)[customView viewWithTag:101];
    textAddress.text =  _addressDetails[@"address1"];
    
    //save current location to plot direciton on map
    srcLat = [[_addressDetails objectForKey:@"latitude"] doubleValue];
    srcLong = [[_addressDetails objectForKey:@"longitude"] doubleValue];
    isFromAddress = YES;
    //get address for current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[_addressDetails objectForKey:@"latitude"] doubleValue] longitude:[[_addressDetails objectForKey:@"longitude"] doubleValue] zoom:15];
    
    [mapView_ setCamera:camera];
    
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    XDKAirMenuController *controller = [XDKAirMenuController sharedMenu];
    if (controller.isMenuOpened) {
        [controller closeMenuAnimated];
    }
    return YES;
}



@end
