//
//  MODAVerifyiMobileViewController.h
//  MODA
//
//  Created by Rahul Sharma on 2/29/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"

@interface VerifyiMobileViewController : UIViewController

@property(weak, nonatomic) IBOutlet UITextField *firstNumber;
@property(weak, nonatomic) IBOutlet UITextField *secondNumber;
@property(weak, nonatomic) IBOutlet UITextField *thirdNumber;
@property(weak, nonatomic) IBOutlet UITextField *fourthNumber;
@property(weak, nonatomic) IBOutlet UITextField *fifthNumber;

@property (strong, nonatomic) UIImage *pickedImage;
@property (strong, nonatomic) NSArray *saveSignUpDetails;

-(IBAction)sendButtonClicked:(id)sender;

@end
