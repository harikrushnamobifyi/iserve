//
//  Update&SubscribeToPubNub.m
//  GuydeeSlave
//
//  Created by Rahul Sharma on 26/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "Update&SubscribeToPubNub.h"
#import "PatientPubNubWrapper.h"

static Update_SubscribeToPubNub *updateAndSubscribeToPubnub;

@interface Update_SubscribeToPubNub () <PatientPubNubWrapperDelegate>


@property (strong, nonatomic) PatientPubNubWrapper *pubNub;
@end

@implementation Update_SubscribeToPubNub
@synthesize pubNub;

+(instancetype)sharedInstance
{
    if (!updateAndSubscribeToPubnub) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            updateAndSubscribeToPubnub = [[self alloc] init];
        });
    }
    
    return updateAndSubscribeToPubnub;
}

-(instancetype)init {
    
    if (self == [super init]) {
        
        pubNub = [PatientPubNubWrapper sharedInstance];
        
    }
    
    return self;
}


#pragma mark - PubNub Methods

-(void)subsCribeToPubNubChannels:(NSArray *)channels{
    
    [pubNub subscribeOnChannels:channels];
    [pubNub setDelegate:self];
}

-(void)unSubsCribeToPubNubChannels:(NSArray *)channels{
    
    [pubNub unSubscribeOnChannels:channels];
    [pubNub setDelegate:self];
}

/*
 
 SUBSCRIBE TO YOUR OWN CHANNEL SO THAT YOU CAN LISTEN TO SERVER DATA AND DOCTOR DATA FROM PUBNUB
 
 */
-(void)subscribeToPassengerChannel {
    
    //pubnub initilization and subscribe to patient channel
    
    
    NSString *patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    
    [pubNub subscribeOnChannels:@[patientPubNubChannel]];
    
    [pubNub setDelegate:self];
    
}

/**
 *  Stop the receiving data from pubnub server
 */
-(void)stopPubNubStream {
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    pubNub.delegate = nil;
    
}


-(void)publishPubNubStream:(NSDictionary *)parameters {
    
    NSDictionary *message = [parameters mutableCopy];
    
    [pubNub sendMessageAsDictionary:message toChannel:kPMDPublishStreamChannel];
        
}

-(void)recievedMessage:(NSDictionary *)messageDict onChannel:(NSString *)channelName {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(recievedMessageWithDictionary:OnChannel:)]) {

        [self.delegate recievedMessageWithDictionary:messageDict OnChannel:channelName];
        
    }

}

@end
