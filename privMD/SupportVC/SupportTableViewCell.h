//
//  SupportTableViewCell.h
//  DallasPoshPassenger
//
//  Created by Rahul Sharma on 14/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportTableViewCell : UITableViewCell

@property(strong,nonatomic) UIImageView *accessoryImage;
@property(strong,nonatomic) UILabel *titleLabel;
@property(strong,nonatomic) UIImageView *cellBgImage;

@end
