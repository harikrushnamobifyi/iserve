//
//  AppConstants.h
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//




#pragma mark - enums

typedef enum {
    
    kNotificationTypeBookingAccept = 2,
    kNotificationTypeBookingOnMyWay = 5,
    kNotificationTypeBookingReachedLocation = 6,
    kNotificationTypeBookingStarted = 7,
    kNotificationTypeBookingComplete = 8,
    kNotificationTypeBookingReject = 12
    
}BookingNotificationType;

typedef enum CancelBookingReasons :NSUInteger{
    
    kCAPassengerDoNotShow = 4,
    kCAWrongAddressShown = 6,
    kCAPassengerRequestedCancel = 5,
    kCADoNotChargeClient = 7,
    kCAOhterReasons = 8,
    
}CancelBookingReasons;

typedef enum {
    kPubNubStartStreamAction = 1,
    kPubNubStopStreamAction = 2,
    kPubNubUpdatePatientAppointmentLocationAction = 3,
    kPubNubStartDoctorLocationStreamAction = 5,
    kPubNubStopDoctorLocationStreamAction  = 6
}PubNubStreamAction;


typedef enum {
    kAppointmentTypeNow = 3,
    kAppointmentTypeLater = 4
}AppointmentType;


#pragma mark - Constants
//eg: give prifix kPMD

extern NSString *const kPMDPublishStreamChannel;
extern NSString *const kPMDPubNubPublisherKey;
extern NSString *const kPMDPubNubSubcriptionKey;
extern NSString *const kPMDGoogleMapsAPIKey;
extern NSString *const kPMDFlurryId;
extern NSString *const kPMDTestDeviceidKey;
extern NSString *const kPMDDeviceIdKey;
extern NSString *const KUBERCarArrayKey;
extern NSString *const kPMDStripeTestKey;
extern NSString *const kPMDStripeLiveKey;

#pragma mark - mark URLs

//Base URL

extern NSString *BASE_URL;
extern NSString *BASE_URL_SUPPORT_WEB;
extern NSString *BASE_URL_UPLOADIMAGE;
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;
//Methods
extern NSString *MethodPatientSignUp;
extern NSString *MethodPatientLogin;
extern NSString *MethodDoctorUploadImage;
extern NSString *MethodPassengerLogout;
extern NSString *MethodFareCalculator;


#pragma mark - ServiceMethods
// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMCancelAppointment;
extern NSString *const kSMCancelOngoingAppointment;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;

//Request Params For SignUp
extern NSString *KDASignUpFirstName;
extern NSString *KDASignUpLastName;
extern NSString *KDASignUpMobile;
extern NSString *KDASignUpEmail;
extern NSString *KDASignUpPassword;
extern NSString *KDASignUpCountry;
extern NSString *KDASignUpCity;
extern NSString *KDASignUpDeviceType;
extern NSString *KDASignUpDeviceId;
extern NSString *KDASignUpAddLine1;
extern NSString *KDASignUpAddLine2;
extern NSString *KDASignUpPushToken;
extern NSString *KDASignUpZipCode;
extern NSString *KDASignUpAccessToken;
extern NSString *KDASignUpDateTime;
extern NSString *KDASignUpCreditCardNo;
extern NSString *KDASignUpCreditCardCVV;
extern NSString *KDASignUpCreditCardExpiry;
extern NSString *KDASignUpTandC;
extern NSString *KDASignUpPricing;
extern NSString *KDASignUpLatitude;
extern NSString *KDASignUpLongitude;


//Request Params For Login

extern NSString *KDALoginEmail;
extern NSString *KDALoginPassword;
extern NSString *KDALoginDeviceType;
extern NSString *KDALoginDevideId;
extern NSString *KDALoginPushToken;
extern NSString *KDALoginUpDateTime;

//Request for Upload Image
extern NSString *KDAUploadDeviceId;
extern NSString *KDAUploadSessionToken;
extern NSString *KDAUploadImageName;
extern NSString *KDAUploadImageChunck;
extern NSString *KDAUploadfrom;
extern NSString *KDAUploadtype;
extern NSString *KDAUploadDateTime;
extern NSString *KDAUploadOffset;

//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;
extern NSString *KDAProfilePic;
extern NSString *KDAFbProfilePicURL;
extern NSString *isFromFB;

#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU

extern NSString *const kNSUPatientPubNubChannelkey;
extern NSString *const kNSUAppoinmentDoctorDetialKey;
extern NSString *const kNSUPatientEmailAddressKey;
extern NSString *const kNSUMongoDataBaseAPIKey;
extern NSString *const kNSUIsPassengerBookedKey;
extern NSString *const kNSUPassengerBookingStatusKey;
extern NSString *const kNSUPatientCouponkey;

#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN



#pragma mark - Controller Keys

#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;
extern NSString *const kNotificationLocationServicesChangedNameKey;
extern NSString *const kNotificationBookingConfirmationNameKey;
#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;

extern NSString *const KNUCurrentLat;
extern NSString *const KNUCurrentLong;
extern NSString *const KNUserCurrentCity;
extern NSString *const KNUserCurrentState;
extern NSString *const KNUserCurrentCountry;

extern NSString *const KUDriverEmail;
extern NSString *const KUBookingDate;
extern NSString *const KUBookingID;

#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadDoctorNameKey;
extern NSString *const kPNPayloadDoctorRatingKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadDoctorEmailKey;
extern NSString *const kPNPayloadDoctorContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadAppoinmentDateStringKey;
extern NSString *const kPNPayloadAppoinmentLatitudeKey;
extern NSString *const kNotificationBookingConfirmationNameKey;
extern NSString *const kPNPayloadDoctorChannelKey;

#pragma mark - OnGoingBooking Payload Keys

extern NSString *const kOnGoingBookingDoctorEmailKey;
extern NSString *const kOnGoingBookingDoctorBookingDate;
extern NSString *const kOnGoingBookingDoctorSubscribedChannelKey;
extern NSString *const kOnGoingBookingDoctorNameKey;
extern NSString *const kOnGoingBookingDoctorPhoneNumberKey;
extern NSString *const kOnGoingBookingDoctorRatingKey;
extern NSString *const kOnGoingBookingDoctorProfileImageKey;




