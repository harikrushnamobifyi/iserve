//
//  TableViewController.m
//  Sup
//
//  Created by Rahul Sharma on 3/5/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import "CountryNameTableViewController.h"
#import "CountryPicker.h"
#import "CountryPickerCell.h"

@interface CountryNameTableViewController ()

@property (strong,  nonatomic) NSString *code;
@property (weak, nonatomic) UIImage *flagCountry;
@property(strong, nonatomic) NSArray *countryName;
@property (strong, nonatomic) NSArray *countrycode;
@property (strong, nonatomic) NSMutableArray *countryNameFirstString;
@property (strong, nonatomic) NSMutableArray *globalyAccesableArray;
@end


@implementation CountryNameTableViewController
@synthesize details;
@synthesize countrycode;
@synthesize oncomplete;
@synthesize countryNameFirstString;
NSArray *countryNames;
NSArray *searchResults;

#pragma marks - table view life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    countryNameFirstString = [[NSMutableArray alloc]init];
    self.globalyAccesableArray = [[NSMutableArray alloc] init];
    details = [CountryPicker countryNames];
    countrycode = [CountryPicker countryCodes];
    
    //for loop is used for getting the first letter of the country
    //it will store the character in to the array (CountryNameFirstString)
    for (int i=0; i<[details count]-1; i++)
    {
        if([[details[i] substringToIndex:1] isEqualToString:[details[i+1] substringToIndex:1]])
        {
            
        }
        else
        {
            [countryNameFirstString addObject:[details[i] substringToIndex:1]];
        }
        if(i==[details count]-2)
        {
            [countryNameFirstString addObject:[details[i] substringToIndex:1]];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationItem setTitle:NSLocalizedString(@"Country Code", @"Country Code")];
}



-(IBAction)cancelView:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  used this method for Sorting an array
 *
 *  @param index used for the generated array
 *
 *  @return list of shorted characters
 */
- (NSArray *)shortArray:(int)index{
    
    NSString *charater = countryNameFirstString[index];
    
    NSPredicate *samplePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF beginswith[c] '%@'",charater]];
    
    NSArray *listOfCharacters = [details filteredArrayUsingPredicate:samplePredicate];
    return listOfCharacters;
}


#pragma mark - Table view data source and delegate methods

/*used for returning the number of sections used in the table view
 *@param take values and generate sections
 *
 * @return number of sections
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
        
    {
        return 1;
    }
    return [countryNameFirstString count];
}



/**
 *  used for giving the title for header in section
 *
 *  @param tableView view will be generated
 *  @param section   number of sections
 *
 *  @return generate the first letter title for section
 */
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return nil;
    }
    return [countryNameFirstString objectAtIndex:section];
}


/**
 *  filter all countries begin with searched text given by user
 *
 *  @param searchText string provided by the user
 *  @param scope button pressed by user
 */
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF beginswith[c] '%@'",searchText]];
    if (searchResults == nil)
    {
        searchResults = [[NSArray alloc]init];
    }
    searchResults = [details filteredArrayUsingPredicate:resultPredicate];
}
/**
 *  number of rows are going to be in the table
 *
 *  @param section numbers of section in whole view
 *
 *  @return number of counts of the sections
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [searchResults count];
    }
    else
    {
        return [[self shortArray:(int)section] count];
    }
}



- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return countryNameFirstString;
}



- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    
    return [countryNameFirstString indexOfObject:title];
}



-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *countryNameList;
    countryNameList = [[NSMutableArray alloc] init];
    NSString *stringCountryCode;
    static NSString *CellIdentifier = @"myCell";
    
    CountryPickerCell *cell = (CountryPickerCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CountryPickerCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        [countryNameList addObjectsFromArray:searchResults];
        
        stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[countryNameList
                                                                              objectAtIndex:indexPath.row]];
        [self.globalyAccesableArray removeAllObjects];
        [self.globalyAccesableArray addObjectsFromArray:searchResults];
        
    }
    else
    {
        
        countryNameList = [[self shortArray:(int)indexPath.section] mutableCopy];
        stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[countryNameList
                                                                              objectAtIndex:indexPath.row]];
        [self.globalyAccesableArray removeAllObjects];
        [self.globalyAccesableArray addObjectsFromArray:countryNameList];
    }
    
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@",stringCountryCode];
    self.flagCountry = [UIImage imageNamed:imagePath];
    cell.imageFlag.image = self.flagCountry;
    cell.labelCountryName.text = [NSString stringWithFormat:@"%@",countryNameList[indexPath.row]];
    NSDictionary *countrycodes = [CountryPicker dictionaryWithCountryCodes];
    cell.labelCountryCode.text = [NSString stringWithFormat:@"+ %@", countrycodes[[NSString stringWithFormat:@"%@",stringCountryCode]]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* //call back method
     * pass Country Code and flag image whenever this method is called.
     */
    if (oncomplete)
    {
        if(tableView != self.searchDisplayController.searchResultsTableView )
        {
            _globalyAccesableArray = [[self shortArray:(int)indexPath.section] mutableCopy];
        }
        NSString *stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[ self.globalyAccesableArray objectAtIndex:indexPath.row]];
        
        NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", stringCountryCode];
        NSLog(@"%@",stringCountryCode);
        UIImage *flagImage  = [UIImage imageNamed:imagePath];
        NSDictionary *countrycodes = [CountryPicker dictionaryWithCountryCodes];
        NSString *code = countrycodes[[NSString stringWithFormat:@"%@",stringCountryCode]];
        
        NSDictionary *countryNames = [CountryPicker countryNamesByCode];
        NSString *countryName = countryNames[[NSString stringWithFormat:@"%@",stringCountryCode]];
        
        oncomplete(code,flagImage,countryName);
        
        [self dismissViewControllerAnimated:YES completion:nil]; 
    }
}
- (IBAction)backButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
