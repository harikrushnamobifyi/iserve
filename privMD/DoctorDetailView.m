//
//  WindowOverlayButton.m
//  MyAlbum
//
//  Created by Surender Rathore on 28/02/14.
//
//

#import "DoctorDetailView.h"
#import "AppointedDoctor.h"
#import "RoundedImageView.h"
#import <AXRatingView/AXRatingView.h>
#import "UIImageView+WebCache.h"

@interface DoctorDetailView ()

@property (strong, nonatomic) RoundedImageView *profilepic;
@property (strong, nonatomic) UILabel *doctorName;
@property (nonatomic,strong) UILabel *appoinmentTime;

@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *doctorNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *navigateButton;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;

- (IBAction)callButtonClickedAction:(id)sender;

@end

@implementation DoctorDetailView

@synthesize callback;
@synthesize viewToaddd;

#define DeafultText @"Please Wait..."

#define _height  150

static DoctorDetailView *docOnTheWayView = nil;

+ (id)sharedInstance {
    
	if (!docOnTheWayView) {
        
        CGSize size = [[UIScreen mainScreen] bounds].size;
        
		docOnTheWayView  = [[self alloc] initWithFrame:CGRectZero];
        docOnTheWayView.frame = CGRectMake(10, size.height-150, 300, _height);
        docOnTheWayView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"details_bg"]];
        [docOnTheWayView createViewToShowDoctorDetails];
	}
	return docOnTheWayView;
}

-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self){
          self =  [[[NSBundle mainBundle] loadNibNamed:@"DoctorDetails" owner:self options:nil]objectAtIndex:0];
    }
    return self;
}

-(void)createViewToShowDoctorDetails
{
    //get doctor detail
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
//    viewToaddd = [[UIView alloc]initWithFrame:CGRectMake(0,0, 320, _height)];
//    viewToaddd.backgroundColor = [UIColor colorWithWhite:0.922 alpha:1.000];
//    viewToaddd.tag = 4004;
//    [self addSubview:viewToaddd];
    
//    
//    _profilepic = [[RoundedImageView alloc] initWithFrame:CGRectMake(15, 10, 80, 80)];
//    _profilepic.image = [UIImage imageNamed:@"signup_image_user.png"];
//    [viewToaddd addSubview:_profilepic];
//    
//    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,[ud objectForKey:@"driverpic"]];
//
//    [_profilepic sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
//                         placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
//                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//                                }];
//    
//    _doctorName = [[UILabel alloc] initWithFrame:CGRectMake(115, 10, 200, 20)];
//    [Helper setToLabel:_doctorNameLabel Text:flStrForStr([ud objectForKey:@"drivername"]).uppercaseString WithFont:Roboto_Regular FSize:17 Color:UIColorFromRGB(0x006699)];
//    [viewToaddd addSubview:_doctorName];
    
//    _appoinmentTime = [[UILabel alloc] initWithFrame:CGRectMake(115, 35, 200, 20)];
//    [Helper setToLabel:_appoinmentTime Text:@"" WithFont:Roboto_Regular FSize:12 Color:[UIColor blackColor]];
//    [viewToaddd addSubview:_appoinmentTime];
    
    
    
    _doctorNameLabel.text = flStrForStr([ud objectForKey:@"drivername"]).uppercaseString;
    _doctorNameLabel.textAlignment = NSTextAlignmentLeft;
    
    
    float rating = [[ud objectForKey:@"Rating"] floatValue];
    
    
    _ratingView.stepInterval = 0.0;
    _ratingView.highlightColor = [UIColor colorWithRed:0.044 green:0.741 blue:1.000 alpha:1.000];
    _ratingView.userInteractionEnabled = NO;
    [_ratingView sizeToFit];
    _ratingView.value = rating;
   
    
    
    
    
}

-(void)updateValues {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    _doctorNameLabel.text = flStrForStr([ud objectForKey:@"drivername"]).uppercaseString;
    _doctorNameLabel.textAlignment = NSTextAlignmentLeft;
    
    float rating = [[ud objectForKey:@"Rating"] floatValue];
    
    _ratingView.value = rating;

    
}

-(void)updateDistance:(NSString*)distane estimateTime:(NSString*)eta {
    
    _distance.text = [NSString stringWithFormat:@"Distance : %@",distane];
    
    _time.text = [NSString stringWithFormat:@"Time : %@",eta];
    
}

- (NSDateFormatter *)serverformatter {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}

- (NSDateFormatter *)dateformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM yyyy";
    });
    return formatter;
}

- (NSDateFormatter *)timeformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"hh:mm:ss a";
    });
    return formatter;
}

- (IBAction)callButtonClickedAction:(id)sender {
    
    NSString *number = [[NSUserDefaults standardUserDefaults]objectForKey:@"DriverTelNo"];
    NSString *cleanedString = [[number componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:telURL])
    {
        [[UIApplication sharedApplication] openURL:telURL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This function is only available in iPhone."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
}
@end
