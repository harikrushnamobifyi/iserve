//
//  Addresscell.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 15/04/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Addresscell : UITableViewCell


/**
 *  used to store address
 */
@property (strong, nonatomic) IBOutlet UILabel *addressLabel1;

/**
 *  used to store sub address
 */
@property (strong, nonatomic) IBOutlet UILabel *addressLabel2;

/**
 *  used to remove the address from Database
 */
@property (strong, nonatomic) IBOutlet UIButton *removeAddressButton;
@end
