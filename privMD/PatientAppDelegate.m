//
//  PatientAppDelegate.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PatientAppDelegate.h"
#import "HelpViewController.h"
#import "PatientViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AppointedDoctor.h"
#import "InvoiceViewController.h"
#import "PMDReachabilityWrapper.h"
#import <AudioToolbox/AudioToolbox.h>
#import "XDKAirMenuController.h"
#import "Flurry.h"
#import "RateViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "NetworkStatusShowingView.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "MapViewController.h"
#import "ViewController.h"

@interface PatientAppDelegate()

@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) PatientViewController *splashViewController;

@end

@implementation PatientAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static UIView *mySharedView;

+ (void) initialize {
    mySharedView = [[UIView alloc]initWithFrame:CGRectMake(0, 430, 320, 50)];
    mySharedView.backgroundColor = [[UIColor redColor]colorWithAlphaComponent:0.5];
}
void uncaughtExceptionHandler(NSException *exception) {
    
    TELogInfo(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    TELogInfo(@"Stack trace: %@", [exception callStackSymbols]);
    TELogInfo(@"desc: %@", [exception description]);
    TELogInfo(@"name: %@", [exception name]);
    TELogInfo(@"user info: %@", [exception userInfo]);
}
//use NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
    [self setupAppearance];
    
    //save device id
    NSString *uuidSting =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:uuidSting forKey:kPMDDeviceIdKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry setCrashReportingEnabled:YES];
    
    // Replace YOUR_API_KEY with the api key in the downloaded package
    [Flurry startSession:kPMDFlurryId];

    [Fabric with:@[[Crashlytics class]]];
    
    //Network Check
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.reachabilityManager startMonitoring];
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     
     {
         TELogInfo(@"Point 2 :Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 [NetworkStatusShowingView removeViewShowingNetworkStatus];

             }
             break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
             {
                 [NetworkStatusShowingView sharedInstance];
             }
             break;
         }
     }];
    
    NSLog(@"launchOptions didFinishLaunchingWithOptions %@",launchOptions);
    
    [self handlePushNotificationWithLaunchOption:launchOptions];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo[@"aps"][@"badge"]) {
        
        application.applicationIconBadgeNumber = [userInfo[@"aps"][@"badge"] integerValue];
        
    }
    else {
        
         application.applicationIconBadgeNumber = 0;
    }
    
   [self handleNotificationForUserInfo:userInfo];
    
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
        //device is simulator
         [[NSUserDefaults standardUserDefaults]setObject:@"garbagevalue" forKey:KDAgetPushToken];
    }
    else {
         [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
        //for testing
        
    }
   
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	TELogInfo(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
}

-(void)gotocontrollerFortesting
{
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RateViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"rateVC"];
    
    UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
    [naviVC pushViewController:invoiceVC animated:YES];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLocationServicesChangedNameKey object:nil userInfo:nil];
    
    // Use Reachability to monitor connectivity
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
   
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    TELogInfo(@"applicationWillTerminate");
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerBookingStatusKey];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    [self saveContext];
}

#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions {
    
    TELogInfo(@"handle push launchOptions == %@ ",launchOptions);
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    if (remoteNotificationPayload) {
        
            TELogInfo(@"handle push %@ remoteNotificationPayload == %@",remoteNotificationPayload);
        //check if user is logged in
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            TELogInfo(@"handle push %@ remoteNotificationPayload KDAcheckUserSessionToken");

            [self handleNotificationForUserInfo:remoteNotificationPayload];

        }
    }
}

-(void)noPushForceChangingController:(NSDictionary *)userInfo :(int)type{

    NSDictionary *dictionary = [userInfo mutableCopy];
    
    if(type == kNotificationTypeBookingOnMyWay)
    {
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"email"] forKey:KUDriverEmail];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"apptDt"] forKey:KUBookingDate];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"apptId"] forKey:@"bookingid"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"fName"] forKey:@"drivername"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"pPic"] forKey:@"driverpic"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"mobile"] forKey:@"DriverTelNo"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"chn"] forKey:@"subscribedChannel"];

        //Status Key For Changing content on HomeViewController
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingOnMyWay forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERONTHEWAY" object:nil userInfo:nil];
        
    }
    
    else if(type == kNotificationTypeBookingReachedLocation){
        
        //Status Key For Changing content on HomeViewController
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isReviewSubmited"];
        [[NSUserDefaults standardUserDefaults ] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingReachedLocation forKey:@"STATUSKEY"];

        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERREACHED" object:nil userInfo:nil];
    }
    else if(type == kNotificationTypeBookingStarted){
        
        //Status Key For Changing content on HomeViewController
    
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBSTARTED" object:nil userInfo:nil];
    }

    else if (type == kNotificationTypeBookingComplete){ //
        
        NSDictionary *dictionary = [userInfo mutableCopy];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        
        
        //Status Key For Changing content on HomeViewController
        NSString *email = [ud objectForKey:kOnGoingBookingDoctorEmailKey];
        NSString *bookingDate = [ud objectForKey:kOnGoingBookingDoctorBookingDate];
        if (email.length == 0) {
            email = dictionary[@"e"];
            bookingDate = dictionary[@"d"];
        }
        
        [ud setInteger:kNotificationTypeBookingComplete forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        ViewController *controller = (ViewController *)[(UINavigationController *)self.window.rootViewController visibleViewController];
        controller.definesPresentationContext = YES;
        
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        InvoiceViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
        invoiceVC.doctorEmail = email;
        invoiceVC.appointmentDate = bookingDate;
        invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        [naviVC presentViewController:invoiceVC animated:YES completion:nil];

        
    } //completed
    else if (type == kNotificationTypeBookingReject){
        
       _cancelReason  = [dictionary[@"r"] integerValue];
        
        NSString *cancelStatus = [self getCancelStatus:_cancelReason];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel Report" message:[NSString stringWithFormat:@"%@",cancelStatus] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [alert show];
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    }
    else if (type == 11) {
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    }
    
}

-(NSString*)getCancelStatus:(CancelBookingReasons)statusCancel {
  
    NSString *cancelStatus;
    TELogInfo(@"status of cancel %d",statusCancel);
    switch (statusCancel) {
        case kCAPassengerDoNotShow:
            cancelStatus = @"Passenger not came.";
            break;
        case kCAWrongAddressShown:
            cancelStatus = @"Address wrong.";
            break;
        case kCAPassengerRequestedCancel:
            cancelStatus = @"Passenger asked to cancel.";
            break;
        case kCADoNotChargeClient:
            cancelStatus = @"Donot charge customer.";
            break;
        case kCAOhterReasons:
            cancelStatus = @"Others.";
            break;
        default:
            cancelStatus = @"Others.";
            break;
    }

    return cancelStatus;
}
-(void)playNotificationSound{
    
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms-received" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}

-(void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
    [self playNotificationSound];
   
    int type = [userInfo[@"aps"][@"nt"] intValue];
    type = type + 2;
   
    if (type == kNotificationTypeBookingAccept) {
        
    }
    else if(type == kNotificationTypeBookingOnMyWay)
    {
        BOOL isCameFrom = [[NSUserDefaults standardUserDefaults]boolForKey:kNSUIsPassengerBookedKey];
        if (!isCameFrom) {
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNSUIsPassengerBookedKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        }
        else if (!isAlreadyBooked){
            //already completed
        }
        else {
            
            NSDictionary *dictionary = userInfo[@"aps"];
            //Local Notification to notifyi HomeViewController
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            
            if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
                
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];

                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
                        
                    });
                    
                });
            }
            else{
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
            }
            
        }

    }
    else if(type == kNotificationTypeBookingReachedLocation){
        
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingReachedLocation];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        }
        else if (!isAlreadyBooked){
            //already completed
        }
        else {
        
            NSDictionary *dictionary = userInfo[@"aps"];

        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        
        if (![menu.currentViewController isKindOfClass:[MapViewController class]]) {
            NSLog(@"Push Message handleNotificationForUserInfo MapViewController");

            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Confirmed" object:nil userInfo:dictionary];
        }
        else{
            NSLog(@"Push Message handleNotificationForUserInfo MapViewController else");

          [[NSNotificationCenter defaultCenter] postNotificationName:@"Confirmed" object:nil userInfo:dictionary];
        }
        }
    } //kNotificationTypeBookingReachedLocation
    else if (type == kNotificationTypeBookingStarted) {
        
        
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingStarted];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked) {
            //do nothing
        }
        else if (!isAlreadyBooked){
            //already completed
        }
        else {
        
            BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingComplete];
            BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
            
            if (isAlreadyCame && isAlreadyBooked) {
                //do nothing
            }
            else if (!isAlreadyBooked){
                //already completed
            }
            else {
                NSDictionary *dictionary = userInfo[@"aps"];
                [self gotoInvoiceScreen:dictionary];
            }

        }
        
    }
    
    else if (type == kNotificationTypeBookingComplete){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    
    }
    else if(type == kNotificationTypeBookingReject){
        
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        
        BOOL isDriverCancel = [[NSUserDefaults standardUserDefaults]boolForKey:@"isDriverCanceledBookingOnce"];
        if (!isDriverCancel) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDriverCanceledBookingOnce"];
        }
        if (isDriverCancel && isAlreadyBooked) {
            
            NSDictionary *dictionary = userInfo[@"aps"];
            [self noPushForceChangingController:dictionary :kNotificationTypeBookingReject];
        }
    }
    
}

-(void)gotoInvoiceScreen :(NSDictionary *)detailsDict {
    
    //Status Key For Changing content on HomeViewController
    TELogInfo(@"InvoiceViewController Push detailsDict %@ ",detailsDict);
    [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingComplete forKey:@"STATUSKEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InvoiceViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
    invoiceVC.doctorEmail = detailsDict[@"e"];
    invoiceVC.appointmentDate = detailsDict[@"d"];
   
    
    UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
    
    if (![[naviVC.viewControllers lastObject] isKindOfClass:[InvoiceViewController class]])
    {
        [naviVC pushViewController:invoiceVC animated:YES];
        
    }

}

-(BOOL)checkCurrentStatus:(NSInteger)Status {
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    NSInteger newBookingStatus =   Status;
    
    if(bookingStatus != newBookingStatus && bookingStatus < newBookingStatus)
    {
        return NO;
    }
    else{
        return YES;
    }
}
-(BOOL)checkIfBokkedOrNot {
    
    BOOL isBooked = [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsPassengerBookedKey];
    
    if(isBooked)
    {
        return YES;
    }
    else{
        return NO;
    }
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            TELogInfo(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark -
#pragma mark Core Data stack



// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
                TELogInfo(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)setupAppearance {
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_navigationbar"] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_Regular size:17], NSFontAttributeName,
                                UIColorFromRGB(0xffffff), NSForegroundColorAttributeName, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
   
}

#pragma mark - Facebook Login

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
