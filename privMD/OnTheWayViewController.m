//
//  OnTheWayViewController.m
//  iserve
//
//  Created by Rahul Sharma on 4/6/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "OnTheWayViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "PatientPubNubWrapper.h"
#import "DirectionService.h"
#import "StatusView.h"
#import "DoctorDetailView.h"
#import "ProArrivedView.h"
#import "InvoiceViewController.h"
#import "MyAppTimerClass.h"

#define mapZoomLevel 16

@interface OnTheWayViewController ()<PatientPubNubWrapperDelegate>
{
    PatientPubNubWrapper *pubNub;
    //Path Plotting Variables
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    StatusView *statusViewObject;
    DoctorDetailView *proDetailsViewObject;
    ProArrivedView *proArrivedViewObject;
    
}

@property(nonatomic,retain) CLLocationManager *locationManager;


@property(nonatomic,assign) BookingNotificationType bookingStatus;
@property(nonatomic,strong) NSString *patientEmail;
@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,assign) BOOL isPathPlotted;
@property(nonatomic,assign) double driverCurLat;
@property(nonatomic,assign) double driverCurLong;
@property(nonatomic,assign) double currentLatitude;
@property(nonatomic,assign) double currentLongitude;
@property(nonatomic,assign) CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong) GMSMarker *destinationMarker;
@property(nonatomic,strong)NSString *startLocation;

@end

@implementation OnTheWayViewController

#pragma Mark UI Methods

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonActionForNavigation:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)backButtonActionForNavigation:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)testingValues {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"abhishek@gmail.com" forKey:KUDriverEmail];
    [[NSUserDefaults standardUserDefaults] setObject:@"12-04-2016 12:12:12" forKey:KUBookingDate];
    [[NSUserDefaults standardUserDefaults] setObject:@"2020" forKey:@"bookingid"];
    [[NSUserDefaults standardUserDefaults] setObject:@"Abhishek" forKey:@"drivername"];
    [[NSUserDefaults standardUserDefaults] setObject:@"image70" forKey:@"driverpic"];
    [[NSUserDefaults standardUserDefaults] setObject:@"7878787878" forKey:@"DriverTelNo"];
    [[NSUserDefaults standardUserDefaults] setObject:@"bcmc" forKey:@"subscribedChannel"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (NSString *)deviceLocation
{
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", _locationManager.location.coordinate.latitude, _locationManager.location.coordinate.longitude];
    return theLocation;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self testingValues];
    
    self.navigationItem.title = [NSString stringWithFormat:@"Job ID : \n %@",@"1234"];
    //Initialize current location
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [_locationManager startUpdatingLocation];
    
    [self createNavLeftButton];
    
    _patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    _serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDPublishStreamChannel];
    _patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    
    //Initialize Map
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_locationManager.location.coordinate.latitude longitude:_locationManager.location.coordinate.longitude zoom:mapZoomLevel];
    
    _mapView.camera = camera;
    _mapView.delegate = self;
    _mapView.settings.myLocationButton = YES;
    _mapView.myLocationEnabled = YES;
    
    [self subscribeToPassengerChannel];
    _bookingStatus = kNotificationTypeBookingOnMyWay-1;
    
     [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(change) userInfo:nil repeats:YES];
    
}
-(void)change {
    
    _bookingStatus = _bookingStatus + 1;
    
    if (_bookingStatus > 9) {
        _bookingStatus = kNotificationTypeBookingOnMyWay-1;
    }
    
    [self changeContentOfPresentController:_bookingStatus];
}

-(void)viewWillAppear:(BOOL)animated {
    
    PatientAppDelegate *shared =  (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    [shared setupAppearance];
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    _bookingStatus = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma Mark PubNub Methods

-(void)subscribeToPassengerChannel {
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    
    [pubNub subscribeOnChannels:@[_patientPubNubChannel]];
    
    [pubNub setDelegate:self];
}

#pragma mark - PubNub Publish and Response

-(void)recievedMessage:(NSDictionary*)messageDict onChannel:(NSString*)channelName
{
    NSInteger bookingStatus = [messageDict[@"a"] integerValue];
    
    if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        
        _driverCurLat = [messageDict[@"lt"] doubleValue];
        _driverCurLong = [messageDict[@"lg"] doubleValue];
        
        [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
        
        return;
    }
    
    if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        
        [self changeContentOfPresentController:bookingStatus];
        
    }
    else if(bookingStatus == kNotificationTypeBookingComplete) {
        
        // Need to open Invoice Pop up
        // call method gotoInvoiceViewController
        
    }
    
    
}

#pragma Mark UIForCurrentView BasedOn Booking Status

-(void)changeContentOfPresentController:(BookingNotificationType)bookingStatus {
    
    if (bookingStatus == kNotificationTypeBookingOnMyWay) {
        
        //Add Status View
        statusViewObject = [StatusView sharedInstance];
        [statusViewObject updateValues:bookingStatus];
        [self.view addSubview:statusViewObject];
        
        //Add ProDetails View
        proDetailsViewObject = [DoctorDetailView sharedInstance];
        [proDetailsViewObject updateValues];
        [self.view addSubview:proDetailsViewObject];
        
        
        //Plotting and Live Tracking of Pro
        
        _currentLatitude  =  13.4321;
        _currentLongitude =  77.3455;
        
        if (!(_currentLatitude == 0 && _currentLongitude == 0)) {
            
            [self setStartLocationCoordinates:_currentLatitude Longitude:_currentLongitude];
            
        } // Appointment Location
        _driverCurLat = 13.76;
        _driverCurLong = 77.454;
        
        if (!(_driverCurLat == 0 && _driverCurLong == 0)) {
            
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_driverCurLat,_driverCurLong);
            
            GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:position];
            
            [_mapView animateWithCameraUpdate:locationUpdate];
            
            [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
            
        } // Doctor current location from server will update with a4 and a5 a6 a7
        
        [self sendRequestgetETAnDistance];
        
        MyAppTimerClass *obj = [MyAppTimerClass sharedInstance];

        [obj startEtaNDisTimer]; //from Mytimer class
        
        
    }
    else if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {

        
        //Add Status View
        [statusViewObject removeFromSuperview];
        statusViewObject = [StatusView sharedInstance];
        [statusViewObject updateValues:bookingStatus];
        [self.view addSubview:statusViewObject];

        [proDetailsViewObject removeFromSuperview];
        
        //Add ProDetails View
        proArrivedViewObject = [ProArrivedView sharedInstance];
        [proArrivedViewObject updateValues];
        [self.view addSubview:proArrivedViewObject];
        
    }
    else if (bookingStatus == kNotificationTypeBookingComplete) {
        
        //Open Invoice Popup
        
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        InvoiceViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
        invoiceVC.doctorEmail = @"abhishek@mobifyi.com";
        invoiceVC.appointmentDate = @"2016-04- 29 12:00:00";
        invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.navigationController presentViewController:invoiceVC animated:YES completion:nil];
        
    }
    else {
        
        //Add Status View
        statusViewObject = [StatusView sharedInstance];
        [statusViewObject updateValues:bookingStatus];
        if (![[self.view subviews] containsObject:statusViewObject]) {
            
            [self.view addSubview:statusViewObject];
            
        }
        
       
        
       
    }
    
}
#pragma Mark Web Services

-(void)checkOngoingAppointmentStatus {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading Appointment Detail.."];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":currentDate,
                             @"ent_user_type":@"2",
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getApptStatus"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       
                                       [self checkOngoingAppointmentStatusResponse:response];
                                   }
                               }];
    
}

-(void)checkOngoingAppointmentStatusResponse:(NSDictionary *)responseDict{
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if (responseDict == nil) {
        
        return;
    }
    else if ([responseDict objectForKey:@"Error"])
    {
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDict objectForKey: @"Error"]];
    }
    else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else if ([responseDict[@"errFlag"] intValue] == 1 || ([responseDict[@"errNum"] intValue] == 49)) {
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDict objectForKey: @"errMsg"]];
        
    }
    else {
        
        
    }
}


#pragma  Mark Path Plotting and Driver Location Update

-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude {
    
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:mapZoomLevel];
    [_mapView setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = _mapView;
    
    [waypoints_ addObject:marker];
    
    marker.icon = [UIImage imageNamed:@"finished_tracking_green_pin_map_icon"];
    
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
}



-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude {
    
    if (!_isPathPlotted) {
        
        TELogInfo(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = _mapView;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        
        UIImage *icon = [UIImage imageNamed:@"finished_tracking_red_pin_map_icon"];
        _destinationMarker.icon = icon;
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count] > 1){
            
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        // CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        
    }
}
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        [polyline setStrokeWidth:3.0];
        [polyline setStrokeColor:UIColorFromRGB(0x4399ca)];
        polyline.map = _mapView;
        
    }
    
    
}

#pragma Mark - Calculate Distance

- (void) sendRequestgetETAnDistance {
    
    if (_driverCurLat == 0 || _driverCurLong == 0 || _currentLatitude == 0 || _currentLongitude == 0) {
        
        [proDetailsViewObject updateDistance:@"..." estimateTime:@"..."];
        return;
    }
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=AIzaSyDzFpzMhM014fzTzOErkt3M7nKRnzwz1hs",_driverCurLat,_driverCurLong,_currentLatitude,_currentLongitude];
    
    NSURL *url = [NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strUrl]];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(addressResponse:)];
    
}

#pragma mark Webservice Handler(Response) -

- (void)addressResponse:(NSDictionary *)response{
    
    if (!response) {
        
        //  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // [alertView show];
        
    }else if ([response objectForKey:@"Error"]){
        //[Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else{
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        
        if ([[dictResponse objectForKey:@"statusNumber"] intValue] == 0)
        {
            NSArray *routes =  response[@"ItemsList"][@"routes"];
            if( routes.count != 0 )
            {
                NSString *distance = response[@"ItemsList"][@"routes"][0][@"legs"][0][@"distance"][@"value"];
                float dis = [distance integerValue]/1609.34;
                distance = [NSString stringWithFormat:@"%.2f Miles",dis];
                NSString *eta =  response[@"ItemsList"][@"routes"][0][@"legs"][0][@"duration"][@"text"];
                
                [proDetailsViewObject updateDistance:distance estimateTime:eta];
            }
            else
            {
                
            }
        }
        
    }
}


@end
