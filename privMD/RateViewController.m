//
//  RateViewController.m
//  privMD
//
//  Created by Rahul Sharma on 10/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "RateViewController.h"
#import <AXRatingView/AXRatingView.h>
#import "NetworkHandler.h"
#import "ViewController.h"

#define kDescriptionPlaceholder @"Please share your experience with us, it will help us provide you better service."

@interface RateViewController ()

@property(nonatomic,assign) int favStatus;
@property(nonatomic,strong) AXRatingView *ratingViewStars;
@property(nonatomic,assign) float rating;

@end

@implementation RateViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_reviewTextView becomeFirstResponder];
    if (![_reviewText isEqualToString:kDescriptionPlaceholder]) {
        
        _reviewTextView.text = _reviewText;
    }
    
    
}

-(IBAction)backButtonClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)doneButtonClicked:(id)sender {
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)dismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TextView Delegate methods

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.textColor == [UIColor lightGrayColor]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = kDescriptionPlaceholder;
        [textView resignFirstResponder];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView hasText]) {
        textView.text = textView.text;
    }
    else {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = kDescriptionPlaceholder;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            
            textView.text = @"";
            
        }
       //code for review to come here
        if (_delegate && [_delegate respondsToSelector:@selector(hasTextChanges:)]) {
            [_delegate hasTextChanges:textView.text];
        }
        [self doneButtonClicked:nil];
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

@end
