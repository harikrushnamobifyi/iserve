//
//  InvoiceViewController.m
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "InvoiceViewController.h"
#import "NetworkHandler.h"
#import "UIImageView+WebCache.h"
#import "SignInViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "RateViewController.h"
#import "QuartzCore/CALayer.h"

#define kDescriptionPlaceholder @"Please share your experience with us, it will help us provide you better service."

@interface InvoiceViewController () <UITextViewDelegate,RatingViewTextDelegate>
@property (strong, nonatomic) NSString *reviewText;
@property(nonatomic,strong)AXRatingView *ratingViewStars;
@property(nonatomic,assign) float rating;
@end

@implementation InvoiceViewController
@synthesize appointmentDate;
@synthesize doctorEmail;
@synthesize imgView;
@synthesize docNameLabel;
@synthesize priceLabel;
@synthesize dateLabel;
@synthesize docAddr;
@synthesize rating;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)dismissViewButtonClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)leaveCommentButtonClicked:(id)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main"  bundle:nil];
    RateViewController *popUpVC =[sb instantiateViewControllerWithIdentifier:@"rateVC"];
    self.definesPresentationContext = YES;
    popUpVC.delegate = self;
    popUpVC.reviewText = _reviewText;
    popUpVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    popUpVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    popUpVC.view.superview.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-40); //it's important to do this after presentModalViewController
    popUpVC.view.superview.center = self.view.center;
    [self presentViewController:popUpVC animated:YES completion:nil];

    
}

- (IBAction)submitButtonClicked:(id)sender {
    
//    if (_isComingFromAppointmnetScreen == YES) {
//        
//        [self dismissViewButtonClicked:nil];
//    }
//    else {
//        
//        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
//        if ( [reachability isNetworkAvailable]) {
//            
//            [self sendRequestForReviewSubmit];
//        }
//        else {
//            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
//            [pi showMessage:kNetworkErrormessage On:self.view];
//        }
//    }

    //Testing
    [self dismissViewButtonClicked:nil];
    
    
}

#pragma mark - ViewController LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Invoice";
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    imgView.layer.shadowColor = [UIColor blackColor].CGColor;
    imgView.layer.shadowOffset = CGSizeMake(0, 3);
    imgView.layer.shadowOpacity = 1;
    imgView.layer.shadowRadius = 1.0;
    imgView.clipsToBounds = YES;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    
    if (_isComingFromAppointmnetScreen == YES) {

        [_ratingView setHidden:YES];
        [_leaveCommentButton setHidden:YES];
        [_submitButton setTitle:@"Okay" forState:UIControlStateNormal];
        [_appointmentWithLabel setText:@"Your appointment was with"];

    }
    else {
        _reviewText = @"";
        [_appointmentWithLabel setText:@"Your last appointment was with"];
        [_ratingView setHidden:NO];
        [_addressHeaderLabel setHidden:YES];
        [docAddr setHidden:YES];
        [_submitButton setHidden:NO];
        [_leaveCommentButton setHidden:NO];
    }
    

    
     rating = 5.0;
     _ratingViewStars = [[AXRatingView alloc] initWithFrame:CGRectMake(70, 70,CGRectGetWidth(_ratingView.frame), CGRectGetHeight(_ratingView.frame))];
    [self.view bringSubviewToFront:imgView];
    
    _ratingViewStars = [[AXRatingView alloc] initWithFrame:CGRectMake(10,0,300,70)];
    _ratingViewStars.baseColor =     [Helper getColorFromHexString:@"#d7d5d5"];
    _ratingViewStars.highlightColor = [Helper getColorFromHexString:@"#ffae12"];
    
    _ratingViewStars.value = rating;
    _ratingViewStars.markFont = [UIFont systemFontOfSize:50.0f];
    _ratingViewStars.userInteractionEnabled = YES;
    [_ratingViewStars addTarget:self action:@selector(ratingChanged:) forControlEvents:UIControlEventValueChanged];
    [_ratingView addSubview:_ratingViewStars];
    

    // Commented for testing
    
    //    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
//    if ( [reachability isNetworkAvailable]) {
//        
//        [self sendRequestForAppointmentInvoice];
//    }
//    else {
//        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
//        [pi showMessage:kNetworkErrormessage On:self.view];
//    }
    
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    if (screenSize.size.height == 480)
    {
        _mainScrollView.frame = CGRectMake(0, 0, 320, 480);
        _mainScrollView.contentSize = CGSizeMake(320,500);
    }
    else
    {
    }
}
- (void)ratingChanged:(AXRatingView *)sender
{
    rating = sender.value;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - WebServiceRequest

-(void)sendRequestForAppointmentInvoice {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Please wait..."];
   
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = self.doctorEmail;
    NSString *appointmntDate = self.appointmentDate;
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_doc_email":docEmail,
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self parseAppointmentDetailResponse:response];
                                   }
                               }];
}

-(void)sendRequestForReviewSubmit {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Saving.."];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = self.doctorEmail;
    NSString *appointmntDate = self.appointmentDate;
    NSString *currentDate = [Helper getCurrentDateTime];
    int rating1 = rating;
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_doc_email":docEmail,
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             @"ent_rating_num":[NSNumber numberWithInt:rating1],
                             @"ent_review_msg":_reviewText,
                             };
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self parseSubmitReviewResponse:response];
                                   }
                               }];
}

#pragma mark - WebService Response
-(void)parseAppointmentDetailResponse:(NSDictionary*)response {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"]){
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"]integerValue] == 1) {
    
        [self dismissViewButtonClicked:nil];
    }
    else if ([response[@"errFlag"]integerValue] == 0) {
        
        
        if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7||[response[@"errNum"] intValue] == 83 || [response[@"errNum"] intValue] == 76)) { //session Expired
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];

            
        }
        else {
            
            dateLabel.text = [NSString stringWithFormat:@"%@ at %@",[Helper getLocalDate:appointmentDate],[Helper getLocalTime:appointmentDate]];
            
            docNameLabel.text = [NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
            
            priceLabel.text = [NSString stringWithFormat:@"$%@",response[@"amount"]];
            
            NSInteger totalduration = [response[@"duration"] integerValue];
            NSInteger minutes = totalduration/60;
            NSInteger hours = 0;
            
            if (minutes >= 60) {
                
                hours = minutes/60;
                minutes = minutes%60;
                _durationLabel.text = [NSString stringWithFormat:@"%02ld Hour %02ld Minutes",(long)hours,(long)minutes];
            
            }
            else {
                
                _durationLabel.text = [NSString stringWithFormat:@"%02ld Minutes",(long)minutes];
            }



            docAddr.text = [NSString stringWithFormat:@"%@ ",response[@"addr1"]];
            NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,response[@"pPic"]];
            
            [imgView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                              placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                     }];

        }
    }
}

-(void)clearUserDefaults {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud removeObjectForKey:@"isBooked"];
    [ud setBool:NO forKey:@"isBooked"];
    [ud removeObjectForKey:@"starttime"]; //This one is for the timer runnig when the doctor reached.
    [ud removeObjectForKey:@"LastTime"];
    [ud removeObjectForKey:kOnGoingBookingDoctorEmailKey];
    [ud removeObjectForKey:kOnGoingBookingDoctorBookingDate];
    [ud removeObjectForKey:kOnGoingBookingDoctorNameKey];
    [ud removeObjectForKey:kOnGoingBookingDoctorPhoneNumberKey];
    [ud removeObjectForKey:kOnGoingBookingDoctorProfileImageKey];
    [ud removeObjectForKey:kOnGoingBookingDoctorRatingKey];
    [ud removeObjectForKey:kOnGoingBookingDoctorSubscribedChannelKey];
    
    //Status Key For Changing content on HomeViewController
    [ud setInteger:0 forKey:@"STATUSKEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)parseSubmitReviewResponse:(NSDictionary*)response {
    
    [self clearUserDefaults];
    
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else if ([[response objectForKey:@"errFlag"] integerValue] == 0)
    {
           
            [[NSNotificationCenter defaultCenter] postNotificationName:@"jobCompleted" object:nil userInfo:nil];

            [self dismissViewButtonClicked:nil];
    }
        
    else {
            [Helper showAlertWithTitle:@"Message" Message:@"Server not responding.Please wait for a while."];
        
    }

}
-(void)hasTextChanges:(NSString *)text {
    
    if (![text isEqualToString:kDescriptionPlaceholder]) {
        
        [_leaveCommentButton setTitle:@"EDIT COMMENT" forState:UIControlStateNormal];
        _reviewText = text;
    }
    else {
        
        [_leaveCommentButton setTitle:@"LEAVE COMMENT" forState:UIControlStateNormal];
        _reviewText = text;
    }
    
}
@end
