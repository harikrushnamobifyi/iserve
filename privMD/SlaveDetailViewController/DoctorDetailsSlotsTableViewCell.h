//
//  DoctorDetailsSlotsTableViewCell.h
//  privMD
//
//  Created by Rahul Sharma on 12/19/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorDetailsSlotsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bottomViewForSlots;

@end
