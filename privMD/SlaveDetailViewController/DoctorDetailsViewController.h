//
//  DoctorDetailsViewController.h
//  ParadiseRide
//
//  Created by Rahul Sharma on 24/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView.h>

@interface DoctorDetailsViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *topBackgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet AXRatingView *doctorDetailsRatingImage;

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) IBOutlet UIImageView *doctorDetailsProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *recommondationLable;
@property (strong, nonatomic) IBOutlet UILabel *doctorDetailsName;
@property (strong, nonatomic) IBOutlet UILabel *doctorEducationLable;
@property (weak, nonatomic) IBOutlet UIButton *bookButton;
@property (weak, nonatomic) IBOutlet UIScrollView *topScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageIndicator;
@property (weak, nonatomic) IBOutlet UIView *bookButtonView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) NSString *docEmail;
@property (nonatomic, strong) NSString *slotsDate;
@property (strong, nonatomic) NSMutableArray *mReviews;
@property (strong, nonatomic) NSMutableArray *mEducation;
@property (strong, nonatomic) NSMutableArray *servicemArr;
@property (strong, nonatomic) NSMutableDictionary *doctDetails;
@property (strong, nonatomic) NSMutableArray *allSectionData;
@property (strong, nonatomic) NSArray *topImages;
@property UIImageView *topImage;
@property (assign, nonatomic) int pageCount;

- (IBAction)loadMoreAction:(id)sender;
- (IBAction)doctorDetailsBookButtonAction:(id)sender;
- (IBAction)navBackButtonAction:(id)sender;

@property(nonatomic , assign) AppointmentType appointmenttype;

@end
