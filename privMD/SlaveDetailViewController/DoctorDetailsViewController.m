//
//  DoctorDetailsViewController.m
//  ParadiseRide
//
//  Created by Rahul Sharma on 24/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "DoctorDetailsViewController.h"
#import "DoctorDetailsTableViewCell.h"
#import "DoctorDetailsReviewTableViewCell.h"
#import "UILabel+DynamicHeight.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "Helper.h"
#import "ProgressIndicator.h"
#import "DoctorSummaryViewController.h"
#import "AppointmentLocation.h"
#import "HorizontalScroller.h"
#import "DoctorDetailsSlotsTableViewCell.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"

CGFloat const kMGOffsetEffects = 40.0;
#define HeaderHeight 270.0f


@interface DoctorDetailsViewController ()<HorizontalScrollerDelegate>
{
    NSMutableArray * topImagesArray;
    NSMutableArray *allHorizontalData;
    HorizontalScroller *scroller;
    int currentHorizontalDataIndex;
    
}
@property (strong, nonatomic) NSMutableArray *buttonArray ;
@property (strong, nonatomic) NSMutableArray *sidUniqueNo ;
@property (strong, nonatomic) NSString *slotID;

@property NSString *str;
@property int count;
@property float defaultY;
@property NSArray *doctorRatingImages;
@property NSInteger pageNumber;
@property CGFloat lastContentOffset;
@property CGRect frame;
@property CGPoint startContentOffset;

@end

@implementation DoctorDetailsViewController
//@synthesize detailScrollView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    topImagesArray = [[NSMutableArray alloc] init];
    currentHorizontalDataIndex = 0;
    _doctDetails = [[NSMutableDictionary alloc]init];
    _allSectionData = [NSMutableArray array];
    _mReviews  = [NSMutableArray array];
    [self.tblView setHidden:YES];
    [self.bookButtonView setHidden:YES];
    
    [_allSectionData addObject:@"RATE CARD"];
    
//    [self requestDoctorDetails];
    
    // Do any additional setup after loading the view.
    
    
    //Testing
    

    [_allSectionData addObject:@"SLOTS"];

    [_allSectionData addObject:@"ABOUT"];

    [_allSectionData addObject:@"EDUCATION"];

    [_allSectionData addObject:@"EXPERTISE"];
    
    [self createTableHeaderView];
    [self createTableFooterView];
    [self.tblView setHidden:NO];
    [self.bookButtonView setHidden:NO];
    [_tblView reloadData];}

-(void)viewWillAppear:(BOOL)animated
{
    self.lastContentOffset = 0;
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    scroller.delegate = nil;

    if (_appointmenttype == kAppointmentTypeLater) {
        
        PatientAppDelegate *app = [[UIApplication sharedApplication]delegate];
        [app setupAppearance];
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - pageIndicator Methods
- (IBAction)changePage:(id)sender {
    
    CGFloat x = _pageIndicator.currentPage * _topScrollView.frame.size.width;
    [_topScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
    
    [self.topScrollView bringSubviewToFront:_pageIndicator];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView  {
    
    if(_count!=100)
    {
        _pageNumber = roundf((scrollView.contentOffset.x) / (scrollView.frame.size.width));
        _pageIndicator.currentPage = _pageNumber;
    }
    else
        _count = 10;
}



#pragma mark - Webservice Handler(Request)

- (void)requestGetReviews{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *strPageCount = [NSString stringWithFormat:@"%d",_pageCount];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime],
                               @"ent_doc_email":_docEmail,
                               @"ent_page":strPageCount
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getMasterReviews" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self responseGetReviews:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}

- (void)responseGetReviews:(NSDictionary *)response{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }else if ([response objectForKey:@"Error"]){
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else{
        NSMutableDictionary *mDict = [response mutableCopy];
        
        if ([mDict[@"errFlag"] intValue] == 1 && ([mDict[@"errNum"] intValue] == 6 || [mDict[@"errNum"] intValue] == 7)) { //session Expired
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
        }
        else if ([[mDict objectForKey:@"errFlag"] intValue] == 0) {
            
             NSMutableArray *loadDefects = [mDict[@"reviews"] mutableCopy];
            [_mReviews addObjectsFromArray:loadDefects];
            [self.tblView reloadData];
        }
    }
    
}


- (void)requestDoctorDetails {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    
    else {
        
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime],
                               @"ent_doc_email":_docEmail,
                               @"ent_slot_date":_slotsDate,
//                               @"ent_offset":[NSNumber numberWithInteger:[Helper getOffsetFromGMTTime]]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getMasterDetails"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self responseDoctorDetail:response];
                                 _doctDetails = [response mutableCopy];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}

#pragma mark - Webservice Handler(Response)
- (void) responseDoctorDetail:(NSDictionary *)response {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"]){
        
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 83 || [response[@"errNum"] intValue] == 76)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        
    }

    else if ([[response objectForKey:@"errFlag"] intValue] == 1) {
        
        [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
        
    }
    
    else {
        
        
        _doctDetails = [response mutableCopy];
        _mEducation = [_doctDetails objectForKey:@"education"];
        _topImages = [_doctDetails objectForKey:@"images"];
        
        
        NSMutableArray *slots = [_doctDetails[@"slots"] mutableCopy];
        if(!allHorizontalData){
            allHorizontalData = [[NSMutableArray alloc]init];
            _sidUniqueNo = [[NSMutableArray alloc]init];
        }
        
        for(NSDictionary *time in slots)
        {
            
            NSString *from = [time objectForKey:@"from"];
            [allHorizontalData addObject:from];
            [_sidUniqueNo addObject:[time objectForKey:@"sid"]];
        }
        
        _str = @"";
        for(int i = 0;i<_mEducation.count;i++)
        {
            if (i > 0) {
                
                _str = [_str stringByAppendingString:@"\n"];
            }

            _str = [_str stringByAppendingString:@"Degree:"];
            _str = [_str stringByAppendingString:_mEducation[i][@"deg"]];
            _str = [_str stringByAppendingString:@"\nMedical School:"];
            _str = [_str stringByAppendingString:_mEducation[i][@"inst"]];
            _str = [_str stringByAppendingString:@"\nYear Of Passing:"];
            _str = [_str stringByAppendingString:_mEducation[i][@"start"]];
            _str = [_str stringByAppendingString:@"-"];
            _str = [_str stringByAppendingString:_mEducation[i][@"end"]];
            _str = [_str stringByAppendingString:@"\n"];
            
        }
        
        //        _str = [_str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        if (slots.count != 0) {
            [_allSectionData addObject:@"SLOTS"];
        }
        
        NSString *abo = _doctDetails[@"about"];
        if (abo.length != 0) {
            [_allSectionData addObject:@"ABOUT"];
        }
        
        if (_mEducation.count != 0) {
            [_allSectionData addObject:@"EDUCATION"];
        }
        
        
        NSString *details = _doctDetails[@"expertise"];
        if (details.length != 0) {
            [_allSectionData addObject:@"EXPERTISE"];
        }
        
        _mReviews = [[_doctDetails objectForKey:@"reviews"] mutableCopy];
        
        if (_mReviews.count != 0) {
            [_allSectionData addObject:@"REVIEWS"];
        }
        
        
        [self createTableHeaderView];
        [self createTableFooterView];
        [self.tblView setHidden:NO];
        [self.bookButtonView setHidden:NO];
        [_tblView reloadData];
        
    }
    
}

#pragma mark - TableView Delegate Methods
-(void)createTableFooterView {
    
    if (_mReviews.count > 0 && [_doctDetails[@"totalRev"] intValue] > 5 && _mReviews.count < [_doctDetails[@"totalRev"] intValue])
    {
        _footerView.hidden = NO;
    }
    else {
        _footerView.hidden = YES;
    }
}

-(void)createTableHeaderView {
    
    _tblView.tag = 100;
    
    NSInteger ratingValue = [[_doctDetails objectForKey:@"rating"] floatValue];
    _doctorDetailsRatingImage.value = 3.5;
    _doctorDetailsRatingImage.baseColor = WHITE_COLOR;
    _doctorDetailsRatingImage.highlightColor = [Helper getColorFromHexString:@"#ffae12"];
    
    
//    _doctorDetailsName.text = [NSString stringWithFormat:@"%@ %@",[_doctDetails objectForKey:@"fName"],[_doctDetails objectForKey:@"lName"]];
    
    _doctorDetailsName.text = [NSString stringWithFormat:@"%@ %@",@"Ramu",@"KAKA"];
    
//    _titleLabel.text = _doctorDetailsName.text;
      _titleLabel.text = @"Ramu Kaka";
    
//    _doctorEducationLable.text = flStrForObj([_doctDetails objectForKey:@"deg"]);
        _doctorEducationLable.text = @"Plumber";
    
    
    //ProfilePic
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage ,[_doctDetails objectForKey:@"pPic"]];
    _doctorDetailsProfileImage.layer.cornerRadius = _doctorDetailsProfileImage.frame.size.width/2;
    [_doctorDetailsProfileImage setClipsToBounds:YES];
    [_doctorDetailsProfileImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                  placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                             
                                             
                                         }];
//    _recommondationLable.text = [NSString stringWithFormat:@"%@ Review(s)",_doctDetails[@"totalRev"]];
    _recommondationLable.text = [NSString stringWithFormat:@"%@ Review(s)",@"10"];

    
     CGRect frame = self.topBackgroundImage.frame;
    
    if (_topImages.count == 0) {
        
    }
    else {
    
        for(_count = 0;_count < _topImages.count;_count++)
        {
            
            frame.origin.x = _topScrollView.frame.size.width*_count;
            _topImage = [[UIImageView alloc] initWithFrame:frame];
            [_topImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[_topImages objectAtIndex:_count]]]];
            
            strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_topImages[_count]];
            
            [_topImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                         placeholderImage:[UIImage imageNamed:@"doc_info_default_image_frame"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                    
                                    
                                }];
            if (_count == 0) {
                
            [self.topBackgroundImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                         placeholderImage:[UIImage imageNamed:@"doc_info_default_image_frame"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                    
                                    
                                }];
            }
            
            _topImage.tag = _count;
            
            [topImagesArray addObject:_topImage];
            [_topScrollView addSubview:_topImage];
            [_topScrollView sendSubviewToBack:_topImage];

            
        }
    }
    
    //pageview
    _pageIndicator.numberOfPages = _topImages.count;
    _pageIndicator.currentPage = 0;
    [_pageIndicator addTarget:self action:@selector(pagecontrolClicked:) forControlEvents:UIControlEventValueChanged];
    
    if (_topImages.count == 0) {
        
        frame.origin.x = _topScrollView.frame.size.width*0;
        [_topBackgroundImage setBackgroundColor:[Helper getColorFromHexString:@"#508FB2"]];
        [topImagesArray addObject:_topBackgroundImage];
        _pageIndicator.numberOfPages = 1;
        
    }
    
    _topScrollView.contentSize = CGSizeMake(_topScrollView.frame.size.width *_topImages.count, _topScrollView.frame.size.height);
    
    _topScrollView.pagingEnabled = YES;
    _topScrollView.delegate = self;

}

-(void)pagecontrolClicked:(id)sender{
    
    NSInteger page = _pageIndicator.currentPage;

    CGRect frame = _topScrollView.frame;
    frame.origin.x= 0;
    frame.origin.y = page;
    
    [_topScrollView scrollRectToVisible:frame animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    float width = tableView.bounds.size.width;
    int fontSize = 40;
    int padding = 10;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, fontSize)];
    view.backgroundColor = [Helper getColorFromHexString:@"#F5F5F5"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 2, width - padding, fontSize)];
    label.text = _allSectionData[section];
    [Helper setToLabel:label Text:_allSectionData[section] WithFont: Roboto_Bold   FSize:16 Color:[Helper getColorFromHexString:@"#508FB2"]];
    [view addSubview:label];
    
    return view;
}

- (IBAction)loadMoreAction:(id)sender {
    
    if (_mReviews.count<= [_doctDetails[@"totalRev"] intValue])
    {
        _pageCount++;
        
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ( [reachability isNetworkAvailable]) {
            [self requestGetReviews];
        }
        else {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
        }
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_allSectionData[section] isEqualToString:@"REVIEWS"]) {
        if (_mReviews.count > 0) {
            return _mReviews.count;
        }
        return 1;
    }
    else {
        
        return 1;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _allSectionData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([_allSectionData[indexPath.section] isEqualToString:@"REVIEWS"]) {
        
        if (_mReviews.count > 0)
        {
            NSDictionary *dict = _mReviews[indexPath.row];
            UILabel *lblForHeight = [[UILabel alloc]initWithFrame:CGRectMake(50,25,250,0)];
            lblForHeight.text = dict[@"review"];
            CGSize size = [lblForHeight sizeOfMultiLineLabel:Roboto_Light SizeOfFont:13];
            return size.height+37;
        }
        else
        {
            return 0;
        }
        
    }
    
    UILabel *lblForHeight = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10,0)];
    
    if ([_allSectionData[indexPath.section]isEqualToString:@"RATE CARD"]) {
        
//        lblForHeight.text = [NSString stringWithFormat:@"$ %@ /hour",_doctDetails[@"fee"]];

        lblForHeight.text = [NSString stringWithFormat:@"$ %@ /hour",@"450"];
    }
    if ([_allSectionData[indexPath.section]isEqualToString:@"ABOUT"]) {
        
//        lblForHeight.text = _doctDetails[@"about"];
        
        lblForHeight.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";
        
    }
    if ([_allSectionData[indexPath.section]isEqualToString:@"EXPERTISE"]) {
        
//        lblForHeight.text = _doctDetails[@"expertise"];
        
                lblForHeight.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";
        
    }
    if ([_allSectionData[indexPath.section]isEqualToString:@"EDUCATION"]) {
        
//        lblForHeight.text = _str;
                lblForHeight.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";
    }
    if ([_allSectionData[indexPath.section]isEqualToString:@"SLOTS"]) {
        
        return 64;
    }
    
    CGSize size = [lblForHeight sizeOfMultiLineLabel:Roboto_Light SizeOfFont:13];
    
    return size.height+10;
}




-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([_allSectionData[indexPath.section]isEqualToString:@"REVIEWS"]) {
        
        static NSString *cellIdentifier = @"docReviewCell";
        DoctorDetailsReviewTableViewCell *cell= (DoctorDetailsReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        NSDictionary *dict = _mReviews[indexPath.row];
        cell.reviewTextView.text = dict[@"review"];
        cell.reviewName.text = dict[@"by"];
        cell.starRating.value = [dict[@"rating"]floatValue];
        cell.starRating.baseColor = [Helper getColorFromHexString:@"#d7d5d5"];
        cell.starRating.highlightColor = [Helper getColorFromHexString:@"#ffae12"];

        CGSize size = [cell.reviewTextView sizeOfMultiLineLabel:Roboto_Light SizeOfFont:13];
        CGRect rect = cell.reviewTextView.frame;
        rect.size.height = size.height;
        cell.reviewTextView.frame = rect;
        
        rect = cell.frame;
        rect.size.height = size.height+37;
        cell.frame = rect;
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,dict[@"patPic"]];
        cell.reviewImage.layer.cornerRadius = cell.reviewImage.frame.size.width/2;
        cell.reviewImage.layer.borderColor = [Helper getColorFromHexString:@"#508FB2"].CGColor;
        cell.reviewImage.layer.borderWidth = 0.5f;
        
        [cell.reviewImage setClipsToBounds:YES];
        [cell.reviewImage sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                            placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       
                                   }];
        return cell;
        
    }
    else {
        
        static NSString *cellIdentifier = @"docDetailCell";
        DoctorDetailsTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if ([_allSectionData[indexPath.section]isEqualToString:@"RATE CARD"]) {
            
            cell.educationDetail.text = [NSString stringWithFormat:@"$ %@ /appointment",_doctDetails[@"fee"]];
            
        }
        if ([_allSectionData[indexPath.section]isEqualToString:@"ABOUT"]) {
            
//            cell.educationDetail.text = _doctDetails[@"about"];
            cell.educationDetail.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";

        }
        
        if ([_allSectionData[indexPath.section]isEqualToString:@"EDUCATION"]) {
            
//            cell.educationDetail.text = _str;
            cell.educationDetail.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";

        }
        if ([_allSectionData[indexPath.section]isEqualToString:@"EXPERTISE"]) {
            
//            cell.educationDetail.text = _doctDetails[@"expertise"];
            cell.educationDetail.text = @"The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.The plumbers from the company are highly qualified. The person who came to my house explained the issue to me very clearly and what had to be done. Appreciate the help.";

        }
        if ([_allSectionData[indexPath.section]isEqualToString:@"SLOTS"]) {
            
            static NSString *cellIdentifierSlot = @"docDetailCellSlots";
            DoctorDetailsSlotsTableViewCell *cellS = [tableView dequeueReusableCellWithIdentifier:cellIdentifierSlot];
                //Horizontal scroller View
            
            if (!scroller) {
                scroller = [[HorizontalScroller alloc] initWithFrame:CGRectMake(0,0,cellS.bottomViewForSlots.frame.size.width,cellS.bottomViewForSlots.frame.size.height)];
                scroller.backgroundColor = [UIColor clearColor];
                scroller.delegate = self;
                [cellS.bottomViewForSlots addSubview:scroller];
            }
            
            return cellS;
            
        }
        
        cell.educationDetail.numberOfLines = 0;
        CGSize size = [cell.educationDetail sizeOfMultiLineLabel:Roboto_Light SizeOfFont:13];
        CGRect rect = cell.educationDetail.frame;
        rect.size.height = size.height+10;
        cell.educationDetail.frame = rect;
        
        return cell;
    }
    
}


#pragma mark - Button Actions

- (IBAction)doctorDetailsBookButtonAction:(id)sender {
    
    AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
    if(_appointmenttype == kAppointmentTypeNow)
    {
        [self performSegueWithIdentifier:@"doctorCheckout" sender:self];
    }
    else {
        
        if (@"laterdate")
        {
            [self performSegueWithIdentifier:@"doctorCheckout" sender:self];
        }
        else {
            
            [Helper showAlertWithTitle:@"Message" Message:@"Please select a slot for the appointment with PriveMD."];
        }
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSString *)sender  {
    
    if ([segue.identifier isEqualToString:@"doctorCheckout"]) {
        
        DoctorSummaryViewController *DSVC = (DoctorSummaryViewController *)[segue destinationViewController];
        DSVC.doctDetails = _doctDetails;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *finalString = [dateFormatter stringFromDate:[NSDate date]];
        NSString *stringDate = finalString;
        DSVC.stringDate = stringDate;
        
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
//        apLocaion.laterAppointmentSlotID = _slotID;
        
        
        if(allHorizontalData.count != 0)
            DSVC.appointmentType = kAppointmentTypeLater;
        else
            DSVC.appointmentType = kAppointmentTypeLater; // changed for testing it should be now
        
    }
}

- (IBAction)navBackButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Up and Down Scrolling Animation

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageHeight = self.topScrollView.frame.size.width;
    float fractionalPage = self.topScrollView.contentOffset.x / pageHeight;
    NSInteger page = lround(fractionalPage);
    self.pageIndicator.currentPage = page;
    
    UIImageView *locImg;
    if(scrollView.tag == 100)
    {
        if(page <1){
            page = 0;
            
        }
        locImg = topImagesArray[page];
        if(scrollView.contentOffset.y == 0){
            _titleView.alpha = 0;
            _titleLabel.alpha = 0;
        }
        
        
        if (self.lastContentOffset > scrollView.contentOffset.y)//downwards
        {
            CGFloat movedOffset = scrollView.contentOffset.y;
            CGFloat headerOffset = HeaderHeight - movedOffset-64;
            
            if (headerOffset > 184 && headerOffset <= 120) {
                
                _titleView.backgroundColor = [Helper getColorFromHexString:@"#508FB2"];
                _titleView.alpha = (movedOffset-40) / 64;
                _titleLabel.alpha = (movedOffset-40) / 64;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  headerOffset+6;
                _titleLabel.frame = frame;
                
                
            }
            
            else if(movedOffset <= 36) {
                
                _titleView.alpha = 0;
                _titleLabel.alpha = 0;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  99;
                _titleLabel.frame = frame;
                
            }
            
            _count = 100;
            
            CGFloat yPos = -scrollView.contentOffset.y;
            if (yPos > 0) {
                
                
                CGRect imgRect = self.topScrollView.frame;
                imgRect.origin.y = scrollView.contentOffset.y;
                imgRect.size.height = HeaderHeight+yPos;
                self.topScrollView.frame = imgRect;
                
                CGRect frame = locImg.frame;
                frame.size.height = HeaderHeight+yPos;
                locImg.frame = frame;
                
                
            }
            
            
            
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y)//upwards
        {
            CGFloat movedOffset = scrollView.contentOffset.y;
            CGFloat headerOffset = HeaderHeight - movedOffset-64;
            _titleLabel.alpha = 1;
            if(scrollView.contentOffset.y <= 0){
                
                locImg.frame = CGRectMake(320*page,0, 320, HeaderHeight-scrollView.contentOffset.y);
                self.topScrollView.frame = CGRectMake(0, scrollView.contentOffset.y,self.topScrollView.frame.size.width, HeaderHeight-scrollView.contentOffset.y);
                
            }
            if (headerOffset > 120 && headerOffset <= 184) {
                
                _titleView.backgroundColor = [Helper getColorFromHexString:@"#508FB2"];
                _titleView.alpha = 1 - headerOffset / 64;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  headerOffset+6;
                _titleLabel.frame = frame;
                [self.view bringSubviewToFront:_titleView];
                [self.view bringSubviewToFront:_titleLabel];
                
                
            }
            else if(headerOffset < 0){
                
                _titleView.alpha = 1;
                _titleLabel.alpha = 1;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  6;
                _titleLabel.frame = frame;
                [self.view bringSubviewToFront:_titleView];
                [self.view bringSubviewToFront:_titleLabel];
                
            }
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
    }
    
}


#pragma mark - HorizontalScrollerDelegate methods
- (void)horizontalScroller:(HorizontalScroller *)scroller clickedViewAtIndex:(int)index
{
    currentHorizontalDataIndex = index;
}

- (NSInteger)numberOfViewsForHorizontalScroller:(HorizontalScroller*)scroller
{
//    return allHorizontalData.count;

    return 5;
}

- (UIView*)horizontalScroller:(HorizontalScroller*)scroller viewAtIndex:(int)index
{
    //  allHorizontalData[index];
    UIView *retView = [[UIView alloc]initWithFrame:CGRectMake(0,0,100,60)];
    UIButton *detailsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    detailsButton.frame = CGRectMake(0,-10,100,55);
    detailsButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    detailsButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    NSString *text = [NSString stringWithFormat:@"%@\n $ %@",allHorizontalData[index],_doctDetails[@"slots"][index][@"slotPrice"]];
    NSString *text = [NSString stringWithFormat:@"%@\n $ %@",@"100",@"50"];
    [Helper setButton:detailsButton Text:text WithFont:Roboto_Regular FSize:14 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [detailsButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    detailsButton.layer.borderColor = [Helper getColorFromHexString:@"#508FB2"].CGColor;
    detailsButton.layer.borderWidth = 2.0f;
    detailsButton.layer.cornerRadius = 5.0f;
    detailsButton.tag = 100+index;
    detailsButton.backgroundColor = [UIColor whiteColor];
    
    if(!_buttonArray)
    {
        _buttonArray = [[NSMutableArray alloc] init];
    }
    [_buttonArray addObject:detailsButton];
    
    [detailsButton addTarget:self action:@selector(detailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [retView addSubview:detailsButton];
    
    return retView;
}

- (NSInteger)initialViewIndexForHorizontalScroller:(HorizontalScroller *)scroller
{
    return currentHorizontalDataIndex;
}
-(void)detailsButtonClicked:(id)sender
{
    if(!_buttonArray){
        //do nothing
    }
    else{
        
        for (UIButton *mBtn in _buttonArray)
        {
            [mBtn setSelected:([mBtn isEqual:sender])?YES:NO];
            if(mBtn.isSelected)
            {
                mBtn.backgroundColor = [Helper getColorFromHexString:@"#508FB2"];
                
//                _slotID = [_sidUniqueNo objectAtIndex:mBtn.tag-100];
                _slotID = @"123455";
                
                AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
//                apLocaion.laterAppointmentTime = allHorizontalData[mBtn.tag-100];
                
                NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
                DoctorDetailsTableViewCell *cell = (DoctorDetailsTableViewCell *)[self.tblView cellForRowAtIndexPath:path];
//                cell.educationDetail.text = [NSString stringWithFormat:@"$ %@ /appointment",_doctDetails[@"slots"][mBtn.tag-100][@"slotPrice"]];
                cell.educationDetail.text = [NSString stringWithFormat:@"$ %@ /appointment",@"100"];
                
            }
            else
            {
                mBtn.backgroundColor = [UIColor whiteColor];
            }
            
        }
    }
}


@end
