//
//  DoctorDetailsTableViewCell.h
//  ParadiseRide
//
//  Created by Rahul Sharma on 24/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorDetailsTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *educationDetail;

@end
