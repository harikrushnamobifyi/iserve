//
//  DoctorDetailsReviewTableViewCell.h
//  ParadiseRide
//
//  Created by Rahul Sharma on 24/07/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView.h>

@interface DoctorDetailsReviewTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *reviewImage;
@property (strong, nonatomic) IBOutlet UILabel *reviewTextView;
@property (strong, nonatomic) IBOutlet UILabel *reviewName;
@property (strong, nonatomic) IBOutlet AXRatingView *starRating;

@end
