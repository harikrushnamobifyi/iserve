//
//  AboutViewController.m
//  UBER
//
//  Created by Rahul Sharma on 21/05/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "WebViewController.h"

@interface AboutViewController ()<CustomNavigationBarDelegate>

@end

@implementation AboutViewController
@synthesize topView;
@synthesize topViewevery1Label;
@synthesize topviewwebsiteButton;
@synthesize likeButton;
@synthesize legalButton;
@synthesize rateButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
	// Do any additional setup after loading the view.
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [Helper setToLabel:topViewevery1Label Text:@"'Mobilising essential services'" WithFont:Roboto_Regular FSize:13 Color:UIColorFromRGB(0x333333)];

    [Helper setButton:topviewwebsiteButton Text:@"'www.iserve.com'" WithFont:Roboto_Regular FSize:11 TitleColor:UIColorFromRGB(0x3399ff) ShadowColor:nil];

    
    [Helper setButton:rateButton Text:@"RATE US IN THE APP STORE" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:@"LIKE US ON FACEBOOK" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:_twitterButton Text:@"LIKE US ON TWITTER" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    _twitterButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _twitterButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_twitterButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:_instagramButton Text:@"LIKE US ON INSTAGRAM" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    _instagramButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _instagramButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_instagramButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:_pinTrestButton Text:@"LIKE US ON PINTEREST" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    _pinTrestButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _pinTrestButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [_pinTrestButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:@"LEGAL" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar{

    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"ABOUT"];
    [customNavigationBarView hideRightBarButton:YES];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (IBAction)rateButtonClicked:(id)sender {
    
    NSString *strURL = @"https://www.google.com";
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];

}
- (IBAction)likeonFBButtonClicked:(id)sender {
    
    UIButton *localBtn = (UIButton *)sender;
    
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    webView.title = @"LIKE";
    
    switch (localBtn.tag) {
        case 100:
                webView.weburl = @"https://www.facebook.com/IServe-991446444255667";
            break;
        case 200:
                webView.weburl = @"https://twitter.com/TheRealGuydd";
            break;
        case 300:
                webView.weburl = @"https://instagram.com/therealguydd/";
            break;
        case 400:
                webView.weburl = @"https://www.pinterest.com/TheRealGuydd/";
            break;
        default:
            break;
    }
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction)likeonTwitter:(id)sender {
}

- (IBAction)legalButtonClicked:(id)sender {
    
    UIButton *mBtn = (UIButton *)sender;
    
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    
    if (mBtn.tag == 2000) {
        
        webView.title = @"iServe";
        webView.weburl = @"http://www.iserve.com/";

    }
    else {
        
        webView.title = @"LEGAL";
        webView.weburl = @"http://www.ideliver.mobi/iServe/privacy.php";
    }
  
   
    [self.navigationController pushViewController:webView animated:YES];

}

@end
