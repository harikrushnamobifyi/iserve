//
//  XDKAirMenuController.m
//  XDKAirMenu
//
//  Created by Xavier De Koninck on 29/12/2013.
//  Copyright (c) 2013 XavierDeKoninck. All rights reserved.
//

#import "XDKAirMenuController.h"
#import "XDKMenuCell.h"
#import "User.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"

#define WIDTH_OPENED (95.f)
#define MIN_SCALE_CONTROLLER (1.0f)
#define MIN_SCALE_TABLEVIEW (0.8f)
#define MIN_ALPHA_TABLEVIEW (0.01f)
#define DELTA_OPENING (65.f)


@interface XDKAirMenuController ()<UITableViewDelegate, UIGestureRecognizerDelegate,UITableViewDataSource,UserDelegate>


@property (nonatomic, strong) NSArray *images_off;
@property (nonatomic, strong) NSArray *images_on;
@property (nonatomic, strong) NSArray *menuTitle;

@property (nonatomic, assign) CGPoint startLocation;
@property (nonatomic, assign) CGPoint lastLocation;
@property (nonatomic, weak) UITableView *tableView;

@property (nonatomic, assign) CGFloat widthOpened;
@property (nonatomic, assign) CGFloat minScaleController;
@property (nonatomic, assign) CGFloat minScaleTableView;
@property (nonatomic, assign) CGFloat minAlphaTableView;
@property UIPanGestureRecognizer *panGesture, *panGesture1;

@end


@implementation XDKAirMenuController
@synthesize panGesture,panGesture1;

static XDKAirMenuController *controller = nil;

+ (id)sharedMenu {
    
    if (!controller) {
        controller  = [[XDKAirMenuController alloc] init];
    }
    
    return controller;
}
-(void)sharedRelease {
    
    for (UIGestureRecognizer *recognizer in self.currentViewController.view.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
            
        }else {
            NSLog(@"removing gesture : %@", recognizer);
            recognizer.delegate = nil;
            [self.currentViewController.view removeGestureRecognizer:recognizer];
        }
    }
    
}

+(BOOL)relese {
    
    [controller.panGesture.view removeGestureRecognizer:controller.panGesture];
    [controller.panGesture1.view removeGestureRecognizer:controller.panGesture1];
    controller.panGesture.delegate = nil;
    controller.panGesture = nil;
    controller.panGesture1.delegate = nil;
    controller.panGesture1 = nil;
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    
    for (UIGestureRecognizer *recognizer in frontWindow.gestureRecognizers) {
        [frontWindow removeGestureRecognizer:recognizer];
    }
    
    
    NSArray *gestrue =  [[[XDKAirMenuController sharedMenu] view] gestureRecognizers];
    for (UIGestureRecognizer *recognizer in gestrue) {
        [[[XDKAirMenuController sharedMenu] view] removeGestureRecognizer:recognizer];
    }
    
    
    [[self sharedMenu] sharedRelease];
    controller.airDelegate = Nil;
    controller = Nil;
    return YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (panGesture) {
        
        [panGesture.view removeGestureRecognizer:panGesture];
        panGesture.delegate = nil;
        panGesture = nil;
    }
    
    
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    panGesture.delegate = self;
    [frontWindow addGestureRecognizer:panGesture];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
     _images_off = [[NSArray alloc]initWithObjects:
                    @"menu_profile_default_image",
                    @"menu_book_iserve_btn_selector",
                    @"menu_booking_btn_selector",
                    @"menu_payment_btn_selector",
                    @"menu_payment_btn_selector",
                    @"menu_support_btn_selector",
                    @"menu_share_btn_selector",
                    @"menu_about_btn_selector",
                    @"menu_logout_btn_selector",
                    nil];
    
    _images_on = [[NSArray alloc]initWithObjects:
                   @"menu_profile_default_image",
                   @"menu_book_iserve_btn_unselector",
                   @"menu_booking_btn_unselector",
                   @"menu_payment_btn_unselector",
                   @"menu_payment_btn_unselector",
                   @"menu_support_btn_unselector",
                   @"menu_share_btn_unselector",
                   @"menu_about_btn_unselector",
                   @"menu_logout_btn_unselector",
                   nil];
    
    
    _menuTitle = [[NSArray alloc]initWithObjects:
                  @"profile",
                  @"Book A Service",
                  @"Bookings",
                  @"Payments",
                  @"My Address",
                  @"Support",
                  @"Share",
                  @"About",
                  @"Logout",
                  nil];
    
    if ([self.airDelegate respondsToSelector:@selector(tableViewForAirMenu:)])
    {
        self.tableView = [self.airDelegate tableViewForAirMenu:self];
        self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"side_menu_bg"]];
        self.tableView.delegate = self;
       self.tableView.dataSource = self;
        [self.tableView setScrollEnabled:NO];
    }
    
    self.widthOpened = WIDTH_OPENED;
    self.minAlphaTableView = MIN_ALPHA_TABLEVIEW;
    self.minScaleTableView = MIN_SCALE_TABLEVIEW;
    self.minScaleController = MIN_SCALE_CONTROLLER;
    
    if ([self.airDelegate respondsToSelector:@selector(widthControllerForAirMenu:)])
        self.widthOpened = [self.airDelegate widthControllerForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minAlphaTableViewForAirMenu:)])
        self.minAlphaTableView = [self.airDelegate minAlphaTableViewForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minScaleTableViewForAirMenu:)])
        self.minScaleTableView = [self.airDelegate minScaleTableViewForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minScaleControllerForAirMenu:)])
        self.minScaleController = [self.airDelegate minScaleControllerForAirMenu:self];
    
    
    _isMenuOpened = FALSE;
    
    [self openViewControllerAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableViewReload) name:@"proPicChange" object:nil];
}
-(void)tableViewReload {
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (self.isMenuOpened)
        [self closeMenuAnimated];
}


#pragma mark - Gestures

- (void)panGesture:(UIPanGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan)
        self.startLocation = [sender locationInView:self.view];
    else if (sender.state == UIGestureRecognizerStateEnded
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight))))
    {
        CGFloat dx = self.lastLocation.x - self.startLocation.x;
        
        if (self.isMenuOnRight)
        {
            if ((self.isMenuOpened && dx > 0.f) || self.view.frame.origin.x > 3*self.view.frame.size.width / 4)
                [self closeMenuAnimated];
            else
                [self openMenuAnimated];
        }
        else
        {
            if ((self.isMenuOpened && dx < 0.f) || self.view.frame.origin.x < self.view.frame.size.width / 4)
                [self closeMenuAnimated];
            else
                [self openMenuAnimated];
        }
        
    }
    else if (sender.state == UIGestureRecognizerStateChanged
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight))))
        [self menuDragging:sender];
    
}

- (void)menuDragging:(UIPanGestureRecognizer *)sender
{
    CGPoint stopLocation = [sender locationInView:self.view];
    self.lastLocation = stopLocation;
    
    CGFloat dx = stopLocation.x - self.startLocation.x;
    
    CGFloat distance = dx;
    
    CGFloat width = (self.isMenuOnRight) ? (-self.view.frame.size.width + self.widthOpened) : (self.view.frame.size.width - self.widthOpened);
    
    
    CGFloat scaleController = 1 - ((self.view.frame.origin.x / width) * (1-self.minScaleController));
    
    CGFloat scaleTableView = 1 - ((1 - self.minScaleTableView) + ((self.view.frame.origin.x / width) * (-1+self.minScaleTableView)));
    
    CGFloat alphaTableView = 1 - ((1 - self.minAlphaTableView) + ((self.view.frame.origin.x / width) * (-1+self.minAlphaTableView)));
    
    
    if (scaleTableView < self.minScaleTableView)
        scaleTableView = self.minScaleTableView;
    
    if (scaleController > 1.f)
        scaleController = 1.f;
    
    self.tableView.transform = CGAffineTransformMakeScale(scaleTableView, scaleTableView);
    
    self.tableView.alpha = alphaTableView;
    
    self.currentViewController.view.transform = CGAffineTransformMakeScale(scaleController, scaleController);
    
    CGRect frame = self.view.frame;
    frame.origin.x = frame.origin.x + distance;
    
    if (self.isMenuOnRight)
    {
        if (frame.origin.x < -frame.size.width)
            frame.origin.x = -frame.size.width;
        if (frame.origin.x > 0.f)
            frame.origin.x = 0.f;
    }
    else
    {
        if (frame.origin.x < 0.f)
            frame.origin.x = 0.f;
        if (frame.origin.x > (frame.size.width))
            frame.origin.x = (frame.size.width);
    }
    
    self.view.frame = frame;
    
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    self.currentViewController.view.frame = frame;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.isMenuOpened)
        return TRUE;
    return FALSE;
}


#pragma mark - Menu

- (void)openViewControllerAtIndexPath:(NSIndexPath*)indexPath
{
    if ([self.airDelegate respondsToSelector:@selector(airMenu:viewControllerAtIndexPath:)])
    {
        BOOL firstTime = FALSE;
        if (self.currentViewController == nil)
            firstTime = TRUE;
        
                    _currentViewController = [self.airDelegate airMenu:self viewControllerAtIndexPath:indexPath];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMenuAnimated)];
            tapGesture.delegate = self;
            [self.currentViewController.view addGestureRecognizer:tapGesture];
            
            CGRect frame = self.view.frame;
            frame.origin.x = 0.f;
            frame.origin.y = 0.f;
            self.currentViewController.view.frame = frame;
            
            frame = self.view.frame;
            frame.origin.x = 0.f;
            frame.origin.y = 0.f;
            self.view.frame = frame;
            
            self.currentViewController.view.autoresizesSubviews = TRUE;
            self.currentViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            
            [self.view addSubview:self.currentViewController.view];
            [self addChildViewController:self.currentViewController];
            
        
        if (panGesture1) {
            [panGesture1.view removeGestureRecognizer:panGesture1];
            panGesture1.delegate = nil;
            panGesture1 = nil;
        }
        panGesture1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
        [self.view addGestureRecognizer:panGesture1];
        

        
        if (!firstTime)
            [self openingAnimation];
    }
}

- (void)openingAnimation
{
    self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
    
    CGRect frame = self.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = -frame.size.width + self.widthOpened;
    else
        frame.origin.x = frame.size.width - self.widthOpened;
    self.view.frame = frame;
    
    self.tableView.alpha = 1.f;
    
    self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
    
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    
    self.currentViewController.view.frame = frame;
    
    [self closeMenuAnimated];

}


#pragma mark - Actions

- (void)openMenuAnimated
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
        
        CGRect frame = self.view.frame;
        
        if (self.isMenuOnRight)
            frame.origin.x = -frame.size.width + self.widthOpened;
        else
            frame.origin.x = frame.size.width - self.widthOpened;
        
        self.view.frame = frame;
     //   [_tableView setBackgroundColor:UIColorFromRGB(0x0088E8)];
        self.tableView.alpha = 1.f;
        [self.view bringSubviewToFront:_tableView];

        self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
        
        frame = self.currentViewController.view.frame;
        if (self.isMenuOnRight)
            frame.origin.x = self.view.frame.size.width - frame.size.width;
        else
            frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    
    _isMenuOpened = TRUE;
}

- (void)closeMenuAnimated
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.currentViewController.view.transform = CGAffineTransformMakeScale(1.f, 1.f);
        
        CGRect frame = self.view.frame;
        frame.origin.x = 0.f;
        self.view.frame = frame;
        
        self.tableView.alpha = self.minAlphaTableView;
        
        self.tableView.transform = CGAffineTransformMakeScale(self.minScaleTableView, self.minScaleTableView);
        
        frame = self.currentViewController.view.frame;
        frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    
    _isMenuOpened = FALSE;
}


#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //3Embed changes
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if(indexPath.row == 5)
//    {
//        NSString *postText =  @"Download the Roadyo App.";
//        NSArray *activityItems = @[postText];
//        
//        UIActivityViewController *activityController =
//                                                        [[UIActivityViewController alloc]
//                                                        initWithActivityItems:activityItems
//                                                         applicationActivities:nil];
//        
//        [self presentViewController:activityController animated:YES completion:nil];
//        
//        
//    }
//    else
    if (indexPath.row ==8){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alertView show];
        [self openingAnimation];
    }
    else
    {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
        [self openViewControllerAtIndexPath:indexPath];
    }
}

#pragma mark - Setters

- (void)setIsMenuOnRight:(BOOL)isMenuOnRight
{
    if (self.view.superview == nil)
        _isMenuOnRight = isMenuOnRight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _menuTitle.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    XDKMenuCell *cell = nil;
    
    if (cell == nil) {
        cell = [[XDKMenuCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xffffff);
        
        
       
    }
    if (indexPath.row == 0) {
        
        UIButton *setMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [setMenuButton setBackgroundColor:[UIColor clearColor]];

        UIImageView *roundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(65,20,90,90)];
        
        roundImageView.layer.cornerRadius = roundImageView.frame.size.width/2;
        [roundImageView setClipsToBounds:YES];
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
       
        NSString *profilePic = [ud objectForKey:KDAProfilePic];
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,profilePic];
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:isFromFB] isEqualToString:@"YES"]){
            strImageUrl =[[NSUserDefaults standardUserDefaults] objectForKey:KDAFbProfilePicURL];
        }
        
        [roundImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                       placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                              }];
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,114,227,30)];
        
   
        
        NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:KDAFirstName];
        NSString *last =[[NSUserDefaults standardUserDefaults]objectForKey:KDALastName];
        [Helper setToLabel:nameLabel Text:[NSString stringWithFormat:@"%@ %@",[name uppercaseString],[last uppercaseString]]   WithFont:OpenSans_Regular FSize:19 Color:UIColorFromRGB(0x000000)];
        
        nameLabel.textAlignment = NSTextAlignmentCenter;
        
       
        
        setMenuButton.frame = CGRectMake(0,0,200,100);
        [setMenuButton setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:roundImageView];
        [cell.contentView  addSubview:nameLabel];
        
        UIView *divider = [[UIView alloc]initWithFrame:CGRectMake(0, 152, 230, .5)];
        divider.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider_big"]];
        [cell.contentView addSubview:divider];

        
        
    }
    
    else {
        
      
        
        UIImage *cellimageNormal = [UIImage imageNamed:_images_off[indexPath.row]];
        UIImage *cellimageSelected = [UIImage imageNamed:_images_on[indexPath.row]];
        
        UIButton *setMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [setMenuButton setBackgroundColor:[UIColor clearColor]];
        setMenuButton.frame = CGRectMake(0,0,227,52);
        setMenuButton.titleLabel.text = _menuTitle[indexPath.row];
        [Helper setButton:setMenuButton Text:_menuTitle[indexPath.row] WithFont:OpenSans_Light FSize:17 TitleColor:UIColorFromRGB(0x000000) ShadowColor:UIColorFromRGB(0x000000)];
        [setMenuButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
        [setMenuButton addTarget:self action:@selector(buttonPressedAction:) forControlEvents:UIControlEventTouchUpInside];
        setMenuButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        [setMenuButton setBackgroundImage:cellimageSelected forState:UIControlStateHighlighted];
         [setMenuButton setBackgroundImage:cellimageNormal forState:UIControlStateNormal];
        setMenuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        setMenuButton.titleEdgeInsets = UIEdgeInsetsMake(0,64, 0, 0);
        

        [setMenuButton setTag:indexPath.row + 100];
        [cell.contentView addSubview:setMenuButton];
        if(indexPath.row == 7) {
            UIView *divider = [[UIView alloc]initWithFrame:CGRectMake(0, 52, 230, .5)];
            divider.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider_big"]];
            [cell.contentView addSubview:divider];

        }
        else{
            
            UIView *divider = [[UIView alloc]initWithFrame:CGRectMake(50, 52, 180, .5)];
            divider.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"divider_big"]];
            [cell.contentView addSubview:divider];
        }
        
    }
    
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 152;
    }
    else {
        return 52;
    }
    
}

#pragma mark- ButtonAction
-(void)buttonPressedAction:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        [self tableView:_tableView didSelectRowAtIndexPath:indexPath];
    }
}


#pragma mark- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) { // logout
        
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:@"Logging out.."];
        
        User *user = [[User alloc] init];
        user.delegate = self;
    
        [user logout];
    }
}
#pragma mark - UserDelegate
-(void)userDidLogoutSucessfully:(BOOL)sucess{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}
-(void)userDidFailedToLogout:(NSError *)error{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

@end
