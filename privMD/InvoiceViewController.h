//
//  InvoiceViewController.h
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@interface InvoiceViewController : UIViewController

@property(nonatomic,strong)NSString *appointmentDate;
@property(nonatomic,strong)NSString *doctorEmail;
@property (assign, nonatomic) BOOL isComingFromAppointmnetScreen;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *docNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *docAddr;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *addressHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIButton *leaveCommentButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *appointmentWithLabel;

- (IBAction)leaveCommentButtonClicked:(id)sender;
- (IBAction)submitButtonClicked:(id)sender;
- (IBAction)dismissViewButtonClicked:(id)sender;

@end
