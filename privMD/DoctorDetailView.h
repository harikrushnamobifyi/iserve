//
//  WindowOverlayButton.h
//  MyAlbum
//
//  Created by Surender Rathore on 28/02/14.
//
//

#import <UIKit/UIKit.h>
typedef void (^ButtonActionCallback)();

@interface DoctorDetailView : UIView

@property(nonatomic,copy)ButtonActionCallback callback;
@property(nonatomic,strong) UIView *viewToaddd;

-(void)createViewToShowDoctorDetails;

-(void)updateDistance:(NSString*)distane estimateTime:(NSString*)eta;

+ (id)sharedInstance;
-(void)updateValues;

@end
