//
//  AddressManageViewController.h
//  Uberx
//
//  Created by Rahul Sharma on 19/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>



/**
 *  Getting Address From DataBase Delegate Method
 */
@protocol AddressManageDelegate <NSObject>

@optional

-(void)getAddressFromDatabase:(NSDictionary *)addressDetails;

@end


@interface AddressManageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


/**
 *  Address Manage TableView Used to List Addresses From Database
 */
@property (weak, nonatomic) IBOutlet UITableView *manageAddressTableView;

/**
 *  Id of the above Getting address From Database Delegate Method
 */
@property (nonatomic,assign) id addressManageDelegate;

/**
 *  Dictionary Contain the Address From Database
 */
@property(strong,nonatomic) NSDictionary *getAddress;

/**
 *  Used to check from which controller to coming to this Controller
 */
@property(nonatomic, assign) NSInteger isComingFromVC;


/**
 *  Add New Address Button Action
 *
 *  @param sender  add new Button Identifications
 */
- (IBAction)addNewAddressButtonAction:(id)sender;


@end
