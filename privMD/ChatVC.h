//
//  ChatVC.h
//  FreeTaxi
//
//  Created by Rahul Sharma on 3/20/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "SOMessagingViewController.h"

@interface ChatVC : SOMessagingViewController
@property(nonatomic, assign) int lastIndex;
@property(nonatomic, strong) NSString *driverChannel;
@property(nonatomic, strong) NSString *bookingId;
@property(nonatomic, strong) NSString *arnID;

@end
