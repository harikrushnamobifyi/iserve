//
//  Map1ViewController.m
//  Servodo
//
//  Created by Rahul Sharma on 10/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpAddressFromMapController.h"

#import "HelpViewController.h"
#import "AppointmentLocation.h"
#import "Database.h"
#import "AddressManageViewController.h"
//#import "PickUpAddressController.h"
#import "PickAddressContainerView.h"
#import "Fonts.h"
#define middleBtnTag 2000
#define curLocBtnTag 3000
#define myCustomMarkerTag 5000
#define topViewTag 90
#define pickupAddressTag 2500

@interface PickUpAddressFromMapController ()<UITextFieldDelegate,PickAddressDelegate>
{
    GMSGeocoder *geocoder_;
    BOOL isCustomMarkerSelected;
    NSString *srcAddr;
}
@property(nonatomic,strong) PickAddressContainerView *bookingControllerVC;

@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;
@property(nonatomic,assign) double currentLatitude;
@property(nonatomic,assign) double currentLongitude;
@property (strong, nonatomic) NSString *patientAddressLine2;
@property (assign, nonatomic) float patientAddressLong;
@property (assign, nonatomic) float patientAddressLati;
@property(nonatomic,strong) NSString *zipCode;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property (strong, nonatomic) UILabel *textFeildAddress;
@property AppointmentLocation *apptLocation;
@property(nonatomic,assign) BOOL isMarkerClicked;
@end

@implementation PickUpAddressFromMapController {
    
    NSArray *_tableData; // active list of data to show in table at the moment
    UIView* _searchBarWrapper;
    UIView *_shadowView;
    
}

NSString *const apiKeysd = @"AIzaSyDzFpzMhM014fzTzOErkt3M7nKRnzwz1hs";

@synthesize mapView_;
@synthesize destAddress;
@synthesize customMarkerButton;
@synthesize setDelAddButtonView;
@synthesize destinationAddressLabel;
@synthesize addLine1View,addLine2View,homeView,zipCodeView,notesView;
@synthesize saveAddressDelegate;

@synthesize apptLocation,PikUpViewForConfirmScreen,isSetbuttonclicked,integer,AddressLine1,addressLine2,home,ZipCode1,notes1;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shop_background.png"]];
    apptLocation = [AppointmentLocation sharedInstance];
    
    [self.navigationController.navigationBar setHidden:NO];
        
    _currentLatitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat] doubleValue];
    
    _currentLongitude =  [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude longitude:_currentLongitude zoom:18];
    
    mapView_.camera = camera; // = [GMSMapView mapWithFrame:[self.view bounds] camera:camera];
    mapView_.settings.compassButton = YES;
    mapView_.delegate = self;
    mapView_.settings.myLocationButton = YES;
    mapView_.myLocationEnabled = YES;
    
    geocoder_ = [[GMSGeocoder alloc] init];
    
    [self.view bringSubviewToFront:_topContainerView];
    //[self.view bringSubviewToFront:topTittleBar];
    [self.view bringSubviewToFront:setDelAddButtonView];
    
    if(_isComingFromAddAddressView == 1)
    {
        UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
        [_topContainerView addGestureRecognizer:tapgesture];
    }
    else if (_isComingFromAddAddressView == 2)
    {
        [_topContainerView setHidden:YES];
    }
    
    addLine1View.layer.borderWidth = 0.5;
    addLine1View.layer.borderColor = UIColorFromRGB(0xb7b7b7).CGColor;
    
    addLine2View.layer.borderWidth = 0.5;
    addLine2View.layer.borderColor = UIColorFromRGB(0xb7b7b7).CGColor;

    homeView.layer.borderWidth = 0.5;
    homeView.layer.borderColor = UIColorFromRGB(0xb7b7b7).CGColor;

    zipCodeView.layer.borderWidth = 0.5;
    zipCodeView.layer.borderColor = UIColorFromRGB(0xb7b7b7).CGColor;

    notesView.layer.borderWidth = 0.5;
    notesView.layer.borderColor = UIColorFromRGB(0xb7b7b7).CGColor;
    
    self.navigationItem.title = @"ADD ADDRESS";
    [self createNavLeftButton];
    [self createNavRightButton];
    [self getCurrentLocation];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(showSearchBar:)];
    
    _shadowView = [[UIView alloc] initWithFrame:self.view.bounds];
    _shadowView.backgroundColor = [UIColor blackColor];
    _shadowView.alpha = 0;
    _shadowView.hidden = YES;
    [self.view addSubview:_shadowView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideSearchBar:)];
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideSearchBar:)];
    [_shadowView addGestureRecognizer:tapGesture];
    [_shadowView addGestureRecognizer:swipeGesture];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
//    [self.navigationController.navigationBar setBackgroundImage:[[UINavigationBar appearance] backgroundImageForBarMetrics:UIBarMetricsDefault] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[[UINavigationBar appearance] shadowImage]];
    self.navigationController.navigationBarHidden = NO;
    
    // Ask for My Location data after the map has already been added to the UI.
//    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
//    });
    
}

- (void) viewDidAppear:(BOOL)animated{

    if (IS_SIMULATOR) {
        
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:14];
        
    }
}

/*-------------------------------- Navigation Bar ----------------------------------*/

-(void) createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *buttonImage = [UIImage imageNamed:@"back_btn_on"];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    UIImage *buttonImage1 = [UIImage imageNamed:@"back_btn_off"];
    [navCancelButton setBackgroundImage:buttonImage1 forState:UIControlStateHighlighted];
//    [navCancelButton addTarget:self action:@selector(backButtonActionForNavigation:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];

    
    //[Helper setButton:navCancelButton Text:@"BACK" WithFont:HELVETICA FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    //[navCancelButton setTitleColor:UIColorFromRGB(0x555555) forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
    
}

- (IBAction)showSearchBar:(id)sender
{
    [_searchBar becomeFirstResponder];
    [UIView animateWithDuration:0.25 animations:^{
        _shadowView.alpha = 0.5;
        _shadowView.hidden = NO;
    }];
    [self showContainerViewControllerWithAnimation:YES];
}

- (void)hideSearchBar:(id)sender
{
    _searchBar.text = nil;
   // [_tableView reloadData];
    
    [UIView animateWithDuration:0.25 animations:^{

        _shadowView.alpha = 0.0;
        _shadowView.hidden = YES;
    }];
    [_searchBar resignFirstResponder];
   // [_tableView reloadData];
}


- (void)searchBarCancelButtonClicked:(UISearchBar*)searchBar
{
    [self hideSearchBar:searchBar];
}


-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [Helper setButton:navNextButton Text:@"ADD" WithFont:HELVETICA FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [navNextButton setTitleColor:UIColorFromRGB(0x555555) forState:UIControlStateHighlighted];
    
    [navNextButton setFrame:CGRectMake(0.0,0.0f,45,45)];
    
    [navNextButton addTarget:self action:@selector(addBtncliked:) forControlEvents:UIControlEventTouchUpInside];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}






-(void)addBtncliked:(UIButton *)sender
{

    if (_isComingFromAddAddressView == 2) {
        
        [self backButtonAction:nil];
    }
    else
    {
        
        if(AddressLine1.text.length == 0)
        {
            UIAlertView *alview =[[UIAlertView alloc] initWithTitle:nil message:@"Enter Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alview.tag=10;
            
            [alview show];
            
        }
        else if (addressLine2.text.length ==0)
        {
            UIAlertView *alview =[[UIAlertView alloc] initWithTitle:nil message:@"Enter Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alview.tag=20;
            
            [alview show];
        }
        else if (ZipCode1.text.length == 0)
        {
            
            UIAlertView *alview =[[UIAlertView alloc] initWithTitle:nil message:@"Enter Zipcode" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alview.tag=30;
            [alview show];
        }
        
        else if (notes1.text.length ==0)
        {
            
            UIAlertView *alview = [[UIAlertView alloc] initWithTitle:nil message:@"Enter Tag to Remember This Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alview.tag = 40;
            [alview show];
            
        }
        
        else
        {
            
            
            
            NSInteger randomNumber = arc4random();
            
            NSDictionary *add = @{@"add_line1":AddressLine1.text,
                                  @"add_line2":addressLine2.text,
                                  @"zip":ZipCode1.text,
                                  @"Notes":notes1.text,
                                  @"home":home.text,
                                  @"app_latitude":[NSString stringWithFormat:@"%f",_currentLatitude],
                                  @"app_longitude":[NSString stringWithFormat:@"%f",_currentLongitude],
                                  @"randomNumber":[NSNumber numberWithInteger:randomNumber]
                                  };
            
            Database *db = [[Database alloc] init];
            BOOL isAdded = [db makeDataBaseEntryForAddress:add];
            
            if (isAdded)
            {
                
                [self backButtonAction:nil];
            }
            else
            {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
    }
}

-(void)backButtonAction:(id)sender
{
    if(_isComingFromAddAddressView == 1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(_isComingFromAddAddressView == 2){
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)addCurrentLocationButton
{
    
    UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0,0, 320,64)];
    customView.backgroundColor = [UIColor whiteColor];
    customView.tag = topViewTag;
    [customView setHidden:NO];
    
    _textFeildAddress = [[UILabel alloc]initWithFrame:CGRectMake(12,0,308,60)];
    _textFeildAddress.tag = 101;
    _textFeildAddress.enabled = NO;
    [Helper setToLabel:_textFeildAddress Text:nil WithFont:HELVETICA FSize:14 Color:UIColorFromRGB(0x000000)];
    _textFeildAddress.numberOfLines=2;
    _textFeildAddress.textAlignment = NSTextAlignmentNatural;
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(160 - 25/2,mapView_.frame.size.height/2 - 33/2+80, 25, 33)];
    img.image = [UIImage imageNamed:@"map_icn_currentlocation"];
    img.tag = middleBtnTag;
    [self.view addSubview:img];
    
    UIButton *buttonCurrentLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonCurrentLocation.frame = CGRectMake(280,120,41,41);
    buttonCurrentLocation.tag = curLocBtnTag;
    [buttonCurrentLocation setImage:[UIImage imageNamed:@"map_icn_target"] forState:UIControlStateNormal];
    
    [buttonCurrentLocation addTarget:self action:@selector(getCurrentLocationButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonCurrentLocation];
    
}

- (void)getCurrentLocationButton:(UIButton *)sender {
    
    CLLocation *location = mapView_.myLocation;
    
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    
    // [self changePubNubStream];
    
    if (location) {
        // buttonCurrentLoc.hidden = YES;
        [mapView_ animateToLocation:location.coordinate];
    }
}


-(IBAction)customMarkerTopBottomClicked:(UIButton *) sender
{
    if (_isComingFromAddAddressView == 2)
    {
        [self backButtonAction:nil];
        [saveAddressDelegate currentAddressWithLat:_currentLatitude andLong:_currentLongitude andAddress:destinationAddressLabel.text];
    }
    else
    {
        [self addBtncliked:nil];
    }
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            
            GMSAddress *address = response.firstResult;
            NSLog(@"Adress %@ ",address);
            NSLog(@"zipcode %@",address.postalCode);
            
            NSString *addresstext = [address.lines componentsJoinedByString:@","];
//            apptLocation.srcAddressLine1 = response.firstResult.lines;
//            apptLocation.appointmentAddressLineTwo =  response.firstResult.subLocality;
//            apptLocation.appointmentZipCode = response.firstResult.postalCode;
            _patientAddressLong =  response.firstResult.coordinate.longitude;
            
            
            AddressLine1.text =[NSString stringWithFormat:@"%@",addresstext];
            addressLine2.text = [NSString stringWithFormat:@"%@",response.firstResult.subLocality];
            home.text = [NSString stringWithFormat:@"%@",response.firstResult.locality];
            ZipCode1.text = [NSString stringWithFormat:@"%@",response.firstResult.postalCode];
            
            if ([ZipCode1.text isEqualToString:@"(null)"]) {
                ZipCode1.text = @"";
                
            }
            
            if ([AddressLine1.text isEqualToString:@"(null)"]) {
                AddressLine1.text = @"";
            }
            
            if ([addressLine2.text isEqualToString:@"(null)"]) {
                addressLine2.text = @"";
            }
            
            if ([home.text isEqualToString:@"(null)"]) {
                home.text = @"";
            }
            
            CLLocationCoordinate2D addressCoordinates = address.coordinate;
            
            _currentLatitude = addressCoordinates.latitude;
            _currentLongitude = addressCoordinates.longitude;
            
            destinationAddressLabel.text = addresstext;
            
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}


-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
        
    }
    else
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
#pragma mark - CLaddaddressDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        //change map camera postion to current location
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                longitude:newLocation.coordinate.longitude
                                                                     zoom:15];
        [mapView_ setCamera:camera];
        
        
        //add marker at current location
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        //save current location to plot direciton on map
        _currentLatitude = newLocation.coordinate.latitude;
        _currentLongitude =  newLocation.coordinate.longitude;
        
        
        //get address for current location
        [self getAddress:position];
        
        
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        
    }
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:[error localizedDescription] On:self.view];
}

#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
    if (_isMarkerClicked) {
        _isMarkerClicked = NO;
        return;
        
    }
    
    if (_isUpdatedLocation) {
        
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        NSLog(@"center Coordinate idleAtCameraPosition :%f %f",coor.latitude,coor.longitude);
        
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
        
        [self getAddress:coor];
    }
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
    
    
}

/**
 *  add another view that will open after clicking setpickup location on map
 */

-(void)addCustomLocationPikUpViewForConfirmScreen
{
    if(!PikUpViewForConfirmScreen)
    {
        PikUpViewForConfirmScreen = [[UIView alloc]initWithFrame:CGRectMake(0,64,320,0)];
        PikUpViewForConfirmScreen.backgroundColor = [UIColor whiteColor];
        PikUpViewForConfirmScreen.tag = 150;
        PikUpViewForConfirmScreen.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_locationbar.png"]];
        
        CGRect basketTopFrame = PikUpViewForConfirmScreen.frame;
        basketTopFrame.size.height = 44;
        
        UIImageView *imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(5,0, 45, 45)];
        imgStr.image =  [UIImage imageNamed:@"conformation_icn_currentlocation"];
        [PikUpViewForConfirmScreen addSubview:imgStr];
        
        UITextField *pickUpAddress = [[UITextField alloc]initWithFrame:CGRectMake(50,2,215, 42)];
        pickUpAddress.placeholder = @"Current Location";
        pickUpAddress.tag = pickupAddressTag;
        pickUpAddress.enabled = NO;
        pickUpAddress.delegate = self;
        pickUpAddress.textAlignment = NSTextAlignmentCenter;
        pickUpAddress.font = [UIFont fontWithName:HELVETICA size:10];
        [PikUpViewForConfirmScreen addSubview:pickUpAddress];
        
        
        UIButton *pickUpAddressButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pickUpAddressButton.frame = CGRectMake(50,2,215, 42);
        pickUpAddressButton.tag = 155;
        //[buttonCurrentLocation setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@""]]];
        // [pickUpAddressButton addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        [PikUpViewForConfirmScreen addSubview:pickUpAddressButton];
        
        
        
        UIButton *addDropLocButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addDropLocButton.frame = CGRectMake(280,11,20,20);
        addDropLocButton.tag = 151;
        //[Helper setButton:addDropLocButton Text:@"+" WithFont:Roboto_Regular FSize:20 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
        //[addDropLocButton addTarget:self action:@selector(plusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [PikUpViewForConfirmScreen addSubview:addDropLocButton];
        
        [UIView animateWithDuration:0.3
                              delay:0.2
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             // basketTop.frame = basketTopFrame;
                             // basketBottom.frame = basketBottomFrame;
                             // LV.frame = RTEC;
                             PikUpViewForConfirmScreen.frame = basketTopFrame;
                         }
                         completion:^(BOOL finished){
                         }];
        
        UIView *cuView = [self.view viewWithTag:topViewTag];
        UITextField *textAddress = (UITextField *)[cuView viewWithTag:101];
        pickUpAddress.text =  textAddress.text;
        srcAddr = pickUpAddress.text;
        {
            // _currentLatitudeFare = _currentLatitude;
            // _currentLongitudeFare =  _currentLongitude;
            
        }
        [self.view addSubview:PikUpViewForConfirmScreen];
        
        /**
         *  Remove Location View From superview Add it again if Required
         *  / ADDED IN CANCELBUTTONCLICKED AND AFTER GETTING THE RESPONSE
         */
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        [customLocationView removeFromSuperview];
    }
    
}


-(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
    
}

#pragma textfield delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    if (textField == AddressLine1) {
        [addressLine2 becomeFirstResponder];
    }
    else if (textField == addressLine2)
    {
        
        [ZipCode1 becomeFirstResponder];
    }
    else if (textField == ZipCode1)
    {
        
        [home becomeFirstResponder];
    }
    else if (textField == home)
    {
        
        [notes1 becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

-(void)pickedAddressWithLat:(float)latitude andLong:(float)longitude andAddress:(NSDictionary *)addressText
{
//    [self.navigationController popViewControllerAnimated:NO];
//    [saveAddressDelegate currentAddressWithLat:latitude andLong:longitude andAddress:addressText];
    
    AddressLine1.text = addressText[@"add1"];
    addressLine2.text = addressText[@"add2"];
    home.text = addressText[@"Home"];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[addressText objectForKey:@"add2"]]);
    _currentLatitude = [addressText[@"lat"] doubleValue];
    _currentLongitude = [addressText[@"long"] doubleValue];
    [self showContainerViewControllerWithAnimation:NO];
    
    
}


#pragma mark -
#pragma mark Search Bar Delegates
#pragma mark - Autocomplete SearchBar methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBar resignFirstResponder];
    //[self.mapPickerTableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [self.searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Length: %lu",(unsigned long)searchWordProtection.length);
    
    if (searchWordProtection.length != 0) {
        
        [self runScript];
        
    } else {
        NSLog(@"The searcTextField is empty.");
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    self.substring = [NSString stringWithString:self.searchBar.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
        NSLog(@"This string: %@ had a space at the begining.",self.substring);
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self showSearchBar:nil];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.arrayOfAddress removeAllObjects];
    [_bookingControllerVC getAddressOfArrayAndReloadTableView:_arrayOfAddress];
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        NSLog(@"Search: %lu",(unsigned long)self.pastSearchResults.count);
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            [self.arrayOfAddress addObjectsFromArray:results];
            NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
            [self.pastSearchResults addObject:searchResult];
               [_bookingControllerVC getAddressOfArrayAndReloadTableView:_arrayOfAddress];
            
        }];
        
    }else {
        
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.arrayOfAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [_bookingControllerVC getAddressOfArrayAndReloadTableView:_arrayOfAddress];
            }
        }
    }
}


#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%f,%f&radius=500po &language=en&key=%@",searchWord,_currentLatitude,_currentLongitude,apiKeysd];
        NSLog(@"AutoComplete URL: %@",urlString);
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    _bookingControllerVC = [segue destinationViewController];
    _bookingControllerVC.pickAddressDelegate = self;
    
}

- (void)showContainerViewControllerWithAnimation:(BOOL)isShowing {
    
    if (isShowing) {
        
    _arrayOfAddress = [[NSMutableArray alloc]init];
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options: UIViewAnimationOptionTransitionNone|UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         CGRect frameTopview = _tableContainerView.frame;
                         frameTopview.origin.y = self.view.frame.size.height - 504;
                         _tableContainerView.frame = frameTopview;
                         [self.view bringSubviewToFront:_tableContainerView];

                         
                     }
     
                     completion:^(BOOL finished){
                         
                     }];
    }
    else {
        
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options: UIViewAnimationOptionTransitionNone|UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             CGRect frameTopview = _tableContainerView.frame;
                             frameTopview.origin.y = self.view.frame.size.height+100;
                             _tableContainerView.frame = frameTopview;
                             [self.view bringSubviewToFront:_tableContainerView];
                             
                             
                         }
         
                         completion:^(BOOL finished){
                             
                         }];

    }
    
}


@end
