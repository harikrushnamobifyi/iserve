//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import <Canvas/CSAnimationView.h>
#import "UILabel+DynamicHeight.h"
static NSBundle *bundle = nil;
@interface HelpViewController ()
{
    NSArray *typeO;
    NSArray *typeoDetails;

}
@end

@implementation HelpViewController
@synthesize topView;
@synthesize signInButton;
@synthesize registerButton;
@synthesize mainScrollView;
@synthesize pageControl;
@synthesize orLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default"]];
    
//    typeO = [[NSArray alloc]initWithObjects:@"See Available Guydds",@"Browse Profiles",@"Meet your Guydd",@"TELL US HOW IT WENT!",@"",nil];
//    typeoDetails = [[NSArray alloc]initWithObjects:@"Just tap a Guydd’s picture to see their profile.",@"See a Guydd’s profile and reviews, then book them if you like.",@"Within minutes you’re Guydd will meet you and your best vacation ever will begin!.",@"When you’re done, just pay with your phone and rate your Guydd.",@"",@"",nil];
//    [self createScrollingView];
    [self.view bringSubviewToFront:topView];
    [self.view bringSubviewToFront:pageControl];
    
    topView.frame = CGRectMake(15,498, 290,47);
   // topView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"roadyo_footer.png"]];
    [self animateIn];
    
    [Helper setButton:signInButton Text:@"Login" WithFont:OpenSans_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    signInButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [signInButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    signInButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [signInButton setBackgroundImage:[UIImage imageNamed:@"FrontScreen_login_btn_off"] forState:UIControlStateNormal];
     [signInButton setBackgroundImage:[UIImage imageNamed:@"FrontScreen_login_btn_on"] forState:UIControlStateSelected];


    [Helper setButton:registerButton Text:@"Register" WithFont:OpenSans_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [registerButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    registerButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [registerButton setBackgroundImage:[UIImage imageNamed:@"register_btn_off"] forState:UIControlStateNormal];

      [registerButton setBackgroundImage:[UIImage imageNamed:@"register_btn_on"] forState:UIControlStateSelected];

  
    //orLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"roadyo_btn_or_on.png"]];
 
    _swpLabel.frame = CGRectMake(240,[UIScreen mainScreen].bounds.size.height-64-50,60,30);

    [self.view bringSubviewToFront:_swpLabel];
    [Helper setToLabel:_swpLabel Text:@"Swipe up" WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0xffffff)];

    pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(280,[UIScreen mainScreen].bounds.size.height-64-50,50,20)];
    pageControl.numberOfPages = 5;
    pageControl.currentPage = 0;
    pageControl.backgroundColor = CLEAR_COLOR;
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:0.972 green:0.814 blue:0.050 alpha:1.000];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithWhite:0.922 alpha:1.000];;
    [pageControl addTarget:self action:@selector(pageControlClicked:) forControlEvents:UIControlEventValueChanged];
    pageControl.transform = CGAffineTransformMakeRotation(M_PI / 2);
    [pageControl setHidden:YES];

    [self.view addSubview:pageControl];


}
- (void)animateIn
{
    CGRect frameTopview = topView.frame;
    frameTopview.origin.y = self.view.bounds.size.height - 65;
  
    [UIView animateWithDuration:0.6f animations:^{
        topView.frame = frameTopview;
    }];
}
-(void)createScrollingView {
    
    NSArray *colors = [[NSArray alloc]initWithObjects:
                       [UIImage imageNamed:@"Default"],
                       [UIImage imageNamed:@"landing_pickuplocation-568h"],
                       [UIImage imageNamed:@"landing_payment-568h"],
                       [UIImage imageNamed:@"landing_recipt-568h"],
                       [UIImage imageNamed:@"landing_rating-568h"],
                       nil];
    
    CGRect screen =  [[UIScreen mainScreen]bounds];
    
    
    mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320,screen.size.height)];
    mainScrollView.bounces = NO;
    mainScrollView.clipsToBounds = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
    
    [self.view addSubview:mainScrollView];
    
    self.mainScrollView.delegate = self;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height * 5);
    
    
    for (int widOffset = 0; widOffset < 5; widOffset++)
    {
        
        CGRect frame;
        frame.origin.x = 0;
        frame.origin.y = self.mainScrollView.frame.size.height * widOffset;
        frame.size = self.mainScrollView.frame.size;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:frame];
        imgView.image = [colors objectAtIndex:widOffset];
        
        if(widOffset != 0)
        {
            UIView *txtContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, imgView.frame.size.height/2+40,320, 130)];
            txtContainerView.backgroundColor = CLEAR_COLOR;
            UILabel *txtLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,0, 300, 30)];
            txtLabel.backgroundColor = CLEAR_COLOR;
            txtLabel.textAlignment = NSTextAlignmentCenter;
            [Helper setToLabel:txtLabel Text:[typeO objectAtIndex:widOffset-1] WithFont:Roboto_Regular FSize:15 Color:UIColorFromRGB(0xffffff)];
            [txtContainerView addSubview:txtLabel];
            
            UILabel *txtDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,30, 280,100)];
            txtDetailLabel.backgroundColor = CLEAR_COLOR;
            txtDetailLabel.numberOfLines = 0;
                        txtDetailLabel.textAlignment = NSTextAlignmentCenter;
            [Helper setToLabel:txtDetailLabel Text:[typeoDetails objectAtIndex:widOffset-1] WithFont:Roboto_Light FSize:15 Color:UIColorFromRGB(0x999999)];
            CGSize size = [txtDetailLabel sizeOfMultiLineLabel];
            CGRect rect = txtDetailLabel.frame;
            rect.size.height = size.height;
            txtDetailLabel.frame = rect;
            [txtContainerView addSubview:txtDetailLabel];

            [imgView addSubview:txtContainerView];
        }
        
        [self.mainScrollView addSubview:imgView];
    }
    
    [mainScrollView setPagingEnabled:YES];
    [self.mainScrollView setScrollEnabled:YES];
    pageControl.transform = CGAffineTransformMakeRotation(M_PI/2.0);

}
#pragma mark-scrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageHeight = self.mainScrollView.frame.size.height;
    // you need to have a **iVar** with getter for scrollView
    
    float fractionalPage = self.mainScrollView.contentOffset.y / pageHeight;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
  
    if(page>0){
      [_swpLabel setHidden:YES];
        [pageControl setHidden:NO];
    }
    else{
        [_swpLabel setHidden:NO];
        [pageControl setHidden:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
    if(registerButton.selected == NO){
        [signInButton setSelected:YES];
    }
    else
        [signInButton setSelected:NO];


    self.navigationController.navigationBarHidden = YES;
    
    self.navigationItem.hidesBackButton = YES;
}
-(void)viewDidDisappear:(BOOL)animated{
    //self.navigationController.navigationBarHidden = NO;
    
    //self.navigationItem.hidesBackButton = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInButtonClicked:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    for (UIView *button in mBtn.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    mBtn.selected = YES;

    [mBtn setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    [self performSegueWithIdentifier:@"SignIn" sender:self];
    
}

- (IBAction)registerButtonClicked:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    
    for (UIView *button in mBtn.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
            [(UIButton *)button setHighlighted:NO];
        }
    }
    mBtn.selected = YES;
   
    [mBtn setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];

    [self performSegueWithIdentifier:@"SignUp" sender:self];
}

- (IBAction)pageControlClicked:(id)sender
{
    TELogInfo(@"pageControl position %ld", (long)[self.pageControl currentPage]);
    //Call to remove the current view off to the left
    unsigned long int page = self.pageControl.currentPage;
    
    CGRect frame = mainScrollView.frame;
    frame.origin.x = 0;
    frame.origin.y = frame.size.width * page;
    [mainScrollView scrollRectToVisible:frame animated:YES];
    
    
}

@end
