//
//  SignInViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface SignInViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate>
{
    NSDictionary *itemList;
    BOOL checkLoginCredentials;
    NSDictionary *facebookData;
}

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UIView *containEmailnPass;
@property (weak, nonatomic)   IBOutlet UIButton *signinButton;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) UIButton *navDoneButton;
@property (strong, nonatomic) UIImageView *emailImageView;
@property (strong, nonatomic) UIImage *pickedImage;

- (IBAction)forgotPasswordButtonClicked:(id)sender;
- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)logInWithFacebookButtonAction:(id)sender;

@end
