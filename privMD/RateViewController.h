//
//  RateViewController.h
//  privMD
//
//  Created by Rahul Sharma on 10/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RatingViewTextDelegate <NSObject>

@required

-(void)hasTextChanges:(NSString *)text;

@end
@interface RateViewController : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak,nonatomic) id<RatingViewTextDelegate> delegate;
@property (strong, nonatomic) NSString *reviewText;
-(IBAction)backButtonClicked:(id)sender;
-(IBAction)doneButtonClicked:(id)sender;
-(IBAction)dismissKeyboard:(id)sender;

@end
