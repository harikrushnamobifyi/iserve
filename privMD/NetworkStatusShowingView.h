//
//  NetworkStatusShowingView.h
//  UBER
//
//  Created by Rahul Sharma on 03/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkStatusShowingView : UIView
{
    NSTimer *timer;
}
+ (id)sharedInstance ;
+(void)removeViewShowingNetworkStatus;

@end
