//
//  InviteViewController.m
//  UBER
//
//  Created by Rahul Sharma on 06/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "InviteViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <FacebookSDK/FacebookSDK.h>
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"


#define imgLinkForSharing @"http://www.ideliver.mobi/iServe/icon/Icon-40@3x.png"

@interface InviteViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate>
{
    MFMailComposeViewController *mailer;
    NSString *shareText;
    NSString *shareText1;
}
@property (strong, nonatomic) IBOutlet UILabel *shareLable;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeMsgShowingLabel;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeLablel;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UILabel *fbShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UILabel *twitterShareLAble;
@property (strong, nonatomic) IBOutlet UIButton *smsButton;
@property (strong, nonatomic) IBOutlet UILabel *smsShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UILabel *emailShareLable;
@property (strong, nonatomic) IBOutlet UILabel *shareyourcodeLabel;

- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)smsButtonClicked:(id)sender;
- (IBAction)emailButtonClicked:(id)sender;

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBar];
    NSString *pCode = flStrForObj([[NSUserDefaults standardUserDefaults]objectForKey:kNSUPatientCouponkey]);
    [Helper setToLabel:_shareLable Text:@"SHARE iSERVE" WithFont:Roboto_Bold FSize:12 Color:UIColorFromRGB(0x656565)];
    
    shareText = @"Share iServe with your friends and family and we will reward you with some discounts on your future booking \n";
    [Helper setToLabel:_promoCodeMsgShowingLabel Text:shareText WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];

     shareText1 = @"Download iServe using my referral code “    “ and earn discounts on booking essential services on the mobile app";
    
    shareText = nil;
    shareText = @"Hi,\nDownload iServe using my referral code  and earn discounts on booking essential services on the mobile app";

    
    [Helper setToLabel:_promoCodeLablel Text:pCode WithFont:Roboto_Light FSize:33 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_shareyourcodeLabel Text:@"SHARE YOUR CODE" WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];

    [Helper setToLabel:_fbShareLabel Text:@"FACEBOOK" WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_twitterShareLAble Text:@"TWITTER" WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_smsShareLabel Text:@"MESSAGE" WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_emailShareLable Text:@"EMAIL" WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];

    
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"SHARE"];
    [customNavigationBarView hideRightBarButton:YES];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{

    [self menuButtonclicked];

}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)facebookButtonClicked:(id)sender {
    
    [self shareOnFacebook];
}

- (IBAction)twitterButtonClicked:(id)sender {
    [self shareOnTwitter];
}

- (IBAction)smsButtonClicked:(id)sender {
    
    [self shareViaSMS];
}

- (IBAction)emailButtonClicked:(id)sender {
    [self EmaiSharing];
}



-(void)shareViaSMS {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"ALERT" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"%@",shareText1];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    messageController.navigationController.navigationBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_navigationbar"]];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self.navigationController presentViewController:messageController animated:YES completion:nil];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)shareOnTwitter
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
           NSString *message = [NSString stringWithFormat:@"%@",shareText];
        
        [tweetSheet setInitialText:message];
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
        [tweetSheet addImage:image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Message"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


-(void)shareOnFacebook
{
    [self publishWithWebDialog];
}

/*
 * Share using the Web Dialog
 */
- (void) publishWithWebDialog {
    // Put together the dialog parameters
    
        NSString *message = [NSString stringWithFormat:@"%@",shareText1];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"iServe",@"name",
                                   message,@"description",imgLinkForSharing,@"picture",nil];
    
    
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or publishing a story.
             [self showAlert:[self checkErrorMessage:error]];
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 TELogInfo(@"User canceled story publishing.");
             } else {
                 // Handle the publish feed callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"post_id"]) {
                     // User clicked the Cancel button
                     TELogInfo(@"User canceled story publishing.");
                 } else {
                     // User clicked the Share button
                     [self showAlert:[self checkPostId:urlParams]];
                 }
             }
         }
     }];
}

/*
 * Helper method to parse URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}


#pragma mark - Helper methods

- (NSString *)checkErrorMessage:(NSError *)error {
   
    NSString *errorMessage = @"";
    errorMessage = @"Operation failed due to a connection problem, retry later.";
    return errorMessage;
}

- (NSString *) checkPostId:(NSDictionary *)results {
   
    NSString *message = @"Posted successfully.";
   
    return message;
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Result"
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

-(void)EmaiSharing
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                               fontWithName:OpenSans_Regular size:14], NSFontAttributeName,
                                    [UIColor whiteColor], NSForegroundColorAttributeName, nil];
        [[[[UINavigationBar appearance] topItem] rightBarButtonItem]setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        [[[UINavigationBar appearance] topItem] setTitle:@"E-mail Sharing"];
        [[UINavigationBar appearance]setBackgroundImage:[UIImage imageNamed:@"login_navigationbar"] forBarMetrics:UIBarMetricsDefault];
        [[[[UINavigationBar appearance] topItem ] leftBarButtonItem]setTintColor:[UIColor whiteColor]];
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        NSString *shareUrl =  [NSString stringWithFormat:@"%@",shareText];
        [mailer setSubject:@"Download iServe and earn discounts"];
    
        NSArray *toRecipents;
        NSMutableString* message =[[NSMutableString alloc] init];
        [message appendString:shareUrl];
        toRecipents = [NSArray arrayWithObject:@""];
        [mailer setMessageBody:message isHTML:NO];
        //[mailer setToRecipients:toRecipents];
        NSString *strURL = [NSString stringWithFormat:@"%@",imgLinkForSharing];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]]];
        
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        if (!photoData) {
            
            [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
            
            [self.navigationController presentViewController:mailer animated:YES completion:^{
                [[UINavigationBar appearance] setTitleTextAttributes:attributes];
            }];
        }
       
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
}
#pragma mark - MFMailComposeViewController Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
