//
//  PickUpViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 04/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpViewController.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"

@interface PickUpViewController ()
@property(nonatomic,assign) BOOL isSearchResultCome;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;

@end

@implementation PickUpViewController
@synthesize onCompletion;
@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Search";
    
    _mAddress = [NSMutableArray array];
    
    self.searchBarController.searchBarStyle = UISearchBarStyleProminent;
    [self createNavLeftButton];
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    [self createFooterViewForTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    PatientAppDelegate *shared =  (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    [shared setupAppearance];
    
    self.navigationController.navigationBarHidden = NO;
    
}

- (void)viewWillDisAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    
    [refreshControl endRefreshing];
}

-(void) createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,44,44)];
    
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"Back", @"Back") WithFont:Roboto_Light FSize:11 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    
    [navCancelButton setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [_searchBarController becomeFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark -
#pragma mark UITableView DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _mAddress.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell=nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundColor=[UIColor clearColor];
    
    if(cell==nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:Roboto_Light size:15];
        cell.textLabel.textColor = UIColorFromRGB(0x000000);
        cell.detailTextLabel.font = [UIFont fontWithName:Roboto_Light size:13];
        cell.detailTextLabel.textColor = UIColorFromRGB(0xa0a0a0);
        cell.detailTextLabel.numberOfLines = 2;
    }
    
    NSDictionary *searchResult = [_mAddress objectAtIndex:indexPath.section];
    cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
    cell.detailTextLabel.text = searchResult[@"description"];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 25;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.section];
    NSString *placeID = [searchResult objectForKey:@"reference"];
    [self.searchBarController resignFirstResponder];
    [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place) {
        
        NSDictionary *dict = [NSDictionary dictionary];
        if (onCompletion) {
            
            
            NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
            NSString *add2 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"formatted_address"])];
            if (add1.length == 0) {
                
                add1 = [add1 stringByAppendingString:flStrForStr([place valueForKey:@"formatted_address"])];
                add2 = @"";
            }
            
            NSString *late = [NSString stringWithFormat:@"%@,",[place valueForKey:@"geometry"][@"location"][@"lat"]];
            NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
            
            dict = @{@"address1":add1,
                     @"address2":add2,
                     @"lat":late,
                     @"lng":longi,
                     };
            
            onCompletion(dict,locationType);
        }
        [self gotoViewController:dict];
        
    }];


}
-(void)gotoViewController:(NSDictionary *)dictionary {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Result", @"Result");
}

- (void)createFooterViewForTable {
    
    UIView *footerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320,98/2)];
    footerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredby_google"]];
    imageView.frame = CGRectMake(0,0, 320,98/2);
    [footerView addSubview:imageView];
    self.tblView.tableFooterView = footerView;
}

#pragma mark -
#pragma mark Search Bar Delegates
#pragma mark - Autocomplete SearchBar methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBarController resignFirstResponder];
    [self.tblView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [self.searchBarController.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length != 0) {
        
        [self runScript];
        
    } else {
       
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    self.substring = [NSString stringWithString:self.searchBarController.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.mAddress removeAllObjects];
    [self.tblView reloadData];
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            [self.mAddress addObjectsFromArray:results];
            isSearchResultCome = YES;
            NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
            [self.pastSearchResults addObject:searchResult];
            [self.tblView reloadData];
            
        }];
        
    }else {
        
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.mAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [self.tblView reloadData];
            }
        }
    }
}


#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete {
    
    NSString *const apiKey = @"AIzaSyCvqXtL6o2DafEgKgmJxNXAoUSG_bqaXoM";
    
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500po &language=en&key=%@",searchWord,latitude,longitude,apiKey];
        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
    
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
   
    NSString *const apiKey = @"AIzaSyCvqXtL6o2DafEgKgmJxNXAoUSG_bqaXoM";

    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,apiKey];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
}


@end
