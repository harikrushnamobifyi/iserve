//
//  PickAddressContainerView.h
//  Deliver_Bird
//
//  Created by Rahul Sharma on 05/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickAddressDelegate <NSObject>

-(void)pickedAddressWithLat:(float)latitude andLong:(float)longitude andAddress:(NSDictionary *)addressText;

@end

@interface PickAddressContainerView : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) id <PickAddressDelegate> pickAddressDelegate;

@property (nonatomic, copy)   void (^onCompletion)(NSDictionary *suburb,NSInteger locationtype);
@property (strong, nonatomic) IBOutlet UITableView *mapPickerTableView;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (assign, nonatomic) NSInteger locationType;
@property (nonatomic,assign) BOOL isSearchResultCome;

@property (strong, nonatomic) NSMutableArray *arrayOfAddress;

-(void)getAddressOfArrayAndReloadTableView:(NSMutableArray *)array;


@end
