//
//  ProListViewController.h
//  iserve
//
//  Created by Rahul Sharma on 4/6/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *proListTableView;
@property (weak, nonatomic) IBOutlet UIButton *bookNowButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UILabel *myCurrentAddressLabel;

- (IBAction)dateButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
- (IBAction)changeAddressButtonClicked:(id)sender;

@end
