//
//  ProListTableViewCell.h
//  iserve
//
//  Created by Rahul Sharma on 4/6/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView.h>

@interface ProListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *proImageView;
@property (weak, nonatomic) IBOutlet UILabel *proNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfReviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricePerHourLabel;

@end
