//
//  ProListViewController.m
//  iserve
//
//  Created by Rahul Sharma on 4/6/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ProListViewController.h"
#import "ProListTableViewCell.h"
#import "PickUpViewController.h"

@interface ProListViewController ()

@end

@implementation ProListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 10)];
    view.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.5];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier=@"ProListCell";
    ProListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.proImageView.image = [UIImage imageNamed:@"haeder_background_image"];
    cell.proImageView.layer.cornerRadius = cell.proImageView.frame.size.width/2;
    [cell.proImageView setClipsToBounds:YES];
    cell.proNameLabel.text = @"James Arthur";
    cell.numberOfReviewLabel.text = [NSString stringWithFormat:@"%@ Review(s)",@"20"];
    cell.starRatingView.value = 5;
    cell.distanceLabel.text = [NSString stringWithFormat:@"%@ Miles away",@"20"];
    cell.pricePerHourLabel.text = [NSString stringWithFormat:@"$ %0.2f",33.23];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"docDetailsVC" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)dateButtonClicked:(id)sender {
    
}

- (IBAction)backButtonClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeAddressButtonClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"pickup" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([[segue identifier] isEqualToString:@"pickup"]) {
        
        PickUpViewController *pickController = [segue destinationViewController];
        pickController.latitude =  [NSString stringWithFormat:@"%f",13.45];
        pickController.longitude = [NSString stringWithFormat:@"%f",77.44];
        pickController.onCompletion = ^(NSDictionary *dict,NSInteger type){
        [self pickupAddressFromSearch:dict];
            
        };
        
    }
}

-(void)pickupAddressFromSearch:(NSDictionary *)addressDetails {
    
    NSString *addressText = flStrForStr(addressDetails[@"address1"]);
    if ([addressDetails[@"address2"] rangeOfString:addressText].location == NSNotFound) {
        addressText = [addressText stringByAppendingString:@","];
        addressText = [addressText stringByAppendingString:flStrForStr(addressDetails[@"address2"])];
    } else {
        
        addressText = flStrForStr(addressDetails[@"address2"]);
    }
    _myCurrentAddressLabel.text = addressText;
}

@end
