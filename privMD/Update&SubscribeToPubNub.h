//
//  Update&SubscribeToPubNub.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 26/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol updateAndSubscribeToPubNubDelegate  <NSObject>

@required

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDictionary OnChannel:(NSString *)channel;

@end

@interface Update_SubscribeToPubNub : NSObject

@property(assign,nonatomic) id <updateAndSubscribeToPubNubDelegate> delegate;

@end
