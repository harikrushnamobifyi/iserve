//
//  ChatVC.m
//  FreeTaxi
//
//  Created by Rahul Sharma on 3/20/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "ChatVC.h"
#import "Message.h"
#import "DataBase.h"
#import "XDKAirMenuController.h"
#import "Helper.h"
#import "ContentManager.h"
#import "PatientPubNubWrapper.h"

@interface ChatVC () <PatientPubNubWrapperDelegate>
{
    PatientPubNubWrapper *pubNub;
}
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) UIImage *myImage;
@property (strong, nonatomic) UIImage *partnerImage;
@property (strong,nonatomic) NSString *serverPubNubChannel;
@end

@implementation ChatVC
@synthesize driverChannel,bookingId,arnID;

-(void) createNavLeftButton
{
    // UIView *navView = [[UIView new]initWithFrame:CGRectMake(0, 0,50, 44)];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:navCancelButton Text:@"BACK" WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:@"BACK" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"BACK" forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)getMessagesFromServer {
    
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];

    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }

    
    NSDictionary *dict = @{@"ent_sess_token":sessionToken,
                           @"ent_dev_id":deviceID,
                           @"ent_user_type":@"2",
                           @"ent_bid":flStrForObj(bookingId),
                           };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getChatHistory"
                                    paramas:dict
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       TELogInfo(@"response %@",response);
                                       
                                       if ([response[@"errFlag"] integerValue] == 0) {
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                           NSLog(@"Messages from Response: %@", response);
                                           [self loadMessages:response[@"chat"]];
                                       }
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

- (void)loadMessages:(NSArray *)messages
{
    self.dataSource = [[NSMutableArray alloc] init];
    for (long i = 0; i < [messages count]; i++) {
        Message *msg = [[Message alloc] init];
        msg.text = messages[i][@"message"];
        if ([messages[i][@"from"] integerValue] == 1) {
            msg.fromMe = NO;
        }else {
            msg.fromMe = YES;
        }
        [self.dataSource addObject:msg];
    }
    
    [self refreshMessages];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.myImage      = [UIImage imageNamed:@"active"];
    self.partnerImage = [UIImage imageNamed:@"amex"];
    [self createNavLeftButton];
     pubNub = [PatientPubNubWrapper sharedInstance];
    pubNub.delegate = self;
    _serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDPublishStreamChannel];
    self.title = @"Chat";

    [self getMessagesFromServer];
    self.dataSource = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {

    [self subscribeToPassengerChannel];
}
-(void)viewWillDisappear:(BOOL)animated {



}

//- (void)loadMessages:(NSArray *)messages
//{
//    self.dataSource = [[NSMutableArray alloc] init];
//    for (long i = [messages count]-1; i >= 0; i--) {
//        Message *msg = [[Message alloc] init];
//        msg.text = messages[i][@"message"];
//        msg.fromMe = [messages[i][@"fromMe"] boolValue];
//        [self.dataSource addObject:msg];
//    }
//    [self refreshMessages];
//    
//}

- (NSMutableArray *)messages
{
    return self.dataSource;
    
}

- (void)configureMessageCell:(SOMessageCell *)cell forMessageAtIndex:(NSInteger)index
{
    NSLog(@"INDEX is: %ld", (long)index);
    Message *message = self.dataSource[index];
    
    // Adjusting content for 3pt. (In this demo the width of bubble's tail is 6pt)
    if (!message.fromMe) {
        cell.contentInsets = UIEdgeInsetsMake(0, 3.0f, 0, 0); //Move content for 3 pt. to right
        cell.textView.textColor = [UIColor blackColor];
    } else {
        cell.contentInsets = UIEdgeInsetsMake(0, 0, 0, 3.0f); //Move content for 3 pt. to left
        cell.textView.textColor = [UIColor blackColor];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - SOMessaging delegate
- (void)didSelectMedia:(NSData *)media inMessageCell:(SOMessageCell *)cell
{
    // Show selected media in fullscreen
    [super didSelectMedia:media inMessageCell:cell];
}

- (void)messageInputView:(SOMessageInputView *)inputView didSendMessage:(NSString *)message
{
    if (![[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        return;
    }
    
    Message *msg = [[Message alloc] init];
    msg.text = message;
    msg.fromMe = YES;
    msg.date = [NSDate date];
    //msg.type = 1;
    [self sendMessage:msg];
    [self sendMessageToDriver:message];
}

- (void)messageInputViewDidSelectMediaButton:(SOMessageInputView *)inputView
{
    // Take a photo/video or choose from gallery
}

- (void)dismissViewController:(UIButton *)sender {
 
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)subscribeToPassengerChannel {
    
    pubNub = [PatientPubNubWrapper sharedInstance];
    
    NSString *myChn = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    
    [pubNub subscribeOnChannels:@[myChn]];
    
    [pubNub setDelegate:self];
}

#pragma PubNubWrapper 
-(void)recievedMessage:(NSDictionary*)messageDict onChannel:(NSString*)channelName {
    
    if ([messageDict[@"a"] integerValue] == 21)
    {
        
        Message *msg = [[Message alloc] init];
        msg.text = messageDict[@"msg"];
        msg.fromMe = NO;
        [self.dataSource addObject:msg];
    
        [self refreshMessages];
    
    }
}


-(void)sendMessageToDriver:(NSString *)message {
    
    NSDictionary *messages = @{@"a":[NSNumber numberWithInt:21],
                               @"msg":message,
                               @"chn":flStrForObj(driverChannel),
                               @"bid":flStrForObj(bookingId),
                               @"uType":@"2",
                               @"arn":arnID,
                               };

    NSLog(@"Sending Chat Message : %@ ",messages);
    
    [pubNub sendMessageAsDictionary:messages toChannel:_serverPubNubChannel];
}

-(void)sentMessageFailedonChannel:(NSString*)channelName error:(NSError*)withError {

    
}

@end
