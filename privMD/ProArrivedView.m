//
//  ProArrivedView.m
//  Servodo
//
//  Created by Rahul Sharma on 18/06/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "ProArrivedView.h"
#import "AppointedDoctor.h"
#import "UIImageView+WebCache.h"
#import "RoundedImageView.h"
#import "MyAppTimerClass.h"

#define proReachedViewTag 4003


@interface ProArrivedView()
{
    unsigned long long secondInt;
    unsigned long long minuteInt;
    unsigned long long hourInt;
    NSString * result1;
}

@property (strong, nonatomic) IBOutlet UIScrollView *mainView;
@property (strong, nonatomic) IBOutlet UIImageView *proImgView;
@property (strong, nonatomic) IBOutlet UILabel *proNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *proContactNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *countDownTimerLabel;


- (IBAction)contactButtonClicked:(id)sender;


@end

@implementation ProArrivedView
@synthesize proContactNoLabel,proImgView,proNameLabel,countDownTimerLabel;
@synthesize upTimer;

static ProArrivedView  *arrivedView = nil;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      self =  [[[NSBundle mainBundle] loadNibNamed:@"GuyddArrivedView" owner:self options:nil]objectAtIndex:0];
       [self makeValueDefaultAfterBookingCompletion];
    }
    return self;
}


+ (id)sharedInstance {
    
	if (!arrivedView) {

        arrivedView  = [[self alloc] initWithFrame:CGRectZero];
        arrivedView.frame = CGRectMake(10,140,320,320);

        
	}
	
	return arrivedView;
}


-(void)mainViewContent{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
   
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,[ud objectForKey:@"driverpic"]];
    
    [proImgView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
               placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                      }];
    
    proImgView.layer.cornerRadius = proImgView.frame.size.width/2;
        proImgView.clipsToBounds = YES;
    proImgView.layer.borderColor = [UIColor colorWithRed:0.118 green:0.522 blue:0.812 alpha:1.000].CGColor;

    [Helper setToLabel:proNameLabel Text:[ud objectForKey:@"drivername"] WithFont:Roboto_Bold FSize:25 Color:UIColorFromRGB(0x333333)];

    
    countDownTimerLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
    countDownTimerLabel.layer.borderWidth = 1.0f;
    countDownTimerLabel.tag = 1000;

}

-(void)updateValues {
    
    [self checkPreviousStatusOfRunningTimer];
     NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    

    proNameLabel.text = [ud objectForKey:@"drivername"];
    proContactNoLabel.titleLabel.text = [ud objectForKey:@"DriverTelNo"];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,[ud objectForKey:@"driverpic"]];
   
    [proImgView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                  placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                         }];
    proImgView.layer.cornerRadius = proImgView.frame.size.width/2;
    proImgView.clipsToBounds = YES;
    proImgView.layer.borderColor = [UIColor colorWithRed:0.118 green:0.522 blue:0.812 alpha:1.000].CGColor;
    proImgView.layer.borderWidth = 2.0f;

    NSDateFormatter *df = [self serverformatter];
    NSDate *appDateTime = [df dateFromString:flStrForObj([ud objectForKey:KUBookingDate])];
    df = [self dateformatter];
    NSString *times = [df stringFromDate:appDateTime];

    df = [self timeformatter];
   NSString *mes = [df stringFromDate:appDateTime];
    
    NSString *at = @"   At ";
    at= [at stringByAppendingString:flStrForObj(mes)];

    times = [times stringByAppendingString:at];
    
    
}

-(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
    
}



- (NSDateFormatter *)serverformatter {
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}

- (NSDateFormatter *)dateformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM yyyy";
    });
    return formatter;
}

- (NSDateFormatter *)timeformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"hh:mm:ss a";
    });
    return formatter;
}


-(void)checkPreviousStatusOfRunningTimer {
    
    NSString *d = [[NSUserDefaults standardUserDefaults] objectForKey:@"started"];
    
    
    NSString *e = [Helper getCurrentDateTime];
    
    if (d.length != 0 && e.length != 0) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];

        
        NSDate *aa = [dateFormatter1 dateFromString:d];
        NSDate *bb = [dateFormatter dateFromString:e];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit| NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                                   fromDate:aa
                                                     toDate:bb
                                                    options:0];
        
        hourInt = (long)components.hour;
        minuteInt = (long)components.minute;
        secondInt = (long)components.second;
        result1 = [NSString stringWithFormat:@"%02lluH %02lluM %02lluS ",hourInt,minuteInt,secondInt];
    }
}

- (void)populateLabelwithTime
{
    if (secondInt == 59) {                   // Add on one second
        secondInt = 0;
        if (minuteInt == 59) {
            minuteInt = 0;
            if (hourInt == 23) {
                hourInt = 0;
            } else {
                hourInt += 1;
            }
        } else {
            minuteInt += 1;
        }
    } else {
        secondInt += 1;
    }

    result1 = [NSString stringWithFormat:@"%02llu:%02llu:%02llu",hourInt,minuteInt,secondInt];
    
    countDownTimerLabel.text = result1;
   
    

}

-(void)makeValueDefaultAfterBookingCompletion {
    
    secondInt = minuteInt = hourInt = 0;
    
}

- (IBAction)contactButtonClicked:(id)sender {
    
    NSString *number = [[NSUserDefaults standardUserDefaults]objectForKey:@"DriverTelNo"];
    NSString *cleanedString = [[number componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:telURL])
    {
        [[UIApplication sharedApplication] openURL:telURL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This function is only available in iPhone."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}
@end
