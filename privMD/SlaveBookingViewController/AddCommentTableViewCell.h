//
//  AddCommentTableViewCell.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 25/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@end
