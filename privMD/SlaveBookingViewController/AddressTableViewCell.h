//
//  AddressTableViewCell.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 25/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface AddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *changeAddressButton;
@property (weak, nonatomic) IBOutlet UILabel *showAddressLabel;
//@property (weak, nonatomic) IBOutlet GMSMapView *addressMapView;


@end
