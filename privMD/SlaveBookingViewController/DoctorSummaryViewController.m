
//
//  DoctorSummaryViewController.m
//  MBCalendarKit
//
//  Created by Rahul Sharma on 21/02/14.
//  Copyright (c) 2014 Moshe Berman. All rights reserved.
//

#import "DoctorSummaryViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ViewController.h"
#import "PaymentCell.h"
#import "PaymentViewController.h"
#import "AppointmentLocation.h"
#import "RoundedImageView.h"
#import "NetworkHandler.h"
#import "Flurry.h"
#import "SignInViewController.h"
#import "Database.h"
#import "Entity.h"
#import "PatientViewController.h"
#import "AddCommentTableViewCell.h"
#import "AddressTableViewCell.h"
#import "CardTableViewCell.h"
#import "ShowPriceTableViewCell.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "AddressManageViewController.h"
#import "PromoCodeTableViewCell.h"

#define HeaderHeight 270.0f


@interface DoctorSummaryViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AddressManageDelegate>
{
    NSInteger lastSelectedCardIndex;
    NSMutableArray * topImagesArray;
    NSInteger promoCodeValue;
    NSInteger disTypeValue;
}
@property (strong,nonatomic)  UITextView *activeTextView;
@property (nonatomic,strong) CardTableViewCell *cardCell;
@property (nonatomic,strong) AddressTableViewCell *addressCell;

- (IBAction)dismissKeyboard:(id)sender;

@property (strong, nonatomic)  UILabel *chooseCardLabel;
@property (strong, nonatomic) IBOutlet UITableView *tbleView;
@property (strong, nonatomic) NSString *messageToDoctor;
@property (strong, nonatomic) NSString *appointmentTime;
@property (strong, nonatomic) NSString *cardId;
@property (strong, nonatomic) NSMutableArray *allSectionData;

@property NSInteger count;
@property CGFloat lastContentOffset;

@property (strong, nonatomic) NSArray *topImages;
@property UIImageView *topImage;
@property (assign, nonatomic) NSInteger pageCount;

@end

@implementation DoctorSummaryViewController
@synthesize confirmButton;
@synthesize docEmailString;
@synthesize chooseCardLabel;
@synthesize appointmentType;
@synthesize cardId;

#pragma mark- WebServiceRequest

-(void)SendRequestForBookAppointment
{
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance]showPIOnWindow:keyWindow withMessge:NSLocalizedString(@"Booking...", @"Booking...")];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    AppointmentLocation *ap = [AppointmentLocation sharedInstance];
    NSString *address1 = @"";
    NSString *address2 = @"";
    NSString *zipcode = @"";
    NSString *notes = _messageToDoctor;
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    _appointmentTime = [Helper getCurrentDateTime];
    
    if(notes.length == 0)
    {
        notes = @"";
    }
    if (zipcode.length == 0) {
        zipcode = @"";
    }
    if (address2.length == 0) {
        address2 = @"";
    }
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_doc_email": _doctDetails[@"email"],
                             @"ent_doc_type": @"1",
                             @"ent_addr_line1": address1,
                             @"ent_addr_line2": address2,
                             @"ent_zipcode":zipcode,
                             @"ent_lat":@"13",
                             @"ent_long":@"77",
                             @"ent_extra_notes":notes,
                             @"ent_date_time":_appointmentTime,
                             @"ent_slot_id":@"slotid",
                             @"ent_card_id":cardId,
                             @"ent_coupon":flStrForObj(self.activeTextField.text),
                             };
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"livebooking"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           [self getBookingAppointment:response];
                                       }
                                   }];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}

-(void)sendAppointmentRequestForNow{
    
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    [[ProgressIndicator sharedInstance]showPIOnWindow:keyWindow withMessge:NSLocalizedString(@"Booking...", @"Booking...")];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    //appointment location details
    AppointmentLocation *ap = [AppointmentLocation sharedInstance];
    NSString *address1 = @"";
    NSString *address2 = @"";
    NSString *zipcode = @"";
    NSString *notes = _messageToDoctor;
    
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    _appointmentTime = [Helper getCurrentDateTime];
    
    if(notes.length == 0)
    {
        notes = @"";
    }
    if (zipcode.length == 0) {
        zipcode = @"";
    }
    
    if (address2 == (id)[NSNull null] || address2.length == 0 ){
        address2 = @"";
    }
    
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_doc_email": _doctDetails[@"email"],
                             @"ent_doc_type": @"1",
                             @"ent_addr_line1": address1,
                             @"ent_addr_line2": address2,
                             @"ent_zipcode":zipcode,
                             @"ent_lat":@"13",
                             @"ent_long":@"77",
                             @"ent_extra_notes":notes,
                             @"ent_date_time":_appointmentTime,
                             @"ent_card_id":cardId,
                             @"ent_coupon":flStrForObj(self.activeTextField.text),
                             };
    
    //  [Flurry logEvent:@"bookingRequest" withParameters:params];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    
    if ( [reachability isNetworkAvailable]) {
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMLiveBooking
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           [self getBookingAppointment:response];
                                       }
                                   }];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}



-(void)getBookingAppointment:(NSDictionary *)responseDictionary
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[responseDictionary objectForKey:@"Error"]];
    }
    else
    {
        
        if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 6 || [responseDictionary[@"errNum"] intValue] == 7 || [responseDictionary[@"errNum"] intValue] == 83 || [responseDictionary[@"errNum"] intValue] == 76)) { //session Expired
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
            
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:@"Message" Message:[responseDictionary objectForKey:@"errMsg"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:[Helper getCurrentDateTime] forKey:@"isAcceptedOnce"];
            [[NSUserDefaults standardUserDefaults] setObject:[Helper getCurrentDateTime] forKey:@"isRejectedOnce"];
            [[NSUserDefaults standardUserDefaults] setObject:[Helper getCurrentDateTime] forKey:@"isCanceledOnce"];
            
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[responseDictionary objectForKey:@"errMsg"]];
        }
    }
}

#pragma mark - Verify Coupen Code -

-(void)checkCoupenCode :(NSString *)promoCode
{
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    AppointmentLocation *ap = [AppointmentLocation sharedInstance];
    double cLat = [ap.pickupLatitude floatValue]; //[[ud objectForKey:KNUCurrentLat] doubleValue];
    double cLong = [ap.pickupLongitude floatValue];
    
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceId,
                             @"ent_coupon":promoCode,
                             @"ent_lat":[NSString stringWithFormat:@"%f", cLat],
                             @"ent_long":[NSString stringWithFormat:@"%f", cLong],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkcoupon"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self checkCoupenCodeResponse:response];
                                   }
                                   
                               }];
    // Activating progress Indicator.
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Promo Code", @"Checking Promo Code")];
    
}

-(void)checkCoupenCodeResponse:(NSDictionary *)responceForCheckCoupen
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if(!responceForCheckCoupen)
    {
        return;
    }
    if ([[responceForCheckCoupen objectForKey:@"errFlag"]integerValue] == 0)
    {
        if ([responceForCheckCoupen[@"distype"] integerValue] == 1) {
            
            disTypeValue = [responceForCheckCoupen[@"distype"] integerValue];
            promoCodeValue = [responceForCheckCoupen[@"discount"] integerValue];
            
        }
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responceForCheckCoupen[@"errMsg"]];
        
        
    }
    else if ([[responceForCheckCoupen objectForKey:@"errFlag"]integerValue] == 1)
    {
        self.activeTextField.text = @"";
        if ([responceForCheckCoupen[@"errFlag"] intValue] == 1 && ([responceForCheckCoupen[@"errNum"] intValue] == 6 || [responceForCheckCoupen[@"errNum"] intValue] == 7 || [responceForCheckCoupen[@"errNum"] intValue] == 83 || [responceForCheckCoupen[@"errNum"] intValue] == 76)) { //session Expired
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
            
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responceForCheckCoupen[@"errMsg"]];
            
        }
        
        
    }
}


- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lastSelectedCardIndex = 0;
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    if (context != nil)
    {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
        if(arrDBResult.count != 0) {
            
            Entity *fav = arrDBResult[lastSelectedCardIndex];
            cardId = fav.idCard;
        }
        else {
            cardId = @"";
        }
    }
    
    
    _topImages = [_doctDetails objectForKey:@"images"];
    

    _allSectionData = [[NSMutableArray alloc]initWithObjects:@"JOB LOCATION",@"CHOOSE PAYMENT METHOD",@"JOB DESCRIPTION",@"PRICE",@"PROMO CODE", nil];

    
    [self createTableHeaderView];
    

    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    
    if (appointmentType == kAppointmentTypeNow) {
        
        _tbleView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _tbleView.bounds.size.width, 64.01f)];
        PatientAppDelegate *app = [[UIApplication sharedApplication]delegate];
        [app setupAppearance];
        
    }
    else {
        
    }

    
}
-(void)viewWillDisappear:(BOOL)animated {
   
    if (appointmentType == kAppointmentTypeLater) {
        
        PatientAppDelegate *app = [[UIApplication sharedApplication]delegate];
        [app setupAppearance];
        
    }


}

#pragma mark TextView Delegate methods


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    self.activeTextView = textView;
    if ([textView.text isEqualToString:@"Please add comment here.."]) {
        textView.text = @"";
    }
    
    if (textView.textColor == [UIColor whiteColor]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.textColor = [UIColor whiteColor];
        textView.text = @"Please add comment here..";
        [textView resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    
    if (![textView.text isEqualToString:@"Please add comment here.."]) {
        
    }
    if(textView.text.length != 0)
    {
        _messageToDoctor = textView.text;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            textView.textColor = [UIColor whiteColor];
            textView.text = @"Please add comment here..";
        }
        
        return NO;
    }
    
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)selectCardButtonClicked:(id)sender
{
    PaymentViewController *vc = (PaymentViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
    vc.callback = ^(NSString *cardNo ,NSString *cardIdentity, NSString *type,NSInteger selectedcardIndex){
        lastSelectedCardIndex = selectedcardIndex;
        cardId = cardIdentity;
        
        [self.tbleView reloadData];
        
    };
    vc.isComingFromSummary = YES;
    UINavigationController *navigationControlelr = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navigationControlelr animated:YES completion:nil];
    
}

- (void)gotoAddressSelectController:(id)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressManageViewController *addressVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"AddressManageViewController"];
    
    addressVC.isComingFromVC = 1;
    addressVC.addressManageDelegate = self;
    [self.navigationController pushViewController:addressVC animated:YES];
}

-(NSString*)getDate{
    
    if (appointmentType == kAppointmentTypeNow) {
        
        //       NSString *finalString = [Helper getLocalDate:[Helper getCurrentDateTime]];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *finalString = [dateFormatter stringFromDate:date];
        
        return finalString;
        
    }
    else {
        
        AppointmentLocation *ap = [AppointmentLocation sharedInstance];
        NSString *finalString = @"";
        return finalString;
        
    }
    
    return @"";
}
-(NSString*)getTime {
    
    if (appointmentType == kAppointmentTypeNow) {
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString *finalString = [dateFormatter stringFromDate:date];
        
        //        NSString *finalString = [Helper getLocalTime:[Helper getCurrentDateTime]];
        return finalString;
        
    }
    else {
        
        AppointmentLocation *ap = [AppointmentLocation sharedInstance];
        NSString *finalString = @"";
        return finalString;
        
    }
    
    return @"";
}

#pragma mark - TableView Delegate Methods

-(void)createTableHeaderView {
    
    _tbleView.tag = 100;
    
    NSInteger ratingValue = [[_doctDetails objectForKey:@"rating"] floatValue];
    _doctorDetailsRatingImage.value = ratingValue;
    _doctorDetailsRatingImage.baseColor = WHITE_COLOR;
    _doctorDetailsRatingImage.highlightColor = [Helper getColorFromHexString:@"#ffae12"];
    
    _doctorDetailsName.text = [NSString stringWithFormat:@"%@ %@",[_doctDetails objectForKey:@"fName"],[_doctDetails objectForKey:@"lName"]];
    
    _titleLabel.text = _doctorDetailsName.text;
    
    _doctorEducationLable.text = flStrForObj([_doctDetails objectForKey:@"deg"]);
    
    //ProfilePic
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage ,[_doctDetails objectForKey:@"pPic"]];
    _doctorDetailsProfileImage.layer.cornerRadius = _doctorDetailsProfileImage.frame.size.width/2;
    [_doctorDetailsProfileImage setClipsToBounds:YES];
    [_doctorDetailsProfileImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                  placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                             
                                             
                                         }];
    _recommondationLable.text = [NSString stringWithFormat:@"%@ Review(s)",_doctDetails[@"totalRev"]];
    
    
    CGRect frame = self.topBackgroundImage.frame;
    
    if (_topImages.count == 0) {
        
    }
    else {
        
        
        
        for(_count = 0;_count < _topImages.count;_count++)
        {
            
            frame.origin.x = _topScrollView.frame.size.width*_count;
            _topImage = [[UIImageView alloc] initWithFrame:frame];
            [_topImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[_topImages objectAtIndex:_count]]]];
            
            strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_topImages[_count]];
            
            [_topImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                         placeholderImage:[UIImage imageNamed:@"doc_info_default_image_frame"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                    
                                    
                                }];
            if (_count == 0) {
                
                [self.topBackgroundImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                           placeholderImage:[UIImage imageNamed:@"doc_info_default_image_frame"]
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                      
                                                      
                                                  }];
            }
            
            _topImage.tag = _count;
            
            [topImagesArray addObject:_topImage];
            [_topScrollView addSubview:_topImage];
            [_topScrollView sendSubviewToBack:_topImage];
            
            
        }
    }
    
    //pageview
    _pageIndicator.numberOfPages = _topImages.count;
    _pageIndicator.currentPage = 0;
    [_pageIndicator addTarget:self action:@selector(pagecontrolClicked:) forControlEvents:UIControlEventValueChanged];
    
    
    if (_topImages.count == 0) {
        
        frame.origin.x = _topScrollView.frame.size.width*0;
        [_topBackgroundImage setBackgroundColor:[Helper getColorFromHexString:@"#508FB2"]];
        [topImagesArray addObject:_topBackgroundImage];
        _pageIndicator.numberOfPages = 1;
        
    }
    
    _topScrollView.contentSize = CGSizeMake(_topScrollView.frame.size.width *_topImages.count, _topScrollView.frame.size.height);
    
    _topScrollView.pagingEnabled = YES;
    _topScrollView.delegate = self;
    
}

-(void)pagecontrolClicked:(id)sender{
    
    NSInteger page = _pageIndicator.currentPage;
    
    CGRect frame = _topScrollView.frame;
    
    
    frame.origin.x= 0;
    frame.origin.y = page;
    
    [_topScrollView scrollRectToVisible:frame animated:YES];
    
}



#pragma mark -
#pragma mark UITableView DataSource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _allSectionData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    float width = tableView.bounds.size.width;
    int fontSize = 40;
    int padding = 10;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, fontSize)];
    view.backgroundColor = [Helper getColorFromHexString:@"#F5F5F5"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 2, width - padding, fontSize)];
    label.text = _allSectionData[section];
    [Helper setToLabel:label Text:_allSectionData[section] WithFont: Roboto_Bold   FSize:16 Color:[Helper getColorFromHexString:@"#508FB2"]];
    [view addSubview:label];
    
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return 70;
    }
    else if (indexPath.section == 1) {
        return 70;
    }
    else if (indexPath.section == 2) {
        return 90;
    }
    else if (indexPath.section ==3) {
        return 90;
    }
    else{
        return 60;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *cellIdentifier = @"addressCell";
        if(!_addressCell){
            _addressCell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            _addressCell.selectionStyle=UITableViewCellAccessoryNone;
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            AppointmentLocation *ap = [AppointmentLocation sharedInstance];
            if(@"address" > 0) {
                _addressCell.showAddressLabel.text = [NSString stringWithFormat:@"%@ %@",ap.srcAddressLine1,ap.srcAddressLine2];
            }
            else{
                _addressCell.showAddressLabel.text = @"Select an Address";
            }
        }
        return _addressCell;
        
    }
    else if (indexPath.section == 1) {
        
        
        static NSString *cellIdentifier = @"cardCell";
        
        if(!_cardCell){
            _cardCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            _cardCell.selectionStyle=UITableViewCellAccessoryNone;
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            _cardCell.cardTextButton.titleLabel.textAlignment = NSTextAlignmentLeft;
            appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
            context = [appDelegate managedObjectContext];
            if (context != nil)
            {
                arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
                if(arrDBResult.count != 0){
                    Entity *fav = arrDBResult[0];
                    
                    cardId = fav.idCard;
                    
                    [Helper setButton:_cardCell.cardTextButton Text:[NSString stringWithFormat:@"PERSONAL ****%@",fav.last4] WithFont:OpenSans_Regular FSize:15 TitleColor:[Helper getColorFromHexString:@"#333333"] ShadowColor:nil];
                    
                }
                
            }
        }
        return _cardCell;
        
    }
    else if (indexPath.section == 2) {
        static NSString *cellIdentifier = @"addcommentCell";
        AddCommentTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellAccessoryNone;
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        return cell;
        
    }
    
    else if(indexPath.section == 3) {
        
        static NSString *cellIdentifier = @"showpriceCell";
        ShowPriceTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        cell.selectionStyle=UITableViewCellAccessoryNone;
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        cell.minimumPriceLabel.text = [NSString stringWithFormat:@"Min %@",[PatientGetLocalCurrency getCurrencyLocal:120.00]];
        
        cell.pricePerMinuteLabel.text = [NSString stringWithFormat:@"%@/Hr",[PatientGetLocalCurrency getCurrencyLocal:12]];
        
        return cell;
        
    }
    else {
        
        static NSString *cellIdentifier = @"promoCodeCell";
        PromoCodeTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellAccessoryNone;
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        return cell;
        
    }
    
    
}

#pragma mark - PAYMENT
-(NSString *)setPlaceholderToCardType:(NSString *)mycardType
{
    NSString* cardTypeName   = @"placeholder";
    if([mycardType isEqualToString:@"amex"])
        cardTypeName = @"amex";
    else if([mycardType isEqualToString:@"diners"])
        cardTypeName = @"diners";
    else if([mycardType isEqualToString:@"discover"])
        cardTypeName = @"discover";
    else if([mycardType isEqualToString:@"jcb"])
        cardTypeName = @"jcb";
    else if([mycardType isEqualToString:@"MasterCard"])
        cardTypeName = @"mastercard";
    else if([mycardType isEqualToString:@"Visa"])
        cardTypeName = @"visa.png";
    
    return cardTypeName;
}

-(void)promoCodeButtonClicked:(id)sender {
    
    if([self.activeTextField.text length])
    {
        [self checkCoupenCode:self.activeTextField.text];
        [self dismissKeyboard:nil];
    }
    
}

#pragma mark - UITextField Delegates -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    return YES;
}

#pragma Mark - UIKeyBoardNotification

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    
    UIEdgeInsets ei = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.tbleView.scrollIndicatorInsets = ei;
        self.tbleView.contentInset = ei;
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.tbleView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.tbleView.scrollIndicatorInsets = ei;
    self.tbleView.contentInset = ei;
}

#pragma Mark - UIButton Action

- (IBAction)dismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
    
}

- (IBAction)navBackButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)confirmButtonClicked:(id)sender
{
    if(cardId.length == 0) {
        
        [Helper showAlertWithTitle:@"Message" Message:@"You cannot confirm booking without selecting card."];
    }
    else
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ( [reachability isNetworkAvailable]) {
            [self.view endEditing:YES];
           
            if (appointmentType == kAppointmentTypeNow) {
                
                [self sendAppointmentRequestForNow];
            
            }
            else if (appointmentType == kAppointmentTypeLater) {
                
                [self SendRequestForBookAppointment];
            }
            
        }
        else {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
        }
        
    }
    
}


#pragma mark - Up and Down Scrolling Animation

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageHeight = self.topScrollView.frame.size.width;
    float fractionalPage = self.topScrollView.contentOffset.x / pageHeight;
    NSInteger page = lround(fractionalPage);
    self.pageIndicator.currentPage = page;
    
    UIImageView *locImg;
    if(scrollView.tag == 100)
    {
        if(page <1){
            page = 0;
            
        }
        locImg = topImagesArray[page];
        if(scrollView.contentOffset.y == 0){
            _titleView.alpha = 0;
            _titleLabel.alpha = 0;
        }
        
        
        if (self.lastContentOffset > scrollView.contentOffset.y)//downwards
        {
            CGFloat movedOffset = scrollView.contentOffset.y;
            CGFloat headerOffset = HeaderHeight - movedOffset - 64;
            
            if (movedOffset > 159 && movedOffset <= 100) {
                
                _titleView.backgroundColor = [Helper getColorFromHexString:@"#508FB2"];
                _titleView.alpha = (movedOffset-40) / 64;
                _titleLabel.alpha = (movedOffset-40) / 64;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  headerOffset+6;
                _titleLabel.frame = frame;
                
                
            }
            
            else if(movedOffset <= 36) {
                
                _titleView.alpha = 0;
                _titleLabel.alpha = 0;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  99;
                _titleLabel.frame = frame;
                
            }
            
            _count = 100;
            
            CGFloat yPos = -scrollView.contentOffset.y;
            if (yPos > 0) {
                
                
                CGRect imgRect = self.topScrollView.frame;
                imgRect.origin.y = scrollView.contentOffset.y;
                imgRect.size.height = HeaderHeight+yPos;
                self.topScrollView.frame = imgRect;
                
                CGRect frame = locImg.frame;
                frame.size.height = HeaderHeight+yPos;
                locImg.frame = frame;
                
                
            }
            
            
            
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y)//upwards
        {
            CGFloat movedOffset = scrollView.contentOffset.y;
            CGFloat headerOffset = HeaderHeight - movedOffset - 64;
            _titleLabel.alpha = 1;
            
            if(scrollView.contentOffset.y <= 0){
                
                locImg.frame = CGRectMake(320*page,0, 320, HeaderHeight-scrollView.contentOffset.y);
                self.topScrollView.frame = CGRectMake(0, scrollView.contentOffset.y,self.topScrollView.frame.size.width, HeaderHeight-scrollView.contentOffset.y);
                
            }
            if (headerOffset > 0 && headerOffset <= 64) {
                
                _titleView.backgroundColor = [Helper getColorFromHexString:@"#508FB2"];
                _titleView.alpha = 1 - headerOffset / 64;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  headerOffset+6;
                _titleLabel.frame = frame;
                [self.view bringSubviewToFront:_titleView];
                [self.view bringSubviewToFront:_titleLabel];
                
                
            }
            else if(headerOffset < 0){
                
                _titleView.alpha = 1;
                _titleLabel.alpha = 1;
                CGRect frame = _titleLabel.frame;
                frame.origin.y =  6;
                _titleLabel.frame = frame;
                [self.view bringSubviewToFront:_titleView];
                [self.view bringSubviewToFront:_titleLabel];
                
            }
            
        }
        
        self.lastContentOffset = scrollView.contentOffset.y;
        
    }
    
}


- (IBAction)changeCardButtonClicked:(id)sender {
    
    PaymentViewController *vc = (PaymentViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
    vc.callback = ^(NSString *cardNo ,NSString *cardIdentity, NSString *type,NSInteger selectedcardIndex){
        lastSelectedCardIndex = selectedcardIndex;
        cardId = cardIdentity;
        
        [self.tbleView reloadData];
        
    };
    vc.isComingFromSummary = YES;
    UINavigationController *navigationControlelr = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navigationControlelr animated:YES completion:nil];

    
}

- (IBAction)applyPromoCodeButtonClicked:(id)sender {
    
    if([self.activeTextField.text length])
    {
        [self checkCoupenCode:self.activeTextField.text];
        [self dismissKeyboard:nil];
    }
}

- (IBAction)changeAddressButton:(id)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressManageViewController *addressVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"AddressManageViewController"];
    
    addressVC.isComingFromVC = 1;
    addressVC.addressManageDelegate = self;
    [self.navigationController pushViewController:addressVC animated:YES];

}
-(void)getAddressFromDatabase:(NSDictionary *)addressDetails {
    
    AppointmentLocation *ap = [AppointmentLocation sharedInstance];
    ap.srcAddressLine1 = addressDetails[@"address1"];
    ap.pickupLatitude = [addressDetails objectForKey:@"latitude"];
    ap.pickupLongitude = [addressDetails objectForKey:@"longitude"];
    
    [self.tbleView reloadData];
}
@end
