//
//  DoctorSummaryViewController.h
//  MBCalendarKit
//
//  Created by Rahul Sharma on 21/02/14.
//  Copyright (c) 2014 Moshe Berman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView.h>



@interface DoctorSummaryViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    CLLocationCoordinate2D getLatLong;
    UIActionSheet          *actionSheet;
    UIPickerView           *pickerView;
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
	NSManagedObjectContext *context;
}

@property (weak, nonatomic) IBOutlet UIImageView *topBackgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (strong, nonatomic) IBOutlet AXRatingView *doctorDetailsRatingImage;

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) IBOutlet UIImageView *doctorDetailsProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *recommondationLable;
@property (strong, nonatomic) IBOutlet UILabel *doctorDetailsName;
@property (strong, nonatomic) IBOutlet UILabel *doctorEducationLable;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIScrollView *topScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageIndicator;
@property (weak, nonatomic) IBOutlet UIView *bookButtonView;

-(IBAction)confirmButtonClicked:(id)sender;
- (IBAction)navBackButtonAction:(id)sender;

@property(strong,nonatomic) NSString *docEmailString;
@property(strong,nonatomic) NSString *stringDate;
@property(strong,nonatomic) NSString *stringTime;
@property (strong, nonatomic) NSDictionary *doctDetails;
@property (strong, nonatomic) NSString *strLocation;
@property (assign, nonatomic) AppointmentType appointmentType;
@property (strong, nonatomic) NSString *slotID;
@property (strong,nonatomic) UITextField *activeTextField;

// Cell buttons action

- (IBAction)changeCardButtonClicked:(id)sender;
- (IBAction)applyPromoCodeButtonClicked:(id)sender;
- (IBAction)changeAddressButton:(id)sender;



@end
