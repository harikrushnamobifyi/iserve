//
//  PromoCodeTableViewCell.h
//  iserve
//
//  Created by Rahul Sharma on 01/09/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoCodeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;

@end
