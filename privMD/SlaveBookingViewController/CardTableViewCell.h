//
//  CardTableViewCell.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 25/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addCardButton;
@property (weak, nonatomic) IBOutlet UIButton *cardTextButton;

@end
