//
//  NetworkHandler.m
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "NetworkHandler.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "NetworkStatusShowingView.h"

@interface NetworkHandler ()
@property(nonatomic,strong)AFHTTPRequestOperationManager *manager;
@property(nonatomic,strong) NSString *lastMethod;
@property int count;
@end

@implementation NetworkHandler
static NetworkHandler *networkHandler;

+ (id)sharedInstance {
    if (!networkHandler) {
        networkHandler  = [[self alloc] init];
    }
    
    return networkHandler;
}
-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock {
    
    _manager = [AFHTTPRequestOperationManager manager];
    //NSOperationQueue *operationQueue = _manager.operationQueue;

    __block  NSString *postUrl = [self getBaseString:method];
    __weak AFHTTPRequestOperationManager *nm = _manager;
    
        [nm POST:postUrl parameters:paramas success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //send success response with data
            completionBlock(YES,responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Network Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            
            completionBlock(NO,nil);
            
        }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        
    }
}

-(void)cancelRequestOperation{
    
    for (NSOperation *operation in _manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        [operation cancel];
    }
}

-(NSString*)getBaseString:(NSString*)method
{
    return [NSString stringWithFormat:@"%@%@", BASE_URL, method];
}

-(NSString*)paramDictionaryToString:(NSDictionary*)params
{
    NSMutableString *request = [[NSMutableString alloc] init];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [request appendFormat:@"&%@=%@", key, obj];
    }];
    
    NSString *finalRequest = request;
    if ([request hasPrefix:@"&"]) {
        finalRequest = [request substringFromIndex:1];
    }
    
    return finalRequest;
}


@end
