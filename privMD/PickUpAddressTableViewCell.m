//
//  PickUpAddressTableViewCell.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 11/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "PickUpAddressTableViewCell.h"

@implementation PickUpAddressTableViewCell

@synthesize addressLine1;
@synthesize addressLine2;
@synthesize delegate;


- (void)awakeFromNib
{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}
- (IBAction)deleteButton:(id)sender
{
    if (self.delegate != nil && [ self.delegate respondsToSelector: @selector( deleteButtonDelegate:)])
    {
        [self.delegate deleteButtonDelegate:(int)_deleteButton.tag];
    }
}

- (IBAction)addButton:(id)sender
{
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(addButtonDelegate:)])
    {
        [self.delegate addButtonDelegate:(int)_addButton.tag];
    }
}
@end
