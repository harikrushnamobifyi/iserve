//
//  LocationServicesMessageVC.m
//  privMD
//
//  Created by Surender Rathore on 22/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "LocationServicesMessageVC.h"

@interface LocationServicesMessageVC ()

@end

@implementation LocationServicesMessageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"LOCATION";
   
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


@end
