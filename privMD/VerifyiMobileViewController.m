//
//  MODAVerifyiMobileViewController.m
//  MODA
//
//  Created by Rahul Sharma on 2/29/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "VerifyiMobileViewController.h"
#import "CardLoginViewController.h"

@interface VerifyiMobileViewController() <UploadFileDelegate>
{
    NSString *verificationCode;
}
@end

@implementation VerifyiMobileViewController
@synthesize saveSignUpDetails;

-(void)viewDidLoad {

     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    [self createNavLeftButton];
    [self createNavRightButton];
    self.navigationItem.title = @"VERIFY MOBILE";
    [self sendServiceToGetVerificationCode];

}

- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)createNavRightButton
{
    UIButton *navNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navNextButton addTarget:self action:@selector(doneButtonClicked) forControlEvents:UIControlEventTouchUpInside];    UIImage *buttonImageOn = [UIImage imageNamed:@"next_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"next_btn_off"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navNextButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navNextButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

- (void)doneButtonClicked
{
    if (_firstNumber.text.length != 0 && _secondNumber.text.length != 0 && _thirdNumber.text.length != 0 && _fourthNumber.text.length != 0 && _fifthNumber.text.length != 0 ) {
        
        NSString *code = [NSString stringWithFormat:@"%@%@%@%@%@",_fifthNumber.text,_fourthNumber.text,_thirdNumber.text,_secondNumber.text,_firstNumber.text];
        if ([code isEqualToString:verificationCode]) {
            
            [self sendServiceToSignedUpUser];
        }
        else {
            _firstNumber.text = _secondNumber.text = _thirdNumber.text = _fourthNumber.text = _fifthNumber.text = @"";
            [_fifthNumber becomeFirstResponder];
            [Helper showAlertWithTitle:@"Message" Message:@"Seems like you have entered wrong verification code"];
        }
        
    }
    else {
        
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter valid codes."];
    }

}

#pragma mark - UIButton Action

- (IBAction)dismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
}

-(IBAction)sendButtonClicked:(id)sender {
    
    [self sendServiceToGetVerificationCode];

}

#pragma mark - WebServiceCall

-(void)sendServiceToGetVerificationCode {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params = @{
                             @"ent_dev_id":deviceID,
                             @"ent_phone":saveSignUpDetails[3],
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       
                                       [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                       
                                       
                                       if (!response)
                                       {
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:NSLocalizedString(@"Message", @"Message")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                                           [alertView show];
                                           
                                       }
                                       else if ([response objectForKey:@"error"])
                                       {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"error"]];
                                       }
                                       else
                                       {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
                                           {
                                               verificationCode = response[@"code"];
                                              NSLog(@"response %@",verificationCode);
                                           }
                                           else
                                           {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                           }
                                       }
                                   }
                               }];

}

-(void)sendServiceToSignedUpUser {
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Signing Up..."];
    
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"";
    }
    
    NSString *country = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KDACountry])
        country = @"Country1";
    else
        country = [[NSUserDefaults standardUserDefaults] objectForKey:KDACountry];
    
    
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KDACity])
        city = @"City1";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KDACity];
    
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    
    NSDictionary *params = @{KDASignUpFirstName:[saveSignUpDetails objectAtIndex:0],
                             KDASignUpLastName:[saveSignUpDetails objectAtIndex:1],
                             KDASignUpEmail:[saveSignUpDetails objectAtIndex:2],
                             KDASignUpMobile:[saveSignUpDetails objectAtIndex:3],
                             KDASignUpZipCode:@"560024",
                             KDASignUpPassword:[saveSignUpDetails objectAtIndex:4],
                             KDASignUpDeviceId:deviceId,
                             KDASignUpAccessToken:@"",
                             KDASignUpCity:city,
                             KDASignUpCountry:country,
                             KDASignUpPushToken:pToken,
                             KDASignUpTandC:@"1",
                             KDASignUpPricing:@"1",
                             KDASignUpDeviceType:@"1",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             @"ent_longitude":lon,
                             @"ent_latitude":lat,
                             @"ent_referral_code":[saveSignUpDetails objectAtIndex:5]
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientSignUp
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response

                                       [self signupResponse:response];
                                   }
                               }];
    
    
}

-(void)signupResponse:(NSDictionary*)response
{
    //check if response is not null
    if(response == nil)
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    
    //check for network error
    if (response[@"Error"])
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:response[@"Error"]];
    }
    else
    {
        
        //handle response here
        NSDictionary *itemList = [[NSDictionary alloc]init];
        itemList = [response mutableCopy];
        
        if ([itemList[@"errFlag"] intValue] == 1) { //error
            
            ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
            [progressIndicator hideProgressIndicator];
            [Helper showAlertWithTitle:@"Error" Message:itemList[@"errMsg"]];
        }
        else {
            
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:[itemList objectForKey:@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:itemList[@"chn"] forKey:kNSUPatientPubNubChannelkey];
            [ud setObject:itemList[@"serverChn"] forKey:kPMDPublishStreamChannel];
            [ud setObject:itemList[@"email"] forKey:kNSUPatientEmailAddressKey];
            [ud setObject:itemList[@"apiKey"] forKey:kNSUMongoDataBaseAPIKey];
            [ud setObject:itemList[@"coupon"] forKey:kNSUPatientCouponkey];
            [ud setObject:[saveSignUpDetails objectAtIndex:0] forKey:KDAFirstName];
            [ud setObject:[saveSignUpDetails objectAtIndex:1] forKey:KDALastName];
            [ud setObject:[saveSignUpDetails objectAtIndex:2] forKey:KDAEmail];
            [ud setObject:[saveSignUpDetails objectAtIndex:3] forKey:KDAPhoneNo];
            [ud setObject:[saveSignUpDetails objectAtIndex:5] forKey:KDAPassword];
            
            [ud synchronize];

            if(_pickedImage)
            {
                UploadFile * upload = [[UploadFile alloc]init];
                upload.delegate = self;
                [upload uploadImageFile:_pickedImage];
            }
            else {
                
                [self gotoNextController:YES];
            }
            
        }
        
    }
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls {
    
    [self gotoNextController:YES];
}
-(void)uploadFile:(UploadFile *)uploadfile didFailedWithError:(NSError *)error {
    
    [self gotoNextController:YES];
}

-(void)gotoNextController:(BOOL)to {
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self performSegueWithIdentifier:@"CardController" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CardController"])
    {
        CardLoginViewController *CLVC = (CardLoginViewController*)[segue destinationViewController];
        CLVC.isComingFromPayment = 3;
        
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self findNextResponder:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL shouldProcess = NO; //default to reject
    BOOL shouldMoveToNextField = NO; //default to remaining on the current field
    
    NSUInteger insertStringLength = [string length];
    if(insertStringLength == 0){ //backspace
        shouldProcess = YES; //Process if the backspace character was pressed
    }
    else {
        if([[textField text] length] == 0) {
            shouldProcess = YES; //Process if there is only 1 character right now
        }
    }
    
    //here we deal with the UITextField on our own
    if(shouldProcess){
        //grab a mutable copy of what's currently in the UITextField
        NSMutableString* mstring = [[textField text] mutableCopy];
        if([mstring length] == 0){
            //nothing in the field yet so append the replacement string
            [mstring appendString:string];
            
            shouldMoveToNextField = YES;
        }
        else{
            //adding a char or deleting?
            if(insertStringLength > 0){
                [mstring insertString:string atIndex:range.location];
            }
            else {
                //delete case - the length of replacement string is zero for a delete
                [mstring deleteCharactersInRange:range];
            }
        }
        
        //set the text now
        [textField setText:mstring];
        
        
        if (shouldMoveToNextField) {
            //MOVE TO NEXT INPUT FIELD HERE
        
            [self findNextResponder:textField];
        }
    }
    
    //always return no since we are manually changing the text field
    return NO;
}

-(void)findNextResponder:(UITextField *)textField {
    
    if(textField == _fifthNumber)
    {
        [_fourthNumber becomeFirstResponder];
    }
    else if(textField == _fourthNumber)
    {
        [_thirdNumber becomeFirstResponder];
    }
    else if (textField == _thirdNumber)
    {
        [_secondNumber becomeFirstResponder];
    }
    else if (textField == _secondNumber)
    {
        [_firstNumber becomeFirstResponder];
    }
    else if (textField == _firstNumber)
    {
        [_firstNumber resignFirstResponder];
        [self doneButtonClicked];
       

        
    }

}
@end
