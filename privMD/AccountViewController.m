//
//  AccountViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "Database.h"

@interface AccountViewController () <CustomNavigationBarDelegate>

@property (assign ,nonatomic) BOOL isKeyboardIsShown;
@property (strong,nonatomic)  UITextField *activeTextField;

@end

@implementation AccountViewController
@synthesize mainScroll;
@synthesize logoutButton;
@synthesize accFirstNameTextField;
@synthesize accLastNameTextField;
@synthesize accEmailTextField;
@synthesize accPasswordTextField;
@synthesize accPhoneNoTextField;
@synthesize accProfilePic;
@synthesize accProfileButton;
@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - WebService call

-(void)sendServiceForUpdateProfile
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Saving.."];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *fName = accFirstNameTextField.text;
    NSString *lName = accLastNameTextField.text;
    NSString *eMail = accEmailTextField.text;
    NSString *phone = accPhoneNoTextField.text;
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_first_name":fName,
                               @"ent_last_name":lName,
                               @"ent_email":eMail,
                               @"ent_phone":phone,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"updateProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 
                                 [self updateProfileResponse:response];
                                 
                             }
                             else {
                                 
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];

}

-(void)updateProfileResponse:(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:accLastNameTextField.text forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:accPhoneNoTextField.text forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:@"proPicChange" object:nil userInfo:nil];
    }
    
}

-(void)sendServiceForegetProfileData
{
   // [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    CGRect fra = accProfilePic.frame;
    fra.origin.x = accProfilePic.frame.size.width/2-10;
    fra.origin.y = accProfilePic.frame.size.height/2-10;
    fra.size.width = 20;
    fra.size.height = 20;
    activityIndicator = [[UIActivityIndicatorView alloc]init];
    activityIndicator.frame = fra;
    [self.accProfilePic addSubview:activityIndicator];
    activityIndicator.backgroundColor=[UIColor clearColor];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];

    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getProfileResponse:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
    

    
}

-(void)getProfileResponse:(NSDictionary *)response
{
  //  ProgressIndicator *pi = [ProgressIndicator sharedInstance];
  //  [pi hideProgressIndicator];
    
    TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 81 || [response[@"errNum"] intValue] == 78 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
          
            
            NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,dictResponse[@"pPic"]];
            
            __weak typeof(self) weakSelf = self;
            
            if([[[NSUserDefaults standardUserDefaults] objectForKey:isFromFB] isEqualToString:@"YES"]){
                strImageUrl =[[NSUserDefaults standardUserDefaults] objectForKey:KDAFbProfilePicURL];
            }

            
            
//            [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
//                          placeholderImage:nil
//                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
//                                     [weakSelf.activityIndicator stopAnimating];
//
//                                 }];
            
            
            
            [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                             placeholderImage:[UIImage imageNamed:@"default_Profile_Pic"]
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                        [weakSelf.activityIndicator stopAnimating];
                                        
                                    }];
            
            
          
            accFirstNameTextField.text = dictResponse[@"fName"];
            accLastNameTextField.text = dictResponse[@"lName"];
            NSString *phone = [NSString stringWithFormat:@"+%@",dictResponse[@"phone"]];
            accPhoneNoTextField.text = phone;
            accEmailTextField.text = dictResponse[@"email"];
            [Helper setToLabel:_nameLabel Text:[NSString stringWithFormat:@"%@ %@",dictResponse[@"fName"],dictResponse[@"lName"]]   WithFont:OpenSans_Regular FSize:18 Color:UIColorFromRGB(0xffffff)];
            
            [Helper setToLabel:_emailLabel Text:[NSString stringWithFormat:@"%@",dictResponse[@"email"]]   WithFont:OpenSans_Regular FSize:12 Color:UIColorFromRGB(0xffffff)];
            
            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:accLastNameTextField.text forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:accPhoneNoTextField.text forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults]setObject:dictResponse[@"pPic"] forKey:KDAProfilePic];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"proPicChange" object:nil userInfo:nil];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   // self.view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7];
    self.navigationController.navigationBarHidden = YES;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
    accEmailTextField = [[UITextField alloc]init];
     accProfilePic.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patitentprofile_imagethumbnail"]];
    
    accFirstNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAFirstName];
    accFirstNameTextField.textColor = UIColorFromRGB(0x000000);
    accFirstNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    accLastNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDALastName];
    accLastNameTextField.textColor = UIColorFromRGB(0x000000);
    accLastNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];

    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    accEmailTextField.userInteractionEnabled = NO;
    accFirstNameTextField.userInteractionEnabled = NO;
    accLastNameTextField.userInteractionEnabled = NO;
    accPhoneNoTextField.userInteractionEnabled = NO;
    accPasswordTextField.userInteractionEnabled = NO;
    accProfileButton.userInteractionEnabled = NO;
    
    accEmailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAEmail];
    accEmailTextField.textColor = UIColorFromRGB(0x000000);
    accEmailTextField.font = [UIFont fontWithName:Roboto_Regular size:13];

    accPhoneNoTextField.text = [[NSUserDefaults standardUserDefaults]objectForKey:KDAPhoneNo];
    accPhoneNoTextField.textColor = UIColorFromRGB(0x000000);
    accPhoneNoTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    accPasswordTextField.text = [[NSUserDefaults standardUserDefaults]objectForKey:KDAPassword];
    accPasswordTextField.textColor = UIColorFromRGB(0x000000);
    accPasswordTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    accPasswordTextField.textColor = UIColorFromRGB(0x000000);
    accPasswordTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    accProfilePic.layer.cornerRadius = accProfilePic.frame.size.width/2;
    [accProfilePic setClipsToBounds:YES];
    [logoutButton setTitle:@"Sign Out" forState:UIControlStateNormal];
    [logoutButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [logoutButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    CGRect frameTopview = logoutButton.frame;
    frameTopview.origin.y = self.view.bounds.size.height - 65;
    
     _scrollView.contentSize = CGSizeMake(320,450);
    accPasswordTextField.hidden = YES;
    _passwordLineView.hidden = YES;
    [self addCustomNavigationBar];
    
    [self sendServiceForegetProfileData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;

}
-(void)viewDidAppear:(BOOL)animated{
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [accPhoneNoTextField resignFirstResponder];
    [accFirstNameTextField resignFirstResponder];
    [accLastNameTextField resignFirstResponder];
    [accEmailTextField resignFirstResponder];
    
    
}
#pragma mark ButtonAction Methods -

- (void)menuButtonPressedAccount
{
    [self.view endEditing:YES];
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}
- (void)saveUserDetails:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    
    mBut.userInteractionEnabled = YES;
    {
        if(mBut.isSelected)
        {
            mBut.selected =NO;
            [mBut setTitle:@"EDIT" forState:UIControlStateNormal];
            accEmailTextField.userInteractionEnabled = NO;
            accFirstNameTextField.userInteractionEnabled = NO;
            accLastNameTextField.userInteractionEnabled = NO;
            accPhoneNoTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = NO;
            
            [self sendServiceForUpdateProfile];
        }
        else
        {
            mBut.selected = YES;
            [mBut setTitle:@"SAVE" forState:UIControlStateSelected];
            accEmailTextField.userInteractionEnabled = NO;
            accFirstNameTextField.userInteractionEnabled = YES;
            accLastNameTextField.userInteractionEnabled = YES;
            accPhoneNoTextField.userInteractionEnabled = YES;
            accProfileButton.userInteractionEnabled = YES;
            
        }
    }
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"PROFILE"];
   // [customNavigationBarView createRightBarButton];
   
    [customNavigationBarView setRightBarButtonTitle:@"EDIT"];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    [self.view endEditing:YES];
    [self saveUserDetails:sender];
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

- (IBAction)profilePicButtonClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Edit Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)passwordButtonClicked:(id)sender
{
//    [Helper showAlertWithTitle:@"Message" Message:@"Please visit our website Roadyo.net to change your password."];

}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Uploading Profile Pic..."];
    //  _flagCheckSnap = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    TELogInfo(@"Image Info : %@",info);
    
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    accProfilePic.image = _pickedImage;
    
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
    
    UploadFile * upload = [[UploadFile alloc]init];
    upload.delegate = self;
    [upload uploadImageFile:_pickedImage];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
   
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
}
-(void)uploadFile:(UploadFile *)uploadfile didFailedWithError:(NSError *)error{
    TELogInfo(@"upload file  error %@",[error localizedDescription]);
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    {
        [Helper showAlertWithTitle:@"Oops!" Message:@"Your profile photo has not been updated try again."];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;   
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
       [Helper setToLabel:_nameLabel Text:[NSString stringWithFormat:@"%@ %@",accFirstNameTextField.text,accLastNameTextField.text] WithFont:OpenSans_Regular FSize:18 Color:[UIColor whiteColor]];
    

    textFieldEditedFlag = 1;
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        if(textField == accFirstNameTextField)
        {
            [accLastNameTextField becomeFirstResponder];
        }
        else if(textField == accLastNameTextField)
        {
            [accPhoneNoTextField becomeFirstResponder];
        }
    

        return YES;
}
/**
 *  KeyBoard Methods
 */

//-(void) keyboardWillShow:(NSNotification *)note
//{
//    if(_isKeyboardIsShown)
//    {
//        return;
//    }
//    // Get the keyboard size
//    CGRect keyboardBounds;
//    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
//    
//    CGSize keyboardSize = [[note.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    // Detect orientation
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
//    CGRect frame = self.scrollView.frame;
//    
//    // Start animation
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:0.3f];
//    
//    // Reduce size of the scroll view
//    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
//        frame.size.height -= keyboardBounds.size.height;
//    else
//        frame.size.height -= keyboardSize.width; //not used just for testing
//    
//    // Apply new size of scroll view
//    self.scrollView.frame = frame;
//    
//    // Scroll the scroll view to see the TextField just above the keyboard
//    if (self.activeTextField)
//    {
//        CGRect textFieldRect = [self.scrollView convertRect:self.activeTextField.bounds fromView:self.activeTextField];
//        [self.scrollView scrollRectToVisible:textFieldRect animated:NO];
//    }
//    [UIView commitAnimations];
//    _isKeyboardIsShown = YES;
//}
//
//
//-(void) keyboardWillHide:(NSNotification *)note
//{
//    // Get the keyboard size
//    CGRect keyboardBounds;
//    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
//    
//    // Detect orientation
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
//    CGRect frame = self.scrollView.frame;
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:0.3f];
//    
//    
//    // Reduce size of the scroll view
//    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
//        frame.size.height += keyboardBounds.size.height;
//    else
//        frame.size.height += keyboardBounds.size.width;
//    
//    // Apply new size of scroll view
//    
//    self.scrollView.frame = frame; //CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
//    
//    [UIView commitAnimations];
//    _isKeyboardIsShown = NO;
//    
//}

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}

@end
