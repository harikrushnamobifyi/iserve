//
//  SignInViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "ViewController.h"
#import "Database.h"
#import "FBLoginHandler.h"
#import "SignUpViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "XDKAirMenuController.h"
@interface SignInViewController ()<FBLoginHandlerDelegate>
@property (nonatomic,strong)  NSMutableArray *arrayContainingCardInfo;

@end

@implementation SignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize navDoneButton;
@synthesize emailImageView;
@synthesize signinButton;
@synthesize profileImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.    


    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"SIGN IN";
    
    [self createNavLeftButton];

    [Helper setButton:signinButton Text:@"LOGIN" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [signinButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [signinButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];

    [Helper setButton:_forgotPasswordButton Text:@"Forgot password?" WithFont:OpenSans_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];

    [emailTextField setValue:UIColorFromRGB(0x999999)
            forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x999999)
                forKeyPath:@"_placeholderLabel.textColor"];
    
    emailTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    emailTextField.textColor = UIColorFromRGB(0x333333);
    passwordTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    passwordTextField.textColor = UIColorFromRGB(0x333333);
    

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)createnavRightButton
{
    navDoneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navDoneButton addTarget:self action:@selector(DoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [navDoneButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];
    
    [Helper setButton:navDoneButton Text:@"Done" WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    
    
    [navDoneButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    // Create a container bar button
    UIBarButtonItem *containingnextButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    
    self.navigationItem.rightBarButtonItem = containingnextButton;
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // [emailTextField becomeFirstResponder];
    
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)signInUser
{
    NSString *email = emailTextField.text;
    
    NSString *password = passwordTextField.text;
    
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
        [emailTextField becomeFirstResponder];
    }
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password"];
        [passwordTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email ID"];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    else
    {
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}



-(void)DoneButtonClicked
{
    [self signInUser];
}

-(void)sendServiceForLogin
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"13.02323";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"77.5833";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"f95ce5b6117c24ebca5dce5d0de81de6c82417e6fd7ebc4fcd07bb64d2c57d16";
    }
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    NSDictionary *params = @{
                             KDALoginEmail:emailTextField.text,
                             KDALoginPassword:passwordTextField.text,
                             KDALoginDevideId:deviceId,
                             KDALoginPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
//                             KDASignUpCity:city,
                             KDALoginDeviceType:@"1",
                             KDALoginUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientLogin
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       TELogInfo(@"response %@",response);
                                       [self loginResponse:response];
                                   }
                               }];
    
    
    
    // Activating progress Indicator.
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing In...", @"Signing In...")];
}
/**
 *  <#Description#>
 *
 *  @param dictionary <#dictionary description#>
 */
-(void)loginResponse:(NSDictionary *)dictionary
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    itemList = [dictionary mutableCopy];
    
    if(!dictionary)
    {
        return;
    }
    else if ([[dictionary objectForKey:@"error"] length])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[dictionary objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        if ([[itemList objectForKey:@"errFlag"]integerValue] == 0)
        {
            //save information
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:[itemList objectForKey:@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:[itemList objectForKey:@"profilePic"] forKey:KDAProfilePic];
            [ud setObject:itemList[@"chn"] forKey:kNSUPatientPubNubChannelkey];
            [ud setObject:itemList[@"email"] forKey:kNSUPatientEmailAddressKey];
            [ud setObject:itemList[@"apiKey"] forKey:kNSUMongoDataBaseAPIKey];
            [ud setObject:itemList[@"serverChn"] forKey:kPMDPublishStreamChannel];
            [ud setObject:itemList[@"coupon"] forKey:kNSUPatientCouponkey];
            [ud setObject:itemList[@"fname"] forKey:KDAFirstName];
            [ud setObject:itemList[@"lname"] forKey:KDALastName];
            [ud setObject:@"NO" forKey:isFromFB];
            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            
            if (!carTypes || !carTypes.count){
                TELogInfo(@"do nothing");
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }

            
            _arrayContainingCardInfo = itemList[@"cards"];
            
            if (!_arrayContainingCardInfo || !_arrayContainingCardInfo.count){
                
            }
            else
            {
               
                [self addInDataBase];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            
            ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
        }
        else if ([[itemList objectForKey:@"errFlag"]integerValue] == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[itemList objectForKey:@"errMsg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            checkLoginCredentials = NO;
            passwordTextField.text = @"";
            [passwordTextField becomeFirstResponder];
        }
        
    }
    
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == emailTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else
    {
        [passwordTextField resignFirstResponder];
        [self DoneButtonClicked];
        
    }
    return YES;
    
}


- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertView *alert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Can't sign in? ", @"Can't sign in? ")
                                        message:NSLocalizedString(@"Enter your email address below and we will send you password reset instruction.", @"Enter your email address below and we will send you password reset instruction.")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    
   
    alert.tag = 1;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [alert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [alert show];
    
}

- (IBAction)signInButtonClicked:(id)sender {
    [self.view endEditing:YES];
     [self signInUser];
}


- (void) forgotPaswordAlertviewTextField
{
    UIAlertView *alert = [[UIAlertView alloc]
                                        initWithTitle:@"Invalid Email ID"
                                        message:@"Reenter your email ID "
                                        delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Submit", nil];
    alert.tag = 1;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [alert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [alert show];}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
       if(buttonIndex == 1)
        {
            UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
            TELogInfo(@"Email Name: %@", forgotEmailtext.text);
    
            if (((unsigned long)forgotEmailtext.text.length ==0) || [Helper emailValidationCheck:forgotEmailtext.text] == 0)
            {
                    [self forgotPaswordAlertviewTextField];
            }
            else
            {
                [self retrievePassword:forgotEmailtext.text];
            }
        }
   
       else
       {
            TELogInfo(@"cancel");
            passwordTextField.text = @"";
        }
    
}

- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Sending...", @"Sending...")];
    
    NSDictionary *params = @{
                             @"ent_email":text,
                             @"ent_user_type":@"2",
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"forgotPassword"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       
                                       [self retrievePasswordResponse:response];
                                   }
                               }];
    
}

-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        NSDictionary *dictResponse = [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
             [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}



- (BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}
-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
       {
        for (int i =0; i<_arrayContainingCardInfo.count; i++)
        {
            [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
        }
    }
    
}

#pragma marks - Login with Facebook

- (IBAction)logInWithFacebookButtonAction:(id)sender {

    FBLoginHandler *fb= [[FBLoginHandler alloc]init];
    fb.delegate = self;
    // Check for Network Availability
    if ([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        [fb loginWithFacebook];
    }
    else
    {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}
/*------------------------------------------------*/
#pragma Facebook Delegate method
/*------------------------------------------------*/
-(void)didFacebookUserLogin:(BOOL)login withDetail:(NSDictionary*)userInfo{
    
    if(login == NO){
        [Helper showAlertWithTitle:@"Message" Message:@"Facebook Login is cancelled by user"];
        return;
    }
    
    // Check for Network Availability
    if ([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
        
        NSDictionary *params = @{
                                 @"ent_email":[userInfo objectForKey:@"email"],
                                 @"zip_code":@"560024",
                                 @"ent_user_type":@"2",
                                 KDASignUpDateTime:[Helper getCurrentDateTime],
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"validateEmailZip"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           TELogInfo(@"response %@",response);
                                           [self validateEmailResponse:response detail:userInfo];
                                       }
                                   }];

    }
    else
    {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void)validateEmailResponse :(NSDictionary *)response detail:(NSDictionary *)detailaData
{
   
    
    TELogInfo(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            facebookData = detailaData;
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [self performSegueWithIdentifier:@"loginToSignupController" sender:self];
            
        }
        else{
                [self getFbLogInService:detailaData];
        }
    }
}


- (void)getFbLogInService:(NSDictionary *)dictionary
{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Logging..", @"Logging..")];

    facebookData = dictionary;
    NSString *emailID;
    if ([facebookData objectForKey:@"email"])
    {
        emailID = [facebookData objectForKey:@"email"];
    }
    else
    {
        emailID = [NSString stringWithFormat:@"%@@gmail.com",[facebookData objectForKey:@"id"]];
    }
    NSString *fbid = [facebookData objectForKey:@"id"];
    
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    NSString *pushToken;
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pushToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pushToken = @"123abhi";
    }
    NSString *lat;
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = [NSString stringWithFormat:@"%@",@"13.028793839817"];
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    NSString *lon;
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = [NSString stringWithFormat:@"%@",@"77.589587399095"];
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    NSString *city;
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"Bengaluru";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    
    
    
    NSDictionary *queryParams  =  @{@"ent_first_name":facebookData[@"first_name"],
                                    @"ent_last_name":facebookData[@"last_name"],
                                    @"ent_email" :emailID,
                                    @"ent_fb_id":fbid,
                                    @"ent_latitude":lat,
                                    @"ent_longitude":lon,
                                    @"ent_device_type" :@"1",
                                    @"ent_dev_id" :deviceId,
                                    @"ent_push_token":pushToken,
                                    @"ent_zipcode":@"qwe",
                                    @"ent_date_time":[Helper getCurrentDateTime],
                                    };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"FacebookSlaveLogin"
                              paramas:queryParams
                         onComplition:^(BOOL success , NSDictionary *response){
                             

                             if (success) {
                                 
                                 [self getFbLoginServiceResponse:response];
                             }
                             else{
                                 
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}




- (void)getFbLoginServiceResponse: (NSDictionary *) response
{
    NSLog(@"Facebook Response : %@",response);
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    //check if response is not null
    if(response == nil)
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    
    //check for network error
    if (response[@"Error"])
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:response[@"Error"]];
    }
    if ([[response objectForKey:@"errNum"] intValue] == 11){
        [Helper showAlertWithTitle:@"Message" Message:@"This Email Id is Temporarely Deactivated Please Log IN with Other Credential"];
       
    }

    else
    {
        NSDictionary *itemLists = [[NSDictionary alloc]init];
        itemLists = [response mutableCopy];
        
        if ([itemLists[@"errFlag"] intValue] == 1) {
            
            [self performSegueWithIdentifier:@"loginToSignupController" sender:self];
        }
        else
        {
            NSString *fbImageURL = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",facebookData[@"id"]];

         
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            
            [ud setObject:itemLists[@"profilePic"] forKey:KDAProfilePic];
            [ud setObject:itemLists[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:itemLists[@"chn"] forKey:kNSUPatientPubNubChannelkey];
            [ud setObject:itemLists[@"email"] forKey:kNSUPatientEmailAddressKey];
            [ud setObject:itemLists[@"apiKey"] forKey:kNSUMongoDataBaseAPIKey];
            [ud setObject:itemLists[@"serverChn"] forKey:kPMDPublishStreamChannel];
            [ud setObject:facebookData[@"first_name"] forKey:KDAFirstName];
            [ud setObject:facebookData[@"last_name"] forKey:KDALastName];

            [ud setObject:[NSString stringWithFormat:@"YES"] forKey:isFromFB];
            [ud setObject:fbImageURL forKey:KDAFbProfilePicURL];
            
            
            [ud synchronize];
            
           
            
            if(![ud objectForKey:KDAcheckUserSessionToken])
            {
                return;
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        }
    }
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"loginToSignupController"])
    {
        SignUpViewController *obj = [segue destinationViewController];
        obj.isComingFromLogin = YES;
        obj.facebookData = facebookData;
    }
}


@end
