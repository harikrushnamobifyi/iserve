//
//  MapViewController.h
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "WildcardGestureRecognizer.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "MyAppTimerClass.h"

typedef enum {
    isBookingDetailsServieCalled = 0,
    isBookingDetailsViewShown = 1
}BookingStatus;

@interface MapViewController : UIViewController<GMSMapViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPopoverControllerDelegate>
{
    BOOL isNurseSelected;
    BOOL isLaterSelected;
    BOOL isNowSelected;
    BOOL isCustomMarkerSelected;
    BOOL isFareButtonClicked;
    BOOL isRequestingButtonClicked;
    
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
	NSManagedObjectContext *context;
    
    UIDatePicker *datepicker;
    UIActionSheet *newSheet;
    UIPopoverController *popOverForDatePicker;
    
    MFMailComposeViewController *mailer;
    
}
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property(nonatomic,strong) UIPickerView *pkrView;
@property (strong, nonatomic) NSMutableArray *doctors ;
@property (strong,nonatomic) NSMutableArray *temp;
@property (strong, nonatomic) NSArray *buttonArray ;
@property (strong, nonatomic) NSArray *buttonArray2 ;
@property (strong, nonatomic) NSDictionary *dictSelectedDoctor;
@property (strong, nonatomic) UITextField *textFeildAddress;
@property (strong, nonatomic) UITableView *filterTableView;
@property (assign, nonatomic) BookingStatus bookingStatus;
@property MyAppTimerClass *myAppTimerClassObj;
@property (strong, nonatomic) NSArray *mastersArr;
@property (strong, nonatomic) NSMutableArray *headerTitleArr;
@property (strong, nonatomic) NSMutableDictionary *mDictLatLong;
@property (strong, nonatomic) NSDictionary  *dictForOneDocDetails;




- (IBAction)datePickerDoneButtonAction:(id)sender;

+ (instancetype) getSharedInstance;
-(void)publishPubNubStream;
-(void)hideActivityIndicatorWithMessage;
- (void) sendRequestgetETAnDistance;


@end
