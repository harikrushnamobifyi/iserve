//
//  StatusView.m
//  iserve
//
//  Created by Rahul Sharma on 04/09/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "StatusView.h"

@interface StatusView()


@property (strong, nonatomic) IBOutlet UIView *jobProgressIndicator;
@property (strong, nonatomic) IBOutlet UIButton *buttonOnTheWay;
@property (strong, nonatomic) IBOutlet UIButton *buttonStartJob;
@property (strong, nonatomic) IBOutlet UIButton *buttonJobDone;

@end




@implementation StatusView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

static StatusView  *statusView = nil;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self =  [[[NSBundle mainBundle] loadNibNamed:@"StatusView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance {
    
    if (!statusView) {
        
        statusView  = [[self alloc] initWithFrame:CGRectZero];
        statusView.frame = CGRectMake(0,64,320,85);
        
    }
    
    return statusView;
}

-(void)updateValues:(BookingNotificationType )status {
    
    switch (status) {
        case kNotificationTypeBookingOnMyWay:
            [_buttonOnTheWay setUserInteractionEnabled:YES];
            [_buttonStartJob setUserInteractionEnabled:NO];
            [_buttonJobDone setUserInteractionEnabled:NO];
            break;
        case kNotificationTypeBookingReachedLocation:
        case kNotificationTypeBookingStarted:
            [_buttonOnTheWay setUserInteractionEnabled:NO];
            [_buttonStartJob setUserInteractionEnabled:YES];
            [_buttonJobDone setUserInteractionEnabled:NO];
            break;
        case kNotificationTypeBookingComplete:
            [_buttonOnTheWay setUserInteractionEnabled:NO];
            [_buttonStartJob setUserInteractionEnabled:NO];
            [_buttonJobDone setUserInteractionEnabled:YES];
            break;
        
            
        default:
            break;
    }
   

}

@end
