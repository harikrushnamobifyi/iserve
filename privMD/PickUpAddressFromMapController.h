//
//  Map1ViewController.h
//  Servodo
//
//  Created by Rahul Sharma on 10/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

/**
 *  Save Address to Dtabase Delegate Method
 */
@protocol SaveAddressDelegate <NSObject>

-(void)currentAddressWithLat:(float)latitude andLong:(float)longitude andAddress:(NSString *)addressText;

@end

/**
 *  Adding Address Delegate Method
 */
@protocol CustomMapViewDelegate

- (void)addaddress:(BOOL)value;

@end


@interface PickUpAddressFromMapController : UIViewController <GMSMapViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,CLLocationManagerDelegate>
/**
 *  Identifier of Save Address Delegate Method
 */
@property (nonatomic,strong)  id <SaveAddressDelegate> saveAddressDelegate;

/**
 *  Identifier of addAddress Delegate Method
 */
@property (nonatomic, assign) id<CustomMapViewDelegate>delegate;

/**
 *  topView containing the address1,address2,Zipcode,home,notes1 TextFields
 */
@property (strong, nonatomic) IBOutlet UIView *topContainerView;

/**
 *  used to get address from user
 */
@property (strong, nonatomic) IBOutlet UITextField *AddressLine1;

/**
 *  used to get sub address from user
 */
@property (strong, nonatomic) IBOutlet UITextField *addressLine2;

/**
 *  used to store zipcode
 */
@property (strong, nonatomic) IBOutlet UITextField *ZipCode1;

/**
 *  used to store city name
 */
@property (strong, nonatomic) IBOutlet UITextField *home;

/**
 *  used to store House number
 */
@property (strong, nonatomic) IBOutlet UITextField *notes1;

/**
 *  used to set the selected location
 */
@property (strong, nonatomic) IBOutlet UIButton *customMarkerButton;

/**
 *  mapview used to select the address
 */
@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;

/**
 *  used to store destination Address
 */
@property (strong, nonatomic) IBOutlet UILabel *destinationAddressLabel;

/**
 *  <#Description#>
 */
@property (strong, nonatomic) IBOutlet UIView *setDelAddButtonView;

/**
 *  <#Description#>
 */
@property (strong, nonatomic) IBOutlet UIView *addLine1View;
@property (strong, nonatomic) IBOutlet UIView *addLine2View;
@property (strong, nonatomic) IBOutlet UIView *homeView;
@property (strong, nonatomic) IBOutlet UIView *zipCodeView;
@property (strong, nonatomic) IBOutlet UIView *notesView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *tableContainerView;

@property (nonatomic,assign) BOOL isSetbuttonclicked;
@property (nonatomic,assign) int integer;
@property int isComingFromAddAddressView;
@property (strong, nonatomic) NSString *destAddress;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;
@property (strong, nonatomic)  NSString *searchString;
@property (strong, nonatomic) NSMutableArray *arrayOfAddress;


- (IBAction)customMarkerTopBottomClicked:(UIButton *) sender;


@end


