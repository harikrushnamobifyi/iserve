//
//  AppConstants.m
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AppConstants.h"

#pragma mark - Constants
NSString *const kPMDPublishStreamChannel = @"all_types";
NSString *const kPMDPubNubPublisherKey   = @"pub-c-c1a8ca63-4252-4b98-9414-8781e29c0956";
NSString *const kPMDPubNubSubcriptionKey = @"sub-c-ee03a4c2-5632-11e5-bfbc-02ee2ddab7fe";
NSString *const kPMDGoogleMapsAPIKey     = @"AIzaSyAH95Cr3QQnPJq8so6w6-hQIaGKw7xMNdU";
NSString *const kPMDFlurryId             = @"24VV3H2V2RPM6ZXG4G99";
NSString *const kPMDTestDeviceidKey      = @"C2A33350-D9CF-4A7E-8751-A36016838381";
NSString *const kPMDDeviceIdKey          = @"deviceid";
NSString *const kPMDStripeTestKey        = @"pk_test_IBYk0hnidox7CDA3doY6KQGi";
NSString *const kPMDStripeLiveKey        = @"pk_live_vjLBkUeVuxSULHpvj6zdu867";

#pragma mark - mark URLs

#define BASE_IP  @"http://www.iserve.ind.in/iServe_v2/"



NSString *BASE_URL                           = BASE_IP @"services_v7.php/";
NSString *BASE_URL_SUPPORT_WEB               = BASE_IP;
NSString *BASE_URL_UPLOADIMAGE               = BASE_IP @"services_v7.php/uploadImage";
NSString *const   baseUrlForXXHDPIImage      = BASE_IP @"pics/xxhdpi/";
NSString *const   baseUrlForOriginalImage    = BASE_IP @"pics/";
#pragma mark - ServiceMethods
// eg : prifix kSM
NSString *const kSMLiveBooking                = @"liveBooking";
NSString *const kSMGetAppointmentDetial       = @"getAppointmentDetails";
NSString *const kSMUpdateSlaveReview          = @"updateSlaveReview";
NSString *const kSMGetMasters                 = @"getMasters";
NSString *const kSMCancelAppointment          = @"cancelAppointment";
NSString *const kSMCancelOngoingAppointment   = @"cancelAppointmentRequest";

//Methods
NSString *MethodPatientSignUp                = @"slaveSignup";
NSString *MethodPatientLogin                 = @"slaveLogin";
NSString *MethodDoctorUploadImage            = @"uploadImage";
NSString *MethodPassengerLogout              = @"logout";
NSString *MethodFareCalculator               = @"fareCalculator";


//SignUp

NSString *KDASignUpFirstName                  = @"ent_first_name";
NSString *KDASignUpLastName                   = @"ent_last_name";
NSString *KDASignUpMobile                     = @"ent_mobile";
NSString *KDASignUpEmail                      = @"ent_email";
NSString *KDASignUpPassword                   = @"ent_password";
NSString *KDASignUpAddLine1                   = @"ent_address_line1";
NSString *KDASignUpAddLine2                   = @"ent_address_line2";
NSString *KDASignUpAccessToken                = @"ent_token";
NSString *KDASignUpDateTime                   = @"ent_date_time";
NSString *KDASignUpCountry                    = @"ent_country";
NSString *KDASignUpCity                       = @"ent_city";
NSString *KDASignUpDeviceType                 = @"ent_device_type";
NSString *KDASignUpDeviceId                   = @"ent_dev_id";
NSString *KDASignUpPushToken                  = @"ent_push_token";
NSString *KDASignUpZipCode                    = @"ent_zipcode";
NSString *KDASignUpCreditCardNo               = @"ent_cc_num";
NSString *KDASignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *KDASignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *KDASignUpTandC                      = @"ent_terms_cond";
NSString *KDASignUpPricing                    = @"ent_pricing_cond";
NSString *KDASignUpLatitude                   = @"ent_latitude";
NSString *KDASignUpLongitude                  = @"ent_longitude";

// Login

NSString *KDALoginEmail                       = @"ent_email";
NSString *KDALoginPassword                    = @"ent_password";
NSString *KDALoginDeviceType                  = @"ent_device_type";
NSString *KDALoginDevideId                    = @"ent_dev_id";
NSString *KDALoginPushToken                   = @"ent_push_token";
NSString *KDALoginUpDateTime                  = @"ent_date_time";

//Upload
//Upload
NSString *KDAUploadDeviceId                    = @"ent_dev_id";
NSString *KDAUploadSessionToken                = @"ent_sess_token";
NSString *KDAUploadImageName                   = @"ent_snap_name";
NSString *KDAUploadImageChunck                 = @"ent_snap_chunk";
NSString *KDAUploadfrom                        = @"ent_upld_from";
NSString *KDAUploadtype                        = @"ent_snap_type";
NSString *KDAUploadDateTime                    = @"ent_date_time";
NSString *KDAUploadOffset                      = @"ent_offset";

// Logout the user

NSString *KDALogoutSessionToken                = @"user_session_token";
NSString *KDALogoutUserId                      = @"logout_user_id";



//Parsms for checking user loged out or not

NSString *KDAcheckUserId                        = @"user_id";
NSString *KDAcheckUserSessionToken              = @"ent_sess_token";
NSString *KDAgetPushToken                       = @"ent_push_token";

//Params to store the Country & City.

NSString *KDACountry                            = @"country";
NSString *KDACity                               = @"city";
NSString *KDALatitude                           = @"latitudeQR";
NSString *KDALongitude                          = @"longitudeQR";

//params for Firstname
NSString *KDAFirstName                          = @"ent_first_name";
NSString *KDALastName                           = @"ent_last_name";
NSString *KDAEmail                              = @"ent_email";
NSString *KDAPhoneNo                            = @"ent_mobile";
NSString *KDAPassword                           = @"ent_password";
NSString *KDAProfilePic                         = @"profilepic";
NSString *KDAFbProfilePicURL                    = @"fbProfilePicURL";
NSString *isFromFB                              = @"isFromFB";



#pragma mark - NSUserDeafults Keys
NSString *const kNSUPatientPubNubChannelkey           = @"pChannel";
NSString *const kNSUAppoinmentDoctorDetialKey         = @"doctorDetial";
NSString *const kNSUPatientEmailAddressKey            = @"pEmail";
NSString *const kNSUMongoDataBaseAPIKey               = @"mongoDBapi";
NSString *const kNSUIsPassengerBookedKey              = @"passengerBooked";
NSString *const kNSUPassengerBookingStatusKey         = @"STATUSKEY";
NSString *const KUBERCarArrayKey                      = @"carTypeArray";
NSString *const kNSUPatientCouponkey                  = @"coupon";


#pragma mark - Notification Name keys
NSString *const kNotificationNewCardAddedNameKey   = @"cardAdded";
NSString *const kNotificationCardDeletedNameKey   = @"cardDeleted";
NSString *const kNotificationLocationServicesChangedNameKey = @"CLChanged";
NSString *const kNotificationBookingConfirmationNameKey = @"bookingConfirmed";

#pragma mark - Network Error
NSString *const kNetworkErrormessage          = @"No network connection";

NSString *const KNUCurrentLat          = @"latitude";
NSString *const KNUCurrentLong         = @"longitude";
NSString *const KNUserCurrentCity      = @"usercity";
NSString *const KNUserCurrentState     = @"userstate";
NSString *const KNUserCurrentCountry   = @"userCountry";

NSString *const KUDriverEmail           = @"DriverEmail";
NSString *const KUBookingDate           = @"BookingDate";
NSString *const KUBookingID             = @"BookingID";





#pragma Mark - OnGoingAppointmentKeys

NSString *const kOnGoingBookingDoctorEmailKey               = @"email";
NSString *const kOnGoingBookingDoctorBookingDate            = @"bookingdate";
NSString *const kOnGoingBookingDoctorSubscribedChannelKey   = @"channel";
NSString *const kOnGoingBookingDoctorNameKey                = @"name";
NSString *const kOnGoingBookingDoctorPhoneNumberKey         = @"phone";
NSString *const kOnGoingBookingDoctorRatingKey              = @"rating";
NSString *const kOnGoingBookingDoctorProfileImageKey        = @"driverpic";




#pragma mark - PushNotification Payload Keys

NSString *const kPNPayloadDoctorNameKey                     = @"n";
NSString *const kPNPayloadAppoinmentTimeKey                 = @"d";
NSString *const kPNPayloadDoctorEmailKey                    = @"e";
NSString *const kPNPayloadDoctorChannelKey                  = @"chn";
NSString *const kPNPayloadDoctorContactNumberKey            = @"ph";
NSString *const kPNPayloadProfilePictureUrlKey              = @"pic";
NSString *const kPNPayloadAppoinmentDateStringKey           = @"d";
NSString *const kPNPayloadAppoinmentLatitudeKey             = @"ltg";
NSString *const kPNPayloadDoctorRatingKey                   = @"r";



