


#import "BookingHistoryBaseViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "CarbonKit.h"

@interface BookingHistoryBaseViewController () <CarbonTabSwipeNavigationDelegate,CustomNavigationBarDelegate>
{
	NSArray *items;
	CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@end

@implementation BookingHistoryBaseViewController

- (void)viewDidLoad {
    
	[super viewDidLoad];
    
	self.title = @"CarbonKit";
	
    items = @[@"Acitve",
              @"Cancelled",
              @"Completed"
              ];
	
	carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
	[carbonTabSwipeNavigation insertIntoRootViewController:self];
	
	[self style];
    
    [self addCustomNavigationBar];

}
#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"BOOKINGS"];
    [customNavigationBarView hideRightBarButton:YES];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    
    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)style {

	UIColor *color = [UIColor colorWithRed:24.0/255 green:75.0/255 blue:152.0/255 alpha:1];
	self.navigationController.navigationBar.translucent = NO;
	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	self.navigationController.navigationBar.barTintColor = color;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	
	carbonTabSwipeNavigation.toolbar.translucent = NO;
	[carbonTabSwipeNavigation setIndicatorColor:color];
	[carbonTabSwipeNavigation setTabExtraWidth:30];
	[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:0];
	[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:1];
	[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:80 forSegmentAtIndex:2];
	
	// Custimize segmented control
	[carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
				  font:[UIFont boldSystemFontOfSize:14]];
	[carbonTabSwipeNavigation setSelectedColor:color
				    font:[UIFont boldSystemFontOfSize:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
				 viewControllerAtIndex:(NSUInteger)index {
	switch (index) {
		case 0:
			return [self.storyboard instantiateViewControllerWithIdentifier:@"bookingViewController"];
			
		case 1:
			return [self.storyboard instantiateViewControllerWithIdentifier:@"bookingViewController"];
            
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"bookingViewController"];
            
        case 3:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"bookingViewController"];
			
		default:
			return [self.storyboard instantiateViewControllerWithIdentifier:@"bookingViewController"];
	}
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
		 willMoveAtIndex:(NSUInteger)index {
	switch(index) {
		case 0:
			self.title = @"BOOKING HISTORY";
			break;
		case 1:
			self.title = @"BOOKING HISTORY";
			break;
		case 2:
			self.title = @"BOOKING HISTORY";
			break;
		default:
			self.title = items[index];
			break;
	}
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
		  didMoveAtIndex:(NSUInteger)index {
	NSLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
	return UIBarPositionTop; // default UIBarPositionTop
}

@end
