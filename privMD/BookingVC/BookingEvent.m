//
//  BookingEvent.m
//  UBER
//
//  Created by Rahul Sharma on 05/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "BookingEvent.h"

#define kPic                                @"pPic"
#define kEmail                              @"email"
#define kFirstName                          @"fname"
#define kAddressLine1                       @"addrLine1"
#define kDropAddressLine1                   @"dropLine1"
#define kAddressLine2                       @"addrLine2"
#define kDropAddressLine2                   @"dropLine2"
#define kAppointmentDateTime                @"apntDt"
#define kAmount                             @"amount"
#define kDistance                           @"distance"
#define kStatus                             @"status"



@implementation BookingEvent


-(instancetype)initWithDictionary:(NSDictionary *)eventsDict forDate:(NSDate *)date {
    
    self = [super init];
    if (self) {
        
        self.title = [eventsDict  objectForKey:kEmail];
        self.image = [eventsDict objectForKey:kPic];
        self.name = [eventsDict objectForKey:kFirstName];
        self.pickAdd = [eventsDict  objectForKey:kAddressLine1];
        self.desAdd = [eventsDict  objectForKey:kDropAddressLine1];
        self.pickAdd2 = flStrForObj([eventsDict  objectForKey:kAddressLine2]);
        self.desAdd2 = flStrForObj([eventsDict  objectForKey:kDropAddressLine2]);
        self.time = [eventsDict objectForKey:kAppointmentDateTime];
        self.distance = [eventsDict objectForKey:kDistance];
        self.amount = [eventsDict  objectForKey:kAmount];
        self.status = [eventsDict objectForKey:kStatus];
        self.date = date;
        self.info = eventsDict;
    }
    

    return self;
}

@end
