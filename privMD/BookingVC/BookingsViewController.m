//
//  BookingsViewController.m
//  UBER
//
//  Created by Rahul Sharma on 05/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "BookingsViewController.h"
#import "BookingEvent.h"
#import "BookingTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "InvoiceViewController.h"
#import <Canvas/CSAnimationView.h>
#import "PatientViewController.h"


@interface BookingsViewController ()

@property (nonatomic,strong) NSMutableArray *events;
@property (nonatomic,strong) NSMutableArray *multipleEvents;

@end

@implementation BookingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _events = [NSMutableArray array];
    _multipleEvents = [NSMutableArray array];

    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    
    if ( [reachability isNetworkAvailable]) {
        
        //Call service here to get details of on going bookings
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
//    NSInteger count = [self events].count;//[[[self multipleEvents] objectAtIndex:section]count];
//    if (count == 0) {
//        count = 3;
//    }
    return 5;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
        
//    BookingEvent *event = [[self events] objectAtIndex:[indexPath section]];
    
    BookingTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.proProfileImageView.image = [UIImage imageNamed:@"menu_profile_default_image"];
    
    [cell.proProfileImageView sd_setImageWithURL:[NSURL URLWithString:@""]
                     placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){}];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"onTheWayVC" sender:self];
//    if ([[self events] count] == 0) {
//        return;
//    }
//    BookingEvent *event = [[self events]objectAtIndex:indexPath.section];
//    NSDictionary *dictionary = event.info;
//    if ([dictionary[@"statCode"] integerValue] == 4) {
//        
//        [Helper showAlertWithTitle:@"Message" Message:@"This Booking has been cancelled by you."];
//    }
//    else if([dictionary[@"statCode"] integerValue] == 5)
//    {
//        [Helper showAlertWithTitle:@"Message" Message:@"This Booking has been cancelled by Driver."];
//
//    }
//    else
//    {
//        [self ButtonPressed:dictionary];        
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 150;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
     [cell startCanvasAnimation];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 20;
    }
    return 0;
}

-(void)ButtonPressed:(NSDictionary *)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main"  bundle:nil];
    self.definesPresentationContext = YES;
    
    InvoiceViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
    
    invoiceVC.doctorEmail = sender[@"email"];
    invoiceVC.appointmentDate = sender[@"apntDt"];
    invoiceVC.isComingFromAppointmnetScreen = YES;
    invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    invoiceVC.view.superview.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-40); //it's important to do this after presentModalViewController
    invoiceVC.view.superview.center = self.view.center;
    
    PatientAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    UINavigationController *naviVC =(UINavigationController*) appDelegate.window.rootViewController;
    
    [naviVC presentViewController:invoiceVC animated:YES completion:nil];
    
}

@end
