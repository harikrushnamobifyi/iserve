//
//  BookingTableViewCell.h
//  iserve
//
//  Created by Rahul Sharma on 4/13/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView.h>

@interface BookingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *professionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *creationDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *proIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *proBokkingStatusLabel;

@property (weak, nonatomic) IBOutlet UIImageView *proProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *proNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *proRatingLabel;
@property (weak, nonatomic) IBOutlet UILabel *proReviewLabel;

@end
