	//
//  SocketIOWrapper.m
//  Snapchat
//
//  Created by Rahul Sharma on 11/09/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "SocketIOWrapper.h"
#import "SIOClient.h"
#import "SIOConfiguration.h"
#import "AppConstants.h"
#import "SignUpViewController.h"
#import "Helper.h"

static SocketIOWrapper *shared = nil;
#define timeStamp [[NSDate date] timeIntervalSince1970]*1000

@interface SocketIOWrapper()<SIOClientDelegate>
@property SIOClient *socketIOClient;
@end

@implementation SocketIOWrapper

@synthesize socketDelegate;


+(instancetype)sharedInstance
{
    if (!shared) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
       });
    }
    return shared;
}

-(void)connectSocket {
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [shared socketIOSetup];
    }];
}

-(void)disconnectSocket {
    
}

-(void)socketIOSetup
{
    //shared.socketIOClient =[[SIOClient alloc]init];
    SIOConfiguration *config = [SIOConfiguration defaultConfiguration];
    self.socketIOClient = [SIOClient sharedInstance];
    [self.socketIOClient setConfiguration:config];
    [self.socketIOClient setDelegate:self];
    [self.socketIOClient connect];
    self.channelsName = [[NSMutableArray alloc] init];
    
}

-(void)sendHeartBeatForUser:(NSString*)userName withStatus:(NSString *)status andPushToken:(NSString *)pushToken {
    
    if (!pushToken) {
        pushToken = @"";
    }
    
    
    NSDictionary *dict = @{@"from":userName,
                           @"status":status,
                           @"pushtoken": pushToken,
                           @"device":@"ios"
                           };
    
    NSLog(@"heartbeat: %@", dict);
    
    [self.socketIOClient publishToChannel:@"Heartbeat" message:dict];
    
    
}

-(void)publishMessageToChannel:(NSString *)channel withMessage:(NSDictionary *)messageDict {
    
    [self.socketIOClient publishToChannel:channel message:messageDict];


}
- (void) sioClient:(SIOClient *)client didConnectToHost:(NSString*)host {
   
    NSLog(@"SIO connect to %@", host);
    
    if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(didConnect)]) {
        [socketDelegate didConnect];
    }
    if(![self.channelsName containsObject:@"UpdateCustomer"])
    {
        [self.channelsName addObject:@"UpdateCustomer"];
        [self.socketIOClient subscribeToChannels:@[@"UpdateCustomer"]];
    }
    
}


- (void) sioClient:(SIOClient *)client didSubscribeToChannel:(NSString*)channel {
    NSLog(@"SIO subscribe to %@", channel);
}


- (void) sioClient:(SIOClient *)client didSendMessageToChannel:(NSString *)channel {
    NSLog(@"SIO message sent to %@", channel);
}


- (void) sioClient:(SIOClient *)client didRecieveMessage:(NSArray*)message onChannel:(NSString *)channel {
    
    NSLog(@"SIO message recieve to %@", channel);
    if (message.count == 0) {
        return;
    }
    
    NSDictionary *dict = message[0];
    if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(receievedMessageOnChannel:withMessage:)]) {
        [socketDelegate receievedMessageOnChannel:channel withMessage:dict];
    }

}

- (void) sioClient:(SIOClient *)client didDisconnectFromHost:(NSString*)host {
 
    NSLog(@"SIO disconnect from %@", host);
}

- (void) sioClient:(SIOClient *)client gotError:(NSDictionary *)errorInfo {
   
    NSLog(@"SIO Error : %@", errorInfo);
}

@end
