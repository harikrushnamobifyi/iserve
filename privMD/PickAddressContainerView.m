//
//  PickAddressContainerView.m
//  Deliver_Bird
//
//  Created by Rahul Sharma on 05/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "PickAddressContainerView.h"
#import "PickUpAddressTableViewCell.h"

@interface PickAddressContainerView ()<PickAddressDelegate>
{
    NSMutableArray *getAddress1;
    NSMutableArray *getAddress2;
    NSMutableArray *gethome;
    NSMutableArray *getlatitude;
    NSMutableArray *getlongitude;
    NSMutableArray *getNotes;
    int removeButtonTag;
    float pickedLatitude;
    float pickedLongitude;
    NSString *pickedAddress;
}


@end

@implementation PickAddressContainerView

@synthesize arrayOfAddress;
@synthesize onCompletion;
@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;
@synthesize pickAddressDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isSearchResultCome = NO;
    
    [self createFooterViewForTable];
}

-(void)viewWillAppear:(BOOL)animated
{
}
- (void)refresh:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dismissViewController:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
#pragma mark -
#pragma mark UITableView DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  arrayOfAddress.count;;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"pickTableCell";
    PickUpAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.addressLine1.text = [arrayOfAddress[indexPath.row][@"terms"] objectAtIndex:0][@"value"] ;
    cell.addressLine2.text = arrayOfAddress[indexPath.row][@"description"];
    
    UIView *cellSepearator = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 0.5)];
    cellSepearator.backgroundColor = UIColorFromRGB(0xb7b7b7);
    [cell addSubview:cellSepearator];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!arrayOfAddress.count ==0)
    {
        if (section == 0)
        {
            return 25;
        }
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSDictionary *searchResult = [self.arrayOfAddress objectAtIndex:indexPath.row];
        NSString *placeID = [searchResult objectForKey:@"reference"];
    
        [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place)
         {
             NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
             NSString *add2 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"formatted_address"])];
             if (add1.length == 0)
             {
                 add1 = [add1 stringByAppendingString:flStrForStr([place valueForKey:@"formatted_address"])];
                 add2 = @"";
             }
             
             NSString *late = [NSString stringWithFormat:@"%@,",[place valueForKey:@"geometry"][@"location"][@"lat"]];
             NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
             NSString *loca = [NSString stringWithFormat:@"%@",[place valueForKey:@"address_components"][1][@"short_name"]];
             
             NSDictionary *dict = @{@"add1":add1,
                                    @"add2":add2,
                                    @"lat":late,
                                    @"long":longi,
                                    @"Home":loca
                                    };
             
             pickedAddress = arrayOfAddress[indexPath.row][@"description"];
             NSLog(@"Picked Latitude = %f",pickedLatitude);
             NSLog(@"Picked Longitude = %f",pickedLongitude);
             NSLog(@"Picked Address = %@",pickedAddress);
             [pickAddressDelegate pickedAddressWithLat:pickedLatitude andLong:pickedLongitude andAddress:dict];
             
            
         }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Result";
}

- (void)createFooterViewForTable {
    
    UIView *footerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320,98/2)];
    footerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredby_google"]];
    //imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    imageView.frame = CGRectMake(0,0, 320,98/2);
    [footerView addSubview:imageView];
    self.mapPickerTableView.tableFooterView = footerView;
}

-(void)getAddressOfArrayAndReloadTableView:(NSMutableArray *)array {
    
    self.arrayOfAddress = [array mutableCopy];
    [self.mapPickerTableView reloadData];
    
}
-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,@"AIzaSyDzFpzMhM014fzTzOErkt3M7nKRnzwz1hs"];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    [task resume];
}


@end
