//
//  ViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "ViewController.h"
#import "PickUpViewController.h"
#import "AccountViewController.h"
#import "MapViewController.h"
#import "BookingHistoryBaseViewController.h"
#import "PaymentViewController.h"
#import "SupportViewController.h"
#import "InviteViewController.h"
#import "AboutViewController.h"
#import "AddressManageViewController.h"

typedef NS_ENUM(NSInteger,iDeliverVCType) {
    
    iServeProfileVC = 0,
    iServeBookVC,
    iServeBookingDetailsVC,
    iServePaymentVC,
    iServeMyAddressVC,
    iServeSupportVC,
    iServeShareVC,
    iServeAboutVC,
    
};



@interface ViewController ()<XDKAirMenuDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) MapViewController *homeVC;
@property(nonatomic, strong) BookingHistoryBaseViewController *BookVC;
@property(nonatomic, strong) SupportViewController *supportVC;
@property(nonatomic, strong) PaymentViewController *paymentVC;
@property(nonatomic, strong) AboutViewController *aboutVC;
@property(nonatomic, strong) InviteViewController *inviteVC;
@property(nonatomic, strong) AccountViewController *profileVC;
@property(nonatomic, strong) AddressManageViewController *addressVC;

@end

@implementation ViewController


- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];
    [_tableView setBackgroundColor:UIColorFromRGB(0xffffff)];
    
    if (IS_IOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.airMenuController = [XDKAirMenuController sharedMenu];
    self.airMenuController.airDelegate = self;
    [self.view addSubview:self.airMenuController.view];
    [self addChildViewController:self.airMenuController];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"TableViewSegue"])
    {
        self.tableView = ((UITableViewController*)segue.destinationViewController).tableView;
    }
}


#pragma mark - XDKAirMenuDelegate

- (UIViewController*)airMenu:(XDKAirMenuController*)airMenu viewControllerAtIndexPath:(NSIndexPath*)indexPath
{
    UIStoryboard *storyboard = self.storyboard;
    UIViewController *vc = nil;
    
    vc.view.autoresizesSubviews = TRUE;
    vc.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
   
    
    switch (indexPath.row) {
        case iServeProfileVC:
        {
            if (!_profileVC) {
                
                _profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
                return _profileVC;
                
            }
            
            return _profileVC;
            
        }
            break;
        case iServeBookVC:{
            
                _homeVC = [storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
                return _homeVC;
            
        }
            break;
        case iServeBookingDetailsVC: {
            
                _BookVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingHistoryViewController"];
                return _BookVC;
            
        }
            break;
            
        case iServePaymentVC: {
            
                _paymentVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentVC"];
                return _paymentVC;
        
        }
            break;
        
        case iServeMyAddressVC: {
            
            if (!_addressVC) {
                
                _addressVC = [storyboard instantiateViewControllerWithIdentifier:@"AddressManageViewController"];
                return _addressVC;
                
            }
            return _addressVC;
            
            }
            break;
            
            
        case iServeSupportVC: {
            
            if (!_supportVC) {
                
                _supportVC = [storyboard instantiateViewControllerWithIdentifier:@"supportTableVC"];
                return _supportVC;
                
            }
            return _supportVC;
            
        }
            break;
            
        case iServeShareVC: {
            
            if (!_inviteVC) {
                
                _inviteVC = [storyboard instantiateViewControllerWithIdentifier:@"inviteVC"];
                return _inviteVC;
                
            }
            return _inviteVC;
            
        }
            break;
        case iServeAboutVC: {
            
            if (!_aboutVC) {
                
                _aboutVC = [storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
                return _aboutVC;
                
            }
            return _aboutVC;
        }
            
            break;
        default:
            vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            return vc;
            break;
    }

}

#pragma mark Webservice Handler(Request) -

- (UITableView*)tableViewForAirMenu:(XDKAirMenuController*)airMenu
{
    return self.tableView;
}

@end
