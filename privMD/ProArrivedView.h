//
//  ProArrivedView.h
//  Servodo
//
//  Created by Rahul Sharma on 18/06/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProArrivedView : UIView

@property(nonatomic,strong) NSTimer *upTimer;

+ (id)sharedInstance;
-(void)updateValues;

-(void)checkPreviousStatusOfRunningTimer;
- (void)populateLabelwithTime;
-(void)makeValueDefaultAfterBookingCompletion;

@end
