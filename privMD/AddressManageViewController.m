//
//  AddressManageViewController.m
//  Uberx
//
//  Created by Rahul Sharma on 19/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//



#import "AddressManageViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "Database.h"
#import "Addresscell.h"
#import "UserAddress.h"
#import "PickUpAddressFromMapController.h"

@interface AddressManageViewController ()<CustomNavigationBarDelegate>
{
    UserAddress *userAddressFromDB;
    NSInteger value;
    BOOL isright;
    NSInteger selectedIndex;
    NSMutableArray *addressArray;
}

@end

@implementation AddressManageViewController

@synthesize getAddress,manageAddressTableView;

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.manageAddressTableView.frame = CGRectMake(0, 126, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-126);
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self addCustomNavigationBar];

    addressArray = [[NSMutableArray alloc] initWithArray:[Database getAddressDetails]];
    [manageAddressTableView reloadData];
    
}


#pragma mark -tableview Delegates-

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.removeAddressButton setTag:indexPath.row + 100];
    [cell bringSubviewToFront:cell.removeAddressButton];
    [cell.removeAddressButton addTarget:self action:@selector(removeAddressButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    userAddressFromDB =  addressArray[indexPath.row];
    [cell.addressLabel1 setText:[NSString stringWithFormat:@"%@, %@",userAddressFromDB.notes,userAddressFromDB.addressLine1]];
    [cell.addressLabel2 setText:[NSString stringWithFormat:@"%@ %@",userAddressFromDB.addressLine2,userAddressFromDB.zipcode]];
   
    float height1 = [self measureHeightLabel:cell.addressLabel1];
    float height2 = [self measureHeightLabel:cell.addressLabel2];
    
    CGRect frameLabel1 = cell.addressLabel1.frame;
    frameLabel1.size.height = height1;
    cell.addressLabel1.frame = frameLabel1;
    
    CGRect frameLabel2 = cell.addressLabel2.frame;
    frameLabel2.origin.y = height1+5;    
    frameLabel2.size.height = height2;
    cell.addressLabel2.frame = frameLabel2;
    
    CGRect frameCrossButton = cell.removeAddressButton.frame;
    frameCrossButton.origin.y = (height1+5+height2+5)/2 - 30/2;
    cell.removeAddressButton.frame = frameCrossButton;
    

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 5,255,0)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 35,255,0)];
   
    userAddressFromDB =  addressArray[indexPath.row];
    
    [label1 setText:[NSString stringWithFormat:@"%@, %@",userAddressFromDB.notes,userAddressFromDB.addressLine1]];
    [label2 setText:[NSString stringWithFormat:@"%@ %@",userAddressFromDB.addressLine2,userAddressFromDB.zipcode]];
    
    
    [Helper setToLabel:label1 Text:label1.text WithFont:OpenSans_Regular FSize:16 Color:UIColorFromRGB(0x4A4971)];
    [Helper setToLabel:label2 Text:label2.text WithFont:OpenSans_Light FSize:16 Color:UIColorFromRGB(0x7E7E7E)];
    
    float height1 = [self measureHeightLabel:label1];
    float height2 = [self measureHeightLabel:label2];
   
    
    CGRect frameLabel1 = label1.frame;
    frameLabel1.size.height = height1;
    label1.frame = frameLabel1;
    
    CGRect frameLabel2 = label2.frame;
    frameLabel2.origin.y = height1+5;
    frameLabel2.size.height = height2;
    label2.frame = frameLabel2;
    
    return height1+height2+11;
}


- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    if (requiredHeight.size.width > label.frame.size.width)
    {
        requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
    }
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[Database getAddressDetails]];
    userAddressFromDB  = [array objectAtIndex:indexPath.row];
    NSDictionary *dict = @{
                           @"address1":userAddressFromDB.addressLine1,
                           @"address2":userAddressFromDB.addressLine2,
                           @"housenumber":userAddressFromDB.notes,
                           @"latitude":userAddressFromDB.latitude,
                           @"longitude":userAddressFromDB.longitude,
                           @"zipcode":userAddressFromDB.zipcode
                           };
    
    if (_isComingFromVC == 1) {
        [_addressManageDelegate getAddressFromDatabase:dict];
        [self leftBarButtonClicked:nil];
        
    }
}


#pragma mark -
#pragma mark -alert view delegates-

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 40)
    {
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            userAddressFromDB  = addressArray[selectedIndex];
            
            [Database DeleteAddress:[NSString stringWithFormat:@"%li",(long)userAddressFromDB.randomNumber.integerValue]];
            
            addressArray = [[NSMutableArray alloc] initWithArray:[Database getAddressDetails]];
            
            [manageAddressTableView reloadData];
        }
    }
}
/*-------------------------------- Navigation Bar ----------------------------------*/
#pragma mark -
#pragma mark -Navigation Bar Methods-

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"MY ADDRESS"];
    [customNavigationBarView hideRightBarButton:YES];
    
    if (_isComingFromVC == 1) {
        UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
        UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView setleftBarButtonImage:buttonImageOn :buttonImageOff];
    }
    
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    if (_isComingFromVC == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self menuButtonclicked];
    }
    
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark-
-(void)removeAddressButtonAction:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    selectedIndex = mBtn.tag - 100;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Remove Address?" message:@"Are you sure you want to remove this address ?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag=40;
    [alert show];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoaddressview"])
    {
        PickUpAddressFromMapController *obj1 = (PickUpAddressFromMapController *)[segue destinationViewController];
        obj1.isComingFromAddAddressView = 1;
    }
}

- (IBAction)addNewAddressButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"gotoaddressview" sender:sender];
}
@end

