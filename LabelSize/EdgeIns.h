//
//  EdgeIns.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 03/04/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EdgeIns : UILabel
@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
