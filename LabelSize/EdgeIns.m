//
//  EdgeIns.m
//  GuydeeSlave
//
//  Created by Rahul Sharma on 03/04/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "EdgeIns.h"

@implementation EdgeIns

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0,15, 0, 15);
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += self.edgeInsets.left + self.edgeInsets.right;
    size.height += self.edgeInsets.top + self.edgeInsets.bottom;
    return size;
}

@end
