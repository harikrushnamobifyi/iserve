//
//  UserAddress.h
//  Servodo
//
//  Created by Rahul Sharma on 15/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserAddress : NSManagedObject

@property (nonatomic, retain) NSString * addressLine1;
@property (nonatomic, retain) NSString * addressLine2;
@property (nonatomic, retain) NSNumber * zipcode;
@property (nonatomic, retain) NSString * home;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic ,retain) NSNumber *latitude;
@property (nonatomic ,retain) NSNumber *longitude;
@property (nonatomic ,retain) NSNumber *randomNumber;

@end
