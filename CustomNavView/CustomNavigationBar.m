//
//  CustomNavigationBar.m
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"

@interface CustomNavigationBar()
@property(nonatomic,strong)UILabel *labelTitle;
@property(nonatomic,strong)UIButton *rightbarButton;
@property(nonatomic,strong)UIButton *titleImageButton;
@property(nonatomic,strong)UIButton *leftbarButton;
@end
@implementation CustomNavigationBar
@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize leftbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_navigationbar"]]];

        //title
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 40)];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:labelTitle Text:@"" WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0xffffff)];
        labelTitle.numberOfLines = 2;
        [self addSubview:labelTitle];
        
        leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftbarButton.frame = CGRectMake(10, 20, 44, 44);
        leftbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];

        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_off"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_on"] forState:UIControlStateHighlighted];
        [leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftbarButton];
        [self createRightBarButton];

    }
    return self;
}

-(void)createRightBarButton
{
    
    UIImage *buttonImageOff = [UIImage imageNamed:@"search_btn_off"];
    UIImage *buttonImageOn = [UIImage imageNamed:@"search_btn_on"];
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake(276, 20,44, 44);
    rightbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateSelected];
    [rightbarButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];

}

-(void)addTitleButton{
    
    UIImage *buttonImage = [UIImage imageNamed:@"navigationbar_logo"];
    _titleImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_titleImageButton setUserInteractionEnabled:NO];
    _titleImageButton.frame = CGRectMake(70,26,170,27);
    [_titleImageButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    _titleImageButton.tag = 100;
    [self addSubview:_titleImageButton];
    [_titleImageButton setHidden:NO];
    [labelTitle setHidden:YES];
    
}

-(void)hideTitleButton:(BOOL)toHide {
    
//    if (toHide) {
//        [_titleImageButton setHidden:YES];
//
//        [labelTitle setHidden:NO];
//        
//    }
//    else {
//        [_titleImageButton setHidden:NO];
//        [labelTitle setHidden:YES];
//    }
}

-(void)hideLeftMenuButton:(BOOL)toHide {
    
    if (toHide) {
        [leftbarButton setBackgroundImage:nil forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:nil forState:UIControlStateHighlighted];;
        
    }
    else {
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_off"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_on"] forState:UIControlStateHighlighted];
    }
}

-(void)setCustomTitle:(NSString *)title{
    
    labelTitle.text = title;
   
    [_titleImageButton setHidden:YES];
    
    [labelTitle setHidden:NO];

}

-(void)setTitle:(NSString*)title{
 
    labelTitle.text = title;
    labelTitle.font = [UIFont fontWithName:OpenSans_Regular size:15];
    [_titleImageButton setHidden:YES];

    [labelTitle setHidden:NO];
}
-(void)setRightBarButtonTitle:(NSString*)title{
    [rightbarButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    
    [rightbarButton setTitle:title forState:UIControlStateNormal];
}
-(void)setLeftBarButtonTitle:(NSString*)title {
    [leftbarButton setTitle:title forState:UIControlStateNormal];
}
-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff{
    [leftbarButton setImage:imageOn forState:UIControlStateNormal];
    [leftbarButton setImage:imageOff forState:UIControlStateNormal];
}

-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}

-(void)setRightBarButtonImage:(NSDictionary*)images{
    
    UIImage *buttonImageOn = [images objectForKey:@"ImgOn"];
    UIImage *buttonImageOff = [images objectForKey:@"ImgOff"];
    
    [rightbarButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
}

-(void)hideRightBarButton:(BOOL)hide {
    
    [rightbarButton setHidden:hide];
}

@end
