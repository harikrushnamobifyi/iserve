//
//  FBLoginHandler.h
//  FBShareSample
//
//  Created by "Surender Rathore" on 17/12/13.
//  Copyright (c) 2013 Facebook Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginButton.h>
#import <FacebookSDK/FacebookSDK.h>

@protocol FBLoginHandlerDelegate <NSObject>
-(void)didFacebookUserLogin:(BOOL)login withDetail:(NSDictionary*)userInfo;

@optional

-(void)didFacebookUserSessionOpened:(BOOL)sessionOpened;
-(void)didFacebookUserLogout:(BOOL)logout;
- (void)didUserCancelLogin;

@end

@interface FBLoginHandler : UIView
@property(nonatomic,strong)id<FBLoginHandlerDelegate> delegate;

+ (id)sharedInstance;
-(void)loginWithFacebook;
-(void)updateFacebookSession;
-(void)logoutFacebookUser;

@end
