//
//  DestinationAddress.m
//  UBER
//
//  Created by Rahul Sharma on 22/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "DestinationAddress.h"


@implementation DestinationAddress

@dynamic desAddress;
@dynamic desAddress2;
@dynamic desLatitude;
@dynamic desLongitude;

@end
